### About project

#### Run project for local development
- Install git, docker and docker-compose
- Run docker containers via CLI by the next command

``` sudo docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d```

- Go inside the container ``` sudo docker exec -it {your_container_name} /bin/bash```
- Install dependencies by calling `` composer install``
- Copy .env.example to .env
- Call in CLI ``php artisan key:generate``
- Set permissions for directories ``chmod 777 -R storage/ bootstrap/cache/`` 
- Type in a browser "localhost:8080"

#### Run project for production
- Install git, docker and docker-compose
- Configure needed domain in ``.dockerbeta/production-domain.conf``
- Run docker containers via CLI by the next command

``` sudo docker-compose -f docker-compose.yml -f docekr-compose.prod.yml up -d```

- Go inside the container ``` sudo docker exec -it {your_container_name} /bin/bash```
- Install dependencies by calling `` composer install``
- Copy .env.example to .env
- Call in CLI ``php artisan key:generate``
- Set permissions for directories ``chmod 777 -R storage/ bootstrap/cache/``
- Generate certificates with LetsEncrypt by calling ``certbor --apache`` and following the instructions
- Type in a browser domain name

# About build

### Fast start
* Clone this repository to any directory
* Go to root directory of project with cli and call `composer install` (PHP 7.3 and composer should be installed before)
* Define directory `public` as root for your app (defined on server level)
* Create DB and user with sufficient rights for it (MySQL 5.7 or other should be installed before)
* Copy file `.env.exemple` to root directory with name `.env`
* Define in `.env` DB, DB user and password
* Call in cli `php artisan migrate --seed`

### Features
Built with [Laravel 6](https://laravel.com/docs/6.x)

#### Test server closed with auth middleware
For simplify organization of closing test server was added additional param to .env ```APP_GLOBAL_AUTH```
It set ```true``` for default. If you need to remove auth middleware for public routes just set ```true``` for .env ```APP_GLOBAL_AUTH```

#### Manual setting up https for urls generation
For the situation when used proxy-pass without https or when https was not recognized automatically by the framework 
app/Providers/RouteServiceProvider.php:27-29

#### .htaccess features
* added compressing
* added redirection for url with ```index.php```

#### Defined identifier for current page
In all views defined variable named `$body_class` in which defined identifier for current page
If you want to remove it, remove `$this->registerServices();` form `\App\Providers\RouteServiceProvider->register()`

#### DB
* Was off strict mode in work with DB for work with groupBy with easy way
* Engine set to **InnoDB**, cause some times on create BD on not wps server, engine was **MyISAM** and cascade not work!    

#### Automation set local
In main template param lang sets automatically based on local which was set in app 

###Testing
The Laravel Dusk installed. The additional options added for capability ``tests/DuskTestCase.php:32``

####Selenium server using
You can use Selenium server for run browser tests with docker. For doing this you should do the next steps:
* Set option ``USE_SELENIUM=true`` in ``.env`` file
* Uncomment lines connected to Selenium in docker-compose file ``docker-compose.yml:34``
* Set ``APP_URL=http://slenium-app`` in ``.env``

###Docker using
The docker configuration are included to the build. You have possibility to run 2 different configurations. 
The first one is prepared for back-end works only. It is set as default. You need just call ``sudo docker-compose up`` to run it.

The second one is prepared for front-end and back-end works. You need to comment this line ``docker-compose.yml:10`` and uncomment this one ``docker-compose.yml:11``
to use it.  Call ``sudo docker-compose up`` after this to start the docker containers

You can get additional details about working with docker here - [Ubuntu preparing](https://docs.google.com/document/d/16Gsv4XpJ4t8Hz0t2Dw_1raqgtPhnFp7GUwf1ickqkIY/edit?usp=sharing)


#### Installed WebMagic modules
* [Dashboard](https://bitbucket.org/webmagic/dashboard)
* [Users](https://bitbucket.org/webmagic/users)
* [Request](https://bitbucket.org/webmagic/request)
* [Notifier](https://bitbucket.org/webmagic/mailer)

#### Installed third-party packages
All these packages installed as `require-dev` and will not be installed when call `composer install --no-dev` 

##### Any environments
* [Collition](https://github.com/nunomaduro/collision) - clear console errors

##### Local and dev-server environments
In ```local``` and ```dev-server``` environments will be available:

* [Iseed](https://github.com/orangehill/iseed) - seeders generator
* [MigrationsGenerator](https://github.com/Xethron/migrations-generator) - migrations generator

##### Only local environment
In ```local``` environment only will be available:

* [Debugbar](https://github.com/barryvdh/laravel-debugbar) - debugbar
* [Laravel Dusk](https://laravel.com/docs/5.5/dusk) - for run browser tests

##### Changing environment
For change environment define in `.env` variable `APP_ENV` with name of needed environment. For example `APP_ENV=local`

#### Take in mind when working
* [Rotes structure convention](https://wm-dev.atlassian.net/wiki/spaces/WEB/pages/2756984/Laravel+5.1+-+5.5)
* [Links structure convention](https://wm-dev.atlassian.net/wiki/spaces/WEB/pages/2787862/Laravel)
* [Base structure convention](https://wm-dev.atlassian.net/wiki/spaces/WEB/pages/3022909/Laravel+5.4)
* [Git working convention](https://wm-dev.atlassian.net/wiki/spaces/WEB/pages/40960020/2.+Git)


## Front

#### Fast start
* Check that you using front-end  configuration of docker
* For installed third-party packages run `npm i` in direction  `/resources/assets/_gulp-builder`
* Define root directory in  `gulpfile.js` and `webpack.config.js` 
* Call in _gulp-builder  `gulp watch` or `gulp build`

As a result you will get two js files, libs.js and script.js  and two css files base-styles.css and style.css in root direction in the corresponding folders

#### More information
* [Working with front end build system](https://wm-dev.atlassian.net/wiki/spaces/WEB/pages/41386077/4.+Gulp)
* [JS code style convention](https://wm-dev.atlassian.net/wiki/spaces/WEB/pages/2363581/JavaScript+Code+Styling)
* [CSS code style convention](https://wm-dev.atlassian.net/wiki/spaces/WEB/pages/62324949/WebMagic+CSS+code+convention)


Additional:
 - service supervisor start (run in container)
 - add in .env PAYTRIOT api data
 - add in .env google recaptcha key and secret
 - chmod 777 -R  public/css/custom.css
 - php artisan storage:link
