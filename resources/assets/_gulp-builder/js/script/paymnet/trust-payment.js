import 'bootstrap/js/dist/modal';
import '../../libs/spinner';

export class TrustPayment {
    constructor() {
        this.orderFormClass = '.js_order-form'
        this.formTrustPayment = $('#st-form');
        this.orderStoreCardClass = '.js_store-card'
        this.purchseOrderInputId = '#order'
        this.purchseFormSavePaymentMethodInputId = '#save-payment-method'
    }
    init() {
        if(this.formTrustPayment.length){
            $.spinnerAdd();
            $.ajax( {
                url: this.formTrustPayment.attr('data-cart'),
                cache: false,
                data: {},
                method: 'GET',
                success: function (data){
                    if(Object.values(data).length){
                        initTrustPayment();
                    }else{
                        window.location.replace("/cart");
                    }
                    $.spinnerRemove();
                },
                error: (error) =>{
                    window.location.replace("/cart");
                    $.spinnerRemove();
                }
            })
        }

        $('body').on('submit', this.orderFormClass, (event)=>{
            event.preventDefault();
            this.processOrderForm()
        });
    }

    /**
     * Create new order by processing order form
     */
    processOrderForm() {
        let _this = this;
        let form = $(this.orderFormClass);
        let data = form.serialize();

        $.spinnerAdd();

        $.ajax( {
            url: form.attr('action'),
            cache: false,
            data: data,
            method: form.attr('method'),
            success: function (data){
                // Redirect for orders without payment
                if(data.redirect !== undefined){
                    location.replace(data.redirect)
                } else {
                    _this.preparePurchaseFormAndInitPayment(data.orderId)
                }
            },
            error: (error) =>{
                $.spinnerRemove();
                alert('Order creating error. Please, try later')
            }
        })
    }

    /**
     * Add additional data to the purchase form and init payment
     *
     * @param orderId
     */
    preparePurchaseFormAndInitPayment(orderId){
        $(this.purchseOrderInputId).val(orderId)

        let savePaymentMethodStatus = $(this.orderStoreCardClass).prop('checked') ? 1 : 0;
        $(this.purchseFormSavePaymentMethodInputId).val(savePaymentMethodStatus)

        this.initPayment()
    }

    /**
     * Start payment process
     */
    initPayment(){
        $('#st-form').submit();
    }

}
