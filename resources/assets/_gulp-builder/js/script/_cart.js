import 'bootstrap/js/dist/modal';
import {getToken} from "../libs/getToken";
import '../libs/spinner';

export class Cart{
    constructor(settings){
        this.settings = settings;
        this.btnDelete = settings.btnDelete;
        this.amountInfo = settings.amountInfo;
        this.wrapCntCart = settings.wrapCntCart;
    }
    init(){
        let $body = $('body');
        let _this = this;

        /**
         * Delete element of cart
         */
        $body.on('click', _this.btnDelete, function (event) {
            event.preventDefault();
            let action = $(this).data('action');
            //let amountItem = $(this).data('amount');
            let cartItem = $(this).closest('.js_item-wrap');
            _this.sendRequest(action, {}, function (data) {
                $('.js_cart-total').html(data);
                cartItem.remove();
                //let newAmountItem = $(_this.amountInfo).html() - amountItem;
                //$(_this.amountInfo).html(newAmountItem);
                let newAmountItem = $('.js_item-wrap').length;
                if(newAmountItem <= 0){
                    $(_this.wrapCntCart).html('<h4>Your cart is empty</h4>');
                    $('.fixed-cart-countdown').remove();
                }
            })
        });
    }
    sendRequest(url = '/', data, successFun){
        $.spinnerAdd();
        data._token = getToken();
        $.ajax({
            url: url,
            method: "POST",
            data: data,
            cache: false,
            success: (data) => {
                $.spinnerRemove();
                successFun(data);
            },
            error: (error) =>{
                $.spinnerRemove();
            }
        });
    }

}
