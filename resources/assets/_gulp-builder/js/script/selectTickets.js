import 'bootstrap/js/dist/modal';
import {getToken} from "../libs/getToken";
import '../libs/spinner';

export class Tikets{
    constructor(settings){
        this.settings = settings;
        this.valTicket = settings.valTicket;
        this.tickets = settings.btnTicket;
        this.pickedTickets = settings.pickedTickets;
        this.ticketsWrap = settings.ticketsWrap;
        this.selectedTicketsWrap = $(settings.selectedTicketsWrap);
        this.selectedClass = settings.selectedActiveClass;
        this.btnSubmit = settings.btnSubmit;
        this.btnSubmitForRandomTickets = settings.btnSubmitForRandomTickets;
        this.btnRandom = settings.btnRandom;
        this.answerItem = settings.answerItem;
        this.selectedClassTemp = 'js_selected-';
        this.allTickets = settings.allTickets;
        this.state = {
            auth: true,
            availableQuantity: 0,
            selectedQuantity: 0,
            selectedAnswer: false,
            selectedAnswerId: null
        }
    }
    init(){
        let $body = $('body');
        let _this = this;
        this.state.availableQuantity = this.selectedTicketsWrap.data('amount');
        const auth = this.selectedTicketsWrap.data('auth');
        this.state.auth = auth === 'true' || auth == '1';

        /**
         * Remove selected tickets of numbers picked
         */
        $body.on('click', _this.pickedTickets, function (event) {
            event.preventDefault();
            const curTicketText = $(this).text().replace(/\s{2,}/g, '');
            $('.' +_this.selectedClassTemp + curTicketText).removeClass(_this.selectedClass);
            _this.removeTicketInBlock(curTicketText);
            $(_this.allTickets).html($(_this.pickedTickets).length);
        });

        /**
         * Selected tickets
         */
        $body.on('click', _this.tickets, function (event) {
            event.preventDefault();
            if(!_this.state.auth){
                return;
            }
            const $curTicket = $(this);
            //$curTicket.html(_this.valTicket.value());
            const $allElCurTickets = $('.' + $curTicket.attr('class').replace(/^\s*/,'').replace(/\s*$/,'').split(/\s+/).join('.'));

            if($curTicket.hasClass(_this.selectedClass)){
                //remove ticket
                $allElCurTickets.removeClass(_this.selectedClass);
                _this.removeTicketInBlock($curTicket.text().replace(/\s{2,}/g, ''));
            }else{
                //add ticket
                if(_this.checkAvailableQuantityTickets()){
                    $allElCurTickets.addClass(_this.selectedClass);
                    _this.addTicketInBlock($curTicket.text().replace(/\s{2,}/g, ''));
                }else{
                    _this.showModalError('Sorry, you have selected or exceeded the maximum tickets for this competition');
                }
            }
            $(_this.allTickets).html($(_this.pickedTickets).length);
        });

        /**
         * Mark selected answer
         */
        $body.on('click', _this.answerItem, function (event) {
            event.preventDefault();
            if(!_this.state.auth){
                return;
            }
            _this.state.selectedAnswer = true;
            _this.state.selectedAnswerId = $(this).data('id');

            $(_this.answerItem).removeClass(_this.selectedClass);
            $(this).addClass(_this.selectedClass);
        });

        /**
         * Random generate selected ticket's
         */
        $body.on('click', _this.btnRandom, function (event) {
            event.preventDefault();
            if(!_this.state.auth){
                return;
            }
            const $thisBtn = $(this);
            $thisBtn.removeClass(_this.selectedClass);
            const amountTickets = $(_this.valTicket).val();
            _this.selectedTicketsWrap.find('.selected').each(function(i) {
                if(amountTickets <= _this.state.availableQuantity) {
                    $(this).removeClass(_this.selectedClass);
                    _this.selectedTicketsWrap.html();
                }
            });
            _this.state.selectedQuantity = _this.selectedTicketsWrap.find('.selected').length;
            if(_this.checkAvailableQuantityTickets() && amountTickets <= _this.state.availableQuantity ){
                $(_this.allTickets).html(amountTickets);
                _this.sendRequest($thisBtn.data('action'), {
                    competition_id: _this.getCompetitionId(),
                    amount: amountTickets
                }, function (data) {
                    $(_this.ticketsWrap).html(data);
                    $('.number-tabs  a').removeClass('active show').first().addClass('active show');
                    _this.selectedTicketsWrap = $(_this.settings.selectedTicketsWrap);
                    _this.state.selectedQuantity = _this.selectedTicketsWrap.find('.selected').length;
                    $('.tab-pane').each(function(i) {
                        $(this).removeClass('active');
                    });
                    $('#lucky-dip').addClass('active');
                } )
            }else{
                _this.showModalError('Sorry, you have selected or exceeded the maximum tickets for this competition');
            }
        });

        /**
         * Submit random tickets and answer
         */
        $body.on('click', this.btnSubmitForRandomTickets, function (event) {
            event.preventDefault();
            if(!_this.state.auth){
                return;
            }
            if(!_this.state.selectedAnswer){
                _this.showModalError('Please select an answer to the competition question');
                return;
            }
            const action = $(this).data('action');
            const amountTickets = $(_this.valTicket).val();

            if(amountTickets > _this.state.availableQuantity ){
                _this.showModalError('Sorry, you have selected or exceeded the maximum tickets for this competition');
                return;
            }

            _this.sendRequest(action, {
                competition_id: _this.getCompetitionId(),
                tickets_count: amountTickets,
                answer_id: _this.state.selectedAnswerId
            }, function () {
                $.spinnerAdd();
                window.location.replace("/cart");
            });
        });
        /**
         * Submit selected numbers and answer
         */
        $body.on('click', _this.btnSubmit, function (event) {
            event.preventDefault();
            if(!_this.state.auth){
                return;
            }
            if(!_this.state.selectedAnswer){
                _this.showModalError('Please select an answer to the competition question');
                return;
            }

            const tickets = _this.getAllTickets();

            if(tickets.length === 0){
                _this.showModalError('Please, select tickets');
                return;
            }

            const action = $(this).data('action');

            _this.sendRequest(action, {
                competition_id: _this.getCompetitionId(),
                tickets: tickets,
                answer_id: _this.state.selectedAnswerId
            }, function () {
                $.spinnerAdd();
                window.location.replace("/cart");
            });

        })
    }

    getCompetitionId(){
        return $('#_competition-id').html();
    }

    sendRequest(url = '/', data, successFun){
        $.spinnerAdd();
        data._token = getToken();
        $.ajax({
            url: url,
            method: "POST",
            data: data,
            cache: false,
            success: (data) => {
                $.spinnerRemove();
                successFun(data);
            },
            error: (error) =>{
                $.spinnerRemove();
                this.showModalError(error.responseJSON.message);
            }
        });
    }

    /**
     * Add selected tickets in array for send
     * @returns {[]}
     */
    getAllTickets(){
        let arraySelectedTickets = [];
        this.selectedTicketsWrap.find('.selected').each(function(i) {
            arraySelectedTickets.push($(this).text());
        });
        return arraySelectedTickets;
    }

    showModalError(text){
        $('#error-modal').on('show.bs.modal', function (event) {
            $(this).find('.js_tickes-modal-msg').text(text);
        });
        $('#error-modal').modal('show');
    }

    /**
     * Add ticket in result block after selected
     * @param val - number
     */
    addTicketInBlock(val){
        this.state.selectedQuantity += 1;
        let htmlTmp = `<li class="numbers-picked-i selected js_tickets-picked ${this.selectedClassTemp + val}">${val}</li>`;
        this.selectedTicketsWrap.append(htmlTmp);
    }

    /**
     * Delete ticket of result block
     * @param val - number
     */
    removeTicketInBlock(val){
        this.state.selectedQuantity -= 1;
        this.selectedTicketsWrap.find('.'+this.selectedClassTemp + val).remove();
    }

    checkAvailableQuantityTickets(){
        if(this.state.selectedQuantity < this.state.availableQuantity ){
            return  true;
        }
        return false;
    }
}
