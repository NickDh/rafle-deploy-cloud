@push('style')

    <style type="text/css">
        /*////// RESET STYLES //////*/
        body{height:100% !important; margin:0; padding:0; width:100% !important;}
        table{border-collapse:separate;}
        img, a img{border:0; outline:none; text-decoration:none;}
        h1, h2, h3, h4, h5, h6{margin:0; padding:0;}
        p{margin: 1em 0;}

        /*////// CLIENT-SPECIFIC STYLES //////*/
        .ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;}
        table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}
        #outlook a{padding:0;}
        img{-ms-interpolation-mode: bicubic;}
        body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%;}

        /*////// GENERAL STYLES //////*/
        img{ max-width: 100%; height: auto; }

        /*////// TABLET STYLES //////*/
        @media only screen and (max-width: 620px) {
            .shrink_font{
                font-size: 62px;
            }
        }


        /*////// MOBILE STYLES //////*/
        @media only screen and (max-width: 480px){
            .shrink_font{
                font-size: 48px;
            }
            .safe_color{
                color: #6a1b9a !important;
            }
            /*////// CLIENT-SPECIFIC STYLES //////*/
            body{width:100% !important; min-width:100% !important;} /* Force iOS Mail to render the email at full width. */
            table[class="flexibleContainer"]{ width: 100% !important; }/* to prevent Yahoo Mail from rendering media query styles on desktop */
        }
    </style>
@endpush
@extends('emails/_email-template')
@section('content')

    <tr>
        <td>
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td bgcolor="#fcfcfc" style="padding: 40px 0;border: 1px solid #f2f2f2;border-radius: 5px;">
                        <!-- body-container -->
                        <table width="480" align="center" border="0" cellspacing="0" cellpadding="0">

                            <!-- email heading -->
                            <tr>
                                <td align="center" style="line-height: 1;color: #282828; font-size: 26px; font-weight: 700; font-family: 'Open Sans', Helvetica, sans-serif; mso-line-height-rule: exactly;">
                                    <div>
                                        <span class="text_container">Thank You For Registering</span>
                                    </div>
                                </td>
                            </tr><!-- END email heading -->

                            <!-- horizontal gap -->
                            <tr><td height="40"></td></tr>

                            <!-- main-icon -->
                            <tr>
                                <td align="center">
                                    <a href="#">
                                        <img src="{{asset('/img/circle-icon-user.png')}}" style="display:block; line-height:0; font-size:0; border:0;" border="0" alt="image" />
                                    </a>
                                </td>
                            </tr><!-- END main-icon -->

                            <!-- horizontal gap -->
                            <tr><td height="35"></td></tr>

                            <!-- email details -->
                            <tr>
                                <td align="center" style="line-height: 1.8;color: #333333; font-size: 14px; font-weight: 400; font-family: 'Open Sans', Helvetica, sans-serif; mso-line-height-rule: exactly;">
                                    <div>
                                        <span>You can now enter our competitions via our website. Please keep your login information below in a safe place.</span>
                                    </div>
                                    <table align="center" cellpadding="10" style="line-height: 1.8;color: #333333; font-size: 14px; font-weight: 600; font-family: 'Open Sans', Helvetica, sans-serif; mso-line-height-rule: exactly;">
                                        <tr>
                                            <td>Username</td>
                                            <td>{{ $user->username }}</td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td>{{ $user->email }}</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr><!-- END email details -->

                            <!-- horizontal gap -->
                            <tr><td height="35"></td></tr>

                            <!-- buttons -->
                            <tr>
                                <td align="center">
                                    <table align="center" border="0" cellspacing="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <table bgcolor="#da2238" height="40" align="left" border="0" cellpadding="0" cellspacing="0" style="border-radius:5px; border-collapse: separate">
                                                    <tr>
                                                        <td align="center" valign="middle" style="padding: 10px; text-transform: uppercase; font-size: 14px; font-weight: 600; font-family: 'Open Sans', Helvetica, sans-serif; mso-line-height-rule: exactly;">
                                                            <div>
																<span class="text_container">
																	<a href="{{route('live-competitions')}}" style="text-decoration: none; color: #ffffff;">View Live Competitions</a>
																</span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <!-- VERTICAL GAP -->
                                            <td width="20"></td>

                                            <td>
                                                <table bgcolor="#52b145" height="40" align="left" border="0" cellpadding="0" cellspacing="0" style="border-radius:5px; border-collapse: separate">
                                                    <tr>
                                                        <td align="center" valign="middle" style="padding: 10px; text-transform: uppercase; font-size: 14px; font-weight: 600; font-family: 'Open Sans', Helvetica, sans-serif; mso-line-height-rule: exactly;">
                                                            <div>
																<span class="text_container">
																	<a href="{{route('user.info')}}" style="text-decoration: none; color: #ffffff;">View My Account</a>
																</span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr><!-- END buttons -->
                        </table><!-- END body-container -->
                    </td>
                </tr>
            </table>
        </td>
    </tr>

@endsection
