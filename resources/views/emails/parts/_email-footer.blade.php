<!-- footer -->
<tr>
    <td>
        <table width="600" align="center" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="center" style="line-height: 1;color: #c6c6c6; font-size: 14px; font-weight: 400; font-family: 'Open Sans', Helvetica, sans-serif; mso-line-height-rule: exactly;">
                    <div class="editable-text">
                        <span class="text_container">www.gasmonkeycompetitions.co.uk</span>
                    </div>
                </td>
            </tr>
            <!-- horizontal gap -->
            <tr><td height="15"></td></tr>

            <tr>
                <td align="center" style="line-height: 1;color: #c6c6c6; font-size: 10px; font-weight: 400; font-family: 'Open Sans', Helvetica, sans-serif; mso-line-height-rule: exactly;">
                    <div class="editable-text">
                        <span class="text_container">Powered By <a href="https://www.rafflelabs.co.uk" style="color: #c6c6c6; text-decoration: none">RaffleLabs</a></span>
                    </div>
                </td>
            </tr>
        </table>
    </td>
</tr><!-- END footer -->

<!-- padding-bottom -->
<tr><td height="100"></td></tr>
