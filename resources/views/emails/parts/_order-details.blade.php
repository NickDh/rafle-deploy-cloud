<!-- row- -->
<tr>
    <td bgcolor="#ffffff" style="padding: 0 20px;border: 1px solid #ebebeb;border-radius: 5px;">
        <table width="400" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <!-- column-1 -->
                    <table align="left" border="0" cellpadding="0" cellspacing="0">
                        <!-- margin-top -->
                        <tr><td height="25"></td></tr>
                        <tr>
                            <td>
                                <table align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="left" style="line-height: 1;color: #282828;font-size: 18px; font-weight: 700; font-family: 'Open Sans', Helvetica, sans-serif; mso-line-height-rule: exactly;">
                                            <div>
                                                <span class="text_container">Order Details</span>
                                            </div>
                                        </td>
                                    </tr>

                                    <!-- horizontal gap -->
                                    <tr><td height="5"></td></tr>

                                    <tr>
                                        <td align="left" style="line-height: 2;color: #333333;font-size: 14px; font-weight: 600; font-family: 'Open Sans', Helvetica, sans-serif; mso-line-height-rule: exactly;">
                                            <div class="editable-text">
                                                <span class="text_container">Total Amount : £{{$order->present()->total}}</span>
                                            </div>
                                            <div class="editable-text">
                                                <span class="text_container">Order Number : #{{$order->id}}</span>
                                            </div>
                                            <!--
                                            <div class="editable-text">
                                                <span class="text_container">Number Of Successful Entires : {{$order->successfulEntriesCount()}}</span>
                                            </div> -->
                                            <div class="editable-text">
                                                <span class="text_container">Date Purchased : {{$order->present()->prettyCreatedDate}}</span>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <!-- margin-bottom -->
                        <tr class="tablet_hide"><td height="20"></td></tr>
                    </table><!-- ENd column-2 -->
                    <!-- vertical gutter -->
                    <table align="left" border="0" cellpadding="0" cellspacing="0">
                        <tr><td height="1"></td></tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr><!-- END row- -->

<!-- horizontal gap -->
<tr><td height="25"></td></tr>

@foreach($order->competitionEntries as $competitionEntry)
    <!-- row- -->
    <tr>
        <td bgcolor="#ffffff" style="padding: 0 20px;border: 1px solid #ebebeb;border-radius: 5px;">
            <table width="400" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <!-- column-1 -->
                        <table align="left" border="0" cellpadding="0" cellspacing="0">
                            <!-- margin-top -->
                            <tr><td height="25"></td></tr>
                            <tr>
                                <td>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="left" style="line-height: 1;color: #282828;font-size: 14px; font-weight: 600; font-family: 'Open Sans', Helvetica, sans-serif; mso-line-height-rule: exactly;">
                                                <div>
                                                    <span class="text_container">{{$competitionEntry->competition_title}}</span>
                                                </div>
                                            </td>
                                        </tr>

                                        <!-- horizontal gap -->
                                        <tr><td height="5"></td></tr>
                                        <tr>
                                            <td align="left" style="line-height: 2;color: #333333;font-size: 12px; font-weight: 400; font-family: 'Open Sans', Helvetica, sans-serif; mso-line-height-rule: exactly;">
                                                <div class="editable-text">
                                                    <span class="text_container">Answer : {{$competitionEntry->answer}}</span>
                                                </div>
                                                <div class="editable-text">
                                                    @if($competitionEntry->isAnswerCorrect())
                                                        <span style="color: #52b145">Correct Answer</span>
                                                    @else
                                                        <span style="color: #da2238">Wrong Answer</span>
                                                    @endif
                                                </div>
                                                <div class="editable-text">
                                                    <span class="text_container">Ticket Numbers : {{$competitionEntry->present()->ticketNumbers}}</span>
                                                </div>
                                                <div class="editable-text">
                                                    <span class="text_container">Qty : {{count($competitionEntry->selectedTickets)}}</span>
                                                </div>
                                                <div class="editable-text">
                                                    <span class="text_container">Price : £{{$competitionEntry->present()->entryPrice()}}</span>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <!-- margin-bottom -->
                            <tr class="tablet_hide"><td height="20"></td>
                            </tr>
                        </table><!-- ENd column-2 -->
                        <!-- vertical gutter -->
                        <table align="left" border="0" cellpadding="0" cellspacing="0">
                            <tr><td height="1"></td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr><!-- END row- -->

    <!-- horizontal gap -->
    <tr><td height="25"></td>
    </tr>
@endforeach


<!-- row- -->
<tr>
    <td bgcolor="#ffffff" style="padding: 0 20px;border: 1px solid #ebebeb;border-radius: 5px;">
        <table width="400" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <!-- column-1 -->
                    <table align="left" border="0" cellpadding="0" cellspacing="0">
                        <!-- margin-top -->
                        <tr><td height="25"></td></tr>
                        <tr>
                            <td>
                                <table align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="left" style="line-height: 1;color: #282828;font-size: 18px; font-weight: 700; font-family: 'Open Sans', Helvetica, sans-serif; mso-line-height-rule: exactly;">
                                            <div>
                                                <span class="text_container">Billing Details</span>
                                            </div>
                                        </td>
                                    </tr>

                                    <!-- horizontal gap -->
                                    <tr><td height="5"></td></tr>

                                    <tr>
                                        <td align="left" style="line-height: 2;color: #333333;font-size: 14px; font-weight: 600; font-family: 'Open Sans', Helvetica, sans-serif; mso-line-height-rule: exactly;">
                                            <div class="editable-text">
                                                <span class="text_container">{{$order->customer_name}}</span>
                                            </div>
                                            <div class="editable-text">
                                                <span class="text_container">{{$order->customer_address}}</span>
                                            </div>
                                            <div class="editable-text">
                                                <span class="text_container">{{$order->customer_phone}}</span>
                                            </div>
                                            <div class="editable-text">
                                                <span class="text_container">{{$order->customer_email}}</span>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <!-- margin-bottom -->
                        <tr class="tablet_hide"><td height="20"></td></tr>
                    </table><!-- ENd column-2 -->
                    <!-- vertical gutter -->
                    <table align="left" border="0" cellpadding="0" cellspacing="0">
                        <tr><td height="1"></td></tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr><!-- END row- -->