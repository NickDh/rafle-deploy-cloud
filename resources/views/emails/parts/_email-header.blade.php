<tr>
    <td>
        <table width="600" align="center" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td bgcolor="#fcfcfc" style="padding-top: 30px;padding-right: 40px;padding-bottom: 0;padding-left: 40px; border: 1px solid #f2f2f2; border-radius: 5px;">
                    <!-- Logo -->
                    <table border="0" cellspacing="0" cellpadding="0" style="margin: 0 auto;">
                        <tr>
                            <td style="text-align: center;">
                                <a href="#" target="_blank"><img src="{{asset('/img/logo.png')}}" alt="GasMonkey Competitions" /></a>
                            </td>
                        </tr>
                        <!-- margin-bottom -->
                        <tr><td height="30"></td></tr>
                    </table><!-- END logo -->
                </td>
            </tr>
        </table>
    </td>
</tr><!-- END header -->
