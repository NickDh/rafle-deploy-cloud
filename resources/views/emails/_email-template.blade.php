<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="format-detection" content="date=no" />
    <meta name="format-detection" content="address=no" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="x-apple-disable-message-reformatting" />
    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    <!--<![endif]-->
    <title>Nitrous Competitions</title>
    <!--[if gte mso 9]>
    <![endif]-->
    <!-- body, html, table, thead, tbody, tr, td, div, a, span { font-family: Arial, sans-serif !important; } -->

    @stack('style')
</head>
<body style="padding: 0;margin: 0;">

<!-- template-1 -->
<table bgcolor="#ffffff" width="100%" align="center" cellspacing="0" cellpadding="0" border="0">
    <!-- padding-top -->
    <tr>
        <td height="100"></td>
    </tr>
    <!-- Logo -->
    @include('emails/parts/_email-header')
    <!-- Logo -->

    <!-- horizontal gap -->
    <tr><td height="25"></td></tr>

    <!-- Main -->
    @section('content')
    @show
    <!-- END Main -->

    <!-- horizontal gap -->
    <tr><td height="40"></td></tr>

    <!-- Footer -->
     @include('emails/parts/_email-footer')
    <!-- END Footer -->

    </table>
</body>
</html>
