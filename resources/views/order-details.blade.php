@extends('core.base')

@section('content')

<div class="top_banner">
    <div class="opacity-mask d-flex align-items-center">
        <div class="container">
            <h1>Order Details</h1>
        </div>
    </div>
</div>
<!-- /top_banner -->
<div class="container margin_30">
    <div class="row order_wrap">
        <div class="col-md-12">
            <div class="card shadow mb-3">
                @include('orders.parts._order-items-details', ['order' => $order])
            </div>
        </div>
        <div class="col-lg-12">
            <!-- Card -->
            <div class="card mb-3 shadow">
                  @include('orders.parts._order-details', [ 'order' => $order, 'btnLink' => route('user.info') ])
            <!-- Card -->
        </div>
    </div>
</div>

@endsection
