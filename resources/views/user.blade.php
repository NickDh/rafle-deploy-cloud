@extends('core.base')

@section('content')

<div class="top_banner">
    <div class="opacity-mask d-flex align-items-center">
        <div class="container">
            <h1>Account</h1>
        </div>
    </div>
</div>
<!-- /top_banner -->
<div class="container margin_60_35 account_wrap">
    <div class="row">
        <div class="col-md-3 account_menu">
            <h4 class="d-none d-lg-block">Account Menu</h4>
                        <nav class="navbar navbar-expand-lg navbar-light p-0">
                <h4 class="d-lg-none">Account Menu</h4>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#accountnav" aria-controls="accountnav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="accountnav">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                        <a class="nav-link @if(request()->is('entries*')) active @endif" href="{{route('entries')}}"><i class="ti-ticket"></i> Entries</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link @if(request()->is('orders*')) active @endif" href="{{route('orders')}}"><i class="ti-shopping-cart"></i> Orders</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link active" href="{{route('user.info')}}"><i class="ti-user"></i> Account Details</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link @if(request()->is('change-password*')) active @endif" href="{{route('change-password')}}"><i class="ti-lock"></i> Change Password</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{route('logout')}}"><i class="ti-power-off"></i> Log Out</a>
                        </li>
                        @auth
                            @if(Auth::user() && Auth::user()->isAdmin())
                        <li class="nav-item">
                        <a class="nav-link" href="{{route('dashboard::index')}}"><i class="ti-key"></i> Admin Dashboard</a>
                        </li>
                            @endif
                        @endauth
                    </ul>
                </div>
            </nav>
        </div>
        <div class="col-md-8 order_wrap mt-4">
            <h2>Account Details</h2>
            <div class="card mb-3">
                <div class="card-body">
                    <form action="{{route('user.update-info')}}" method="post" class="js_form-change-user-info">
                        <div class="row">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" class="form-control" placeholder="First Name" value="{{$user->first_name}}" name="first_name" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control" placeholder="Last Name" value="{{$user->last_name}}" name="last_name" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" class="form-control" id="display-name" name="username" placeholder="{{$user->username}}" disabled>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" placeholder="Email" id="email" value="{{$user->email}}" name="email" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="text" class="form-control" placeholder="Phone" id="phone" value="{{$user->phone}}" name="phone" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>First Line Address</label>
                                    <input type="text" class="form-control" placeholder="First Line Address" value="{{$user->address}}" name="address" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>City / Town</label>
                                    <input type="text" class="form-control" placeholder="City/Town" value="{{$user->town}}" name="town" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Post Code</label>
                                    <input type="text" class="form-control" placeholder="Post Code" value="{{$user->post_code}}" name="post_code" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group wrap-checkbox">
                                    <input class="form-checkbox" type="checkbox" name="subscription" id="assent" @if($user->subscription) checked @endif>
                                    <label for="assent"><i class="checked-ic"></i><span>Keep me updated with all the news, special offers and discount codes</span></label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button class="btn_1 full-width reg" type="submit">Update Details</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
