@if ($paginator->hasPages())
    <div class="entry_pagination">
        <p><span>{{($paginator->currentPage()-1)*$paginator->perPage()+1}}-{{$paginator->currentPage()*$paginator->perPage()}}</span> of {{$paginator->total()}}</p>
        <ul>
            <li>
                <a class="prev js_btn-prev @if($paginator->onFirstPage()) __disable @endif" data-page="{{$paginator->currentPage()}}" href="{{ $paginator->url($paginator->previousPageUrl()) }}">
                    <i class="ion-ios-arrow-back"></i>
                </a>
            </li>
            <li>
                <a class="next js_btn-next @if(!$paginator->hasMorePages()) __disable @endif" data-page="{{$paginator->currentPage()}}" href="{{ $paginator->nextPageUrl() }}">
                    <i class="ion-ios-arrow-forward"></i>
                </a>
            </li>
        </ul>
    </div>
@endif
