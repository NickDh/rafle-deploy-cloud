<div class="container margin_60_35">
    <div class="main_title">
        <h2>About Us</h2>
        <p>The faces behind the Gas Monkey.</p>
    </div>
    <div class="row align-items-center">
        <div class="col-lg-6 about-us ">
        	<p>GASMONKEY Competitions is a new & innovative entry into the competitions market.</p>
        	<p>Our objective is to provide a new entertaining experience for our customers in order for them to have a chance at  winning some great aspirational prizes.</p>
        	<p>At the same time, GasMonkey competitions wants to operate as a responsible company supporting its customers, communities and identified charities.</p>
        	<p>Our company values are,<br>
        		To be a RESPONSIBLE and SUPPORTIVE company operating in a FUN but RELIABLE manor.<br>
        		Our social media platforms make it fun & simple to get involved!<br>
        		We are proud to display our Trustpilot ranking.<br>
        	</p>
        	<p>Good luck with the competitions!</p>
        </div>
        <div class="col-lg-6 text-center">
            <img src="{{asset('/img/about-us.jpg')}}" width="100%" alt="About Us" />
        </div>
    </div>
    <!-- /row -->
</div>
<!-- /container -->