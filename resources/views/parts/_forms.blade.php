<!-- Modal -->
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Login Or Register</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">x</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <p>You must be registered to play our competitions. Please login or create an
                            account.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                    <a class="btn_1" href="{{route('user.login')}}">Login</a>
                    <a class="btn_1" href="{{route('user.register')}}">Register</a>
            </div>
        </div>
    </div>
</div>
<!-- Modal end -->


<div class="modal fade" id="error-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Error</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">x</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <p class="js_tickes-modal-msg">Sorry, you have selected or exceeded the maximum tickets for this competition</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


{{--Configm--}}
<div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>You are sure?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">x</span></button>
            </div>
            <div class="modal-footer">
                <div class="solid-btn">
                    <a class="text-white js_delete-btn-confirm" data-dismiss="modal" aria-label="Close">Yes</a>
                    <a class="text-white" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>

        </div>
    </div>
</div>


{{--3D Secure--}}
<button style="display: none;" class="js_modal-secure-btn"
        data-toggle="modal" data-target="#confirm-secure" ></button>
<div class="modal fade" id="confirm-secure" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">

        <div class="modal-content secure-modal-cnt">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">x</span></button>
            </div>
            <div class="modal-form modal-form-secure js_wrap-secure clearfix">
            </div>
        </div>
    </div>
</div>
