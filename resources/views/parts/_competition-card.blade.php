<div class="col-md-4 col-xl-4">
    <div class="grid_item">
        <figure>
            @if(\App\Enums\CompetitionStatusEnum::PAUSED()->is($competition->status))
                <span class="ribbon paused">{{$competition->status}}</span>
            @elseif(\App\Enums\CompetitionStatusEnum::FINISHED()->is($competition->status))
            @elseif(\App\Enums\CompetitionStatusEnum::SOLD_OUT()->is($competition->status))
                <span class="ribbon sold-out">{{$competition->status}}</span>
            @elseif($competition->sale_price)
                <span class="ribbon sale">ON SALE</span>
            @else
            @endif
            @if(! \App\Enums\CompetitionStatusEnum::FINISHED()->is($competition->status))
                <a href="{{route('live-competition.show', $competition->slug)}}">
            @else
                <a href="{{route('live-competition.finished', $competition->slug)}}">
            @endif
                <img class="img-fluid lazy" src="{{$competition->present()->main_image}}" alt="{{$competition->title}}">
            </a>
            @if(! $competition->isFinished())
                <div class="countdown">{{now()->diffInDays($competition->deadline)}} DAYS LEFT</div>
            @else
                <div class="countdown">Finished {{date('d/m/Y \@ H:i', strtotime($competition->deadline))}}</div>
            @endif
        </figure>
        @if(! \App\Enums\CompetitionStatusEnum::FINISHED()->is($competition->status))
                <a href="{{route('live-competition.show', $competition->slug)}}">
        @else
                <a href="{{route('live-competition.finished', $competition->slug)}}">
        @endif
            <h3>{{$competition->title}}</h3>
        </a>
        <div class="row p-2 align-items-center">
            <div class="col-md-12">
                @if(! $competition->isFinished())
                <div class="price_box">
                    <span class="new_price"><span>£</span>{{$competition->sale_price ? formatValue($competition->sale_price) : formatValue($competition->price)}}</span>
                    @if($competition->sale_price)
                    <span class="old_price">£{{formatValue($competition->price)}}</span>
                    @endif
                </div>
                @else
                @endif
            </div>
            
        </div>
    </div>
    <!-- /grid_item -->
</div>
