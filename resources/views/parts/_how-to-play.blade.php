<div class="bg_purple how-to">
    <div class="container margin_60">
        <div class="main_title">
        <h2>How To Play</h2>
        </div>
        <div class="row">
            <div class="col-md-3 text-center">
                <img src="{{asset('img/how-to-1.svg')}}" width="200" height="144">
                <h3>Choose a prize</h3>
                <p>Pick which prize competition you would like to enter</p>
            </div>
            <div class="col-md-3 text-center">
                <img src="{{asset('img/how-to-2.svg')}}" width="200" height="144">
                <h3>Select your tickets</h3>
                <p>Pick how many entries you'd like by selecting numbers on the page</p>
            </div>
            <div class="col-md-3 text-center">
                <img src="{{asset('img/how-to-3.svg')}}" width="200" height="144">
                <h3>Answer the question</h3>
                <p>Answer the multiple choice question. You must answer correctly for your entry to count.
            </div>
            <div class="col-md-3 text-center">
                <img src="{{asset('img/how-to-4.svg')}}" width="200" height="144">
                <h3>Watch the live draw</h3>
                <p>Follow our Facebook Page and watch the winner drawn live!</p>
            </div>
        </div>
    </div><!-- /container -->
</div>
<!-- /bg_gray -->
