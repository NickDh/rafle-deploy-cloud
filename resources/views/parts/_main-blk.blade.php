<div class="header-video">
    <div id="hero_video">
          <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
            <source src="{{asset('img/intro.mp4')}}" type="video/mp4">
          </video>
        <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0.5)">
            <div class="container">
                <div class="row justify-content-center justify-content-md-start">
                    <div class="col-lg-6">
                        <div class="slide-text white">
                            <h3>Petrol Head & Lifestyle Prizes</h3>
                            <p>Fancy winning dream prizes?</p>
                            <a class="btn_1" href="{{route('live-competitions')}}" role="button">View Competitions</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <img src="{{asset('img/video_fix.png')}}" alt="" class="header-video--media" data-video-src="video/intro" data-teaser-source="video/intro" data-provider="" data-video-width="1920" data-video-height="960">
</div>
<!-- /header-video -->

@if($live_draw)
<!-- draw-countdown -->
<div class="draw-section">
    <div class="container">
        <div class="row small-gutters align-items-center">
            <div class="col-md-8 offset-md-2 text-center bg_black p-4">
                <h2 class="mb-2">OUR NEXT LIVE DRAW <img src="{{asset('img/fb-live.svg')}}" style="width: 60px;margin-top: -5px;margin-left: 10px;"></h2>
                <div id="live-draw-countdown" data-countdown="{{date('Y/m/d H:i', strtotime($live_draw->date))}}"></div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- TrustBox widget - Micro Button -->
                        <div class="trustpilot-widget" data-locale="en-GB" data-template-id="5419b757fa0340045cd0c938" data-businessunit-id="5fa3f532263f0b0001b6ac27" data-style-height="25px" data-style-width="100%">
                          <a href="https://uk.trustpilot.com/review/gasmonkeycompetitions.co.uk" target="_blank" rel="noopener">Trustpilot</a>
                        </div>
                        <!-- End TrustBox widget -->
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- draw-countdown -->
@endif
