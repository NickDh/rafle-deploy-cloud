<!-- Header bar area Start -->
<div class="header-bar-area v2">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-2 col-md-4 col-6">
                <a class="navbar-brand p-0 m-0" href="{{url('/')}}">
                    <img src="{{asset('img/logo.svg')}}" alt="Logo">
                </a>
            </div>
            <div class="col-lg-10 col-md-8 col-6 text-right">
                <div class="header_menu_wrap">
                    <div class="header_menu">
                        <ul class="main-menu">
                            <li class="nav-item">
                                <a class="nav-link @if(request()->is('/*')) active @endif" href="{{route('main')}}">HOME</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if(request()->is('live-competitions*')) active @endif" href="{{route('live-competitions')}}">LIVE COMPETITIONS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if(request()->is('winners*')) active @endif" href="{{route('winners')}}">WINNERS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if(request()->is('how-to-play*')) active @endif" href="{{route('static.how-to-play')}}">HOW TO PLAY</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if(request()->is('charity*')) active @endif" href="{{route('static.charity')}}">CHARITY</a>
                            </li>
                            @foreach($headerLinksPages as $page)
                                <li class="nav-item">
                                    <a class="nav-link @if($page->isCurrent()) active @endif" href="{{$page->present()->link()}}">{{$page->title}}</a>
                                </li>
                            @endforeach
                        </ul>
                        <div class="hamburger-menu">
                            <span class="line-top"></span>
                            <span class="line-center"></span>
                            <span class="line-bottom"></span>
                        </div>
                    </div>
                    <div class="user_option">
                        <ul>
                            <li>
                                @auth
                                <a href="{{route('orders')}}"><i class="header-user"></i></a>
                                @endauth
                                @guest
                                <a href="{{route('user.login')}}"><i class="header-user"></i></a>
                                @endguest
                            </li>
                            <li>
                                <a href="{{route('cart.view')}}" class="header-cart-link"><i class="header-cart"></i><strong>{{$cartItems ?? 0}}</strong></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Header bar area End -->
<!-- Ofcanvas-menu -->
<div class="ofcavas-menu">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link js_menu-mob-i" href="{{route('main')}}">HOME</a>
        </li>
        <li class="nav-item">
            <a class="nav-link js_menu-mob-i" href="{{route('live-competitions')}}">COMPETITIONS</a>
        </li>
        <li class="nav-item">
            <a class="nav-link js_menu-mob-i" href="{{route('winners')}}">WINNERS</a>
        </li>
        <li class="nav-item">
            <a class="nav-link js_menu-mob-i" href="{{route('static.how-to-play')}}">HOW TO PLAY</a>
        </li>
        <li class="nav-item">
            <a class="nav-link js_menu-mob-i" href="{{route('static.charity')}}">CHARITY</a>
        </li>
        @foreach($headerLinksPages as $page)
            <li class="nav-item">
                <a class="nav-link js_menu-mob-i" href="{{$page->present()->link()}}">{{$page->title}}</a>
            </li>
        @endforeach
    </ul>
</div>
<!-- Ofcanvas-menu END-->
<main>