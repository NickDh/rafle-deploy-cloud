<div class="row">
    <div class="col-6"></div>
    <div class="col-6">
        @include('parts/_pagination-ajax', ['paginator' => $entries])
    </div>
</div>
<div class="row order_wrap">
    <div class="col-md-12">
            @foreach($entries as $competitionEntry)
                <div class="row mb-4 align-items-center js_item-wrap">
                    <div class="col-md-5 col-lg-3 col-xl-3">
                      <div class="mb-3 mb-md-0 text-center">
                        <img class="img-fluid" src="{{$competitionEntry->competition->present()->main_image}}" alt="{{$competitionEntry->competition_title}}">
                      </div>
                    </div>
                    <div class="col-md-7 col-lg-9 col-xl-9">
                        <div>
                            <div class="d-flex justify-content-between">
                              <div>
                                <h3>{{$competitionEntry->competition_title}}</h3>
                                <p class="mb-2 text-muted text-uppercase small">Numbers : {{$competitionEntry->present()->ticketNumbers}}</p>
                                <p class="mb-2 text-muted text-uppercase small">Deadline : {{$competitionEntry->competition->present()->realDeadline}}</p>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="mb-4">
            @endforeach
        </div>
    </div>
</div>
