<div class="bg_dark">
<div class="container margin_60_35 competition-grid">
    <div class="main_title">
        <h2>Live Competitions</h2>
        <p>View a list of our live competitions below.</p>
    </div>
    <div class="row small-gutters">
        @foreach($competitions as $competition)
                @include('parts/_competition-card')
        @endforeach
    </div>
    <!-- /row -->
    <div class="row text-center mt-2 mb-2">
        <div class="col-md-12">
            <a class="btn_1" href="{{route('live-competitions')}}" role="button">VIEW ALL COMPETITIONS</a>
        </div>
    </div>
</div>
</div>