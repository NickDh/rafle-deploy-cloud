</main>
<!-- Footer starts  -->
<footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <h3 data-target="#collapse_1">Quick Links</h3>
                    <div class="collapse dont-collapse-sm links" id="collapse_1">
                        <ul>
                            <li><a href="{{route('finished-competitions')}}">Finished Competitions</a></li>
                            <li><a href="{{route('live-competitions')}}">Live Competitions</a></li>
                            @isset($footerLinkPages['1'])
                                @foreach($footerLinkPages['1'] as $page)
                                    <li><a href="{{$page->present()->link()}}">{{$page->title}}</a></li>
                                @endforeach
                            @endisset

                            <li><a href="{{route('user.login')}}">My Account</a></li>
                            <li><a href="{{route('cart.view')}}">Basket</a></li>
                            <li><a href="{{route('static.contacts')}}">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h3 data-target="#collapse_2">Legal Pages</h3>
                    <div class="collapse dont-collapse-sm links" id="collapse_2">
                        <ul>
                            <li><a href="{{route('static.terms-conditions')}}">Terms & Conditions</a></li>
                            <li><a href="{{route('static.privacy-policy')}}">Privacy & Cookie Policy</a></li>
                            @isset($footerLinkPages['2'])
                                @foreach($footerLinkPages['2'] as $page)
                                    <li><a href="{{$page->present()->link()}}">{{$page->title}}</a></li>
                                    @endforeach
                            @endisset
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                        <h3 data-target="#collapse_3">Contact</h3>
                    <div class="collapse dont-collapse-sm contacts" id="collapse_3">
                        <ul>
                            <li><i class="ti-email"></i><a href="{{route('static.contacts')}}">info@gasmonkeycompetitions.co.uk</a></li>
                        </ul>
                    </div>
                    <h3 data-target="#collapse_4">Follow Us</h3>
                    <div class="collapse dont-collapse-sm" id="collapse_4">
                        <div class="follow_us">
                            <ul>
                                <li><a href="http://www.facebook.com/gasmonkeycompetitions/"><img src="{{asset('img/facebook_icon.svg')}}" alt="" class="lazy"></a></li>
                                <li><a href="http://www.instagram.com/gasmonkeycompetitions/"><img src="{{asset('img/instagram_icon.svg')}}" alt="" class="lazy"></a></li>
                                <li><a href="https://twitter.com/gasmonkeycomp"><img src="{{asset('img/twitter_icon.svg')}}" alt="" class="lazy"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="mt-4">
                    <a href="https://www.begambleaware.org/" target="_blank"><img src="{{asset('img/gamble-aware.png')}}" class="pb-2" /></a>
                    <p>This site is protected by reCAPTCHA and the Google
                    <a href="https://policies.google.com/privacy">Privacy Policy</a> and
                    <a href="https://policies.google.com/terms">Terms of Service</a> apply.</p> 
                    </div>
                </div>
            </div>
            <!-- /row-->
            <hr>
            <div class="row add_bottom_25">
                <div class="col-lg-6">
                    <ul class="footer-selector clearfix">
                        <li><img src="{{asset('img/cards_all.svg')}}" alt="Payment Cards" width="198" height="30" class="lazy"></li>
                    </ul>
                </div>
                <div class="col-lg-6">
                    <ul class="additional_links">
                        <li><span>© 2020 Gas Monkey Competitions</span></li>
                        <li><span>Powered By <a href="https://www.rafflelabs.co.uk" rel="nofollow">Raffle Labs</a></span></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!--/footer-->
<!-- Footer ends  -->
