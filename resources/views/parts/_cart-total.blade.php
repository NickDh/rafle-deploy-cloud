<form action="{{route('order.checkout')}}" method="GET">
<h5 class="mb-3">Basket Total</h5>
<ul class="list-group list-group-flush">
    <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
        Entries
        <span>£{{$cart->getSubTotal()}}</span>
    </li>
    @if($cart->getDiscount()->isNotEmpty())
        <li class="list-group-item d-flex justify-content-between align-items-center px-0">
            Coupon Code
            <span>- £{{$cart->getDiscountValue()}}</span>
        </li>
    @endif
    <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 mb-3">
        <div>
            <strong>Total</strong>
        </div>
        <span><strong>£{{$cart->getTotal()}}</strong></span>
    </li>
</ul>
@if(isset($isSavedCardExists) && $isSavedCardExists)
    <h5 class="mb-3">Payment Method</h5>
    <div class="form-check mb-3">
        <input type="checkbox" class="form-check-input" id="storedcard" name="use_saved_card" checked>
        <label class="form-check-label" for="storedcard">Use stored card ending in {{$savedCard4digits}} for this transaction.</label>
    </div>
@endif
<div class="form-check mb-2">
    <input class="form-check-input" name="agree" type="checkbox" id="checkbox-agree"
           required>
    <label class="form-check-label" for="checkbox-agree">
        <strong>I have read and agree to the terms outlined in both the privacy
            policy
            and terms and conditions of the competitions.</strong>
    </label>
</div>
<button type="submit" class="btn_1 full-width cart">PROCEED TO PAYMENT</button>
</form>
