
<div class="container margin_60_35">
    <div class="main_title">
        <h2>Our Gassed Winners</h2>
        <p class="mt-2">Here are just a few of our lucky competition winners!</p>
    </div>
    <div class="owl-carousel owl-theme winner_carousel">
        @foreach($winners as $winner)
        <div class="item">
            <div class="grid_item">
                <figure>
                    <img class="owl-lazy" src="{{$winner->present()->main_image}}" data-src="{{$winner->present()->main_image}}" alt="{{$winner->username}} Winner">
                </figure>
            </div>
            <!-- /grid_item -->
        </div>
        <!-- /item -->
        @endforeach
    </div>
    <!-- /products_carousel -->
    <!-- TrustBox widget - Carousel -->
    <div class="trustpilot-widget mt-4" data-locale="en-GB" data-template-id="53aa8912dec7e10d38f59f36" data-businessunit-id="5fa3f532263f0b0001b6ac27" data-style-height="140px" data-style-width="100%" data-theme="light" data-stars="4,5" data-review-languages="en">
      <a href="https://uk.trustpilot.com/review/gasmonkeycompetitions.co.uk" target="_blank" rel="noopener">Trustpilot</a>
    </div>
    <!-- End TrustBox widget -->
</div>
<!-- /container -->
