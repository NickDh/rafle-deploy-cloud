@extends('core.base')

@section('content')

    <div class="top_banner">
        <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0.3)">
            <div class="container">
                <h1>Past Winners</h1>
            </div>
        </div>
    </div>
    <!-- /top_banner -->

    <div class="container margin_60_35 winner-grid">
        <div class="row">
            @foreach($winners as $winner)
            <div class="col-md-4 col-xl-4 p-4">
                <figure>
                    <img class="img-fluid lazy" src="{{$winner->present()->main_image}}" alt="{{$winner->competition}} Winner">
                </figure>
                <div class="title">
                    <h3>{{$winner->competition}}</h3>
                </div>
                <div class="row align-items-center">
                    <div class="col-8 winner-name">
                        {{$winner->username}}
                    </div>
                    <div class="col-4 winner-ticket">
                        <img src="img/winner-ticket.svg" width="20">
                        {{$winner->ticket_number}}
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <!-- /row -->
        @include('parts/_pagination', ['paginator' => $winners])
    </div>

@endsection
