{{-- View for element Webmagic\Dashboard\Pages\LoginPage --}}
@extends('dashboard::core.adminlte_base')

@section('title', $title)

@section('body_class', 'hold-transition login-page ')

@section('base_content')
    <div class="login-box" style="margin:7% auto">
        <div class="login-logo">
            <a href="{{$logo_link}}"><img src="{{asset('img/raffle-labs.svg')}}" alt="{{$title}}"></a>
        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">@lang('dashboard::common.login_page.description')</p>
                <form class="  " action="{{route('login')}}" method="POST" data-send-all-checkbox="false">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="POST">

                    <div class="form-group clearfix">
                        <label for="5eb2926c6ee91">Login</label>
                        <span class="">
                            <input id="5eb2926c6ee91" class="form-control" type="text" placeholder="" value="" name="login" required="required"/>
                        </span>
                    </div>
                    <div class="form-group clearfix">
                        <label for="5eb2926c6f661">Password</label>
                        <span class="">
                            <input id="5eb2926c6f661" class="form-control" type="password" placeholder="" value="" name="password" required="required"/>
                        </span>
                    </div>
                    <input type="hidden" name="recaptcha" id="recaptcha">
                    <div class="form-group row">
                            <div class="col-xs-8" data-original-title="" title="">
                                <div class="mt-1" data-original-title="" title="">
                                    <input type="checkbox" id="remember" class="checkbox" name="remember" data-original-title="" title="">
                                    <label for="remember" class="checkbox-lbl" data-original-title="" title=""></label>

                                    <label for="remember" data-original-title="" title="">Remember Me</label>
                                </div>
                            </div>
                            <div class="col-xs-4" data-original-title="" title="">
                                <span class="">
                                    <button type="submit" class="btn btn-flat  btn btn-primary pull-right ml-2">
                                        Log in
                                    </button>
                                </span>
                            </div>

                    </div>
                    @if ($errors->has('username'))
                        <div id="form-status" class="text-danger">{{ $errors->first('username') }}</div>
                    @endif
                    @if ($errors->has('recaptcha'))
                        <div id="form-status" class="text-danger">{{ $errors->first('recaptcha') }}</div>
                    @endif
                </form>

                @if($register_link)<a href="{{$register_link}}">I forgot my password</a><br>@endif
                @if($forgot_password_link)<a href="{{$forgot_password_link}}" class="text-center">Register a new membership</a>@endif

            </div>
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
@endsection
@push('after-scripts')
    @include('parts/_recaptcha')
@endpush
<style>
    .login-page.skin-red .wrapper {
        background-color: #d2d6de;
    }
</style>
