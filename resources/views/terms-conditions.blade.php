@extends('core.base')

@section('content')

<main>
    <div class="top_banner">
        <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0.3)">
            <div class="container">
                <h1>Terms & Conditions</h1>
            </div>
        </div>
    </div>
    <!-- /top_banner -->
    <div class="container margin_60_35 account_wrap">
        <div class="row">
            <div class="col-md-12">
            	<p>
				    G&amp;R Automotive Ltd T/A GasMonkey Competitions
				</p>
				<p>
				    registered in England company No: 12835503
				</p>
				<p>
				    <strong>STANDARD COMPETITION TERMS </strong>
				</p>
				<p>
				    1. Qualifying Persons
				</p>
				<p>
				    1.1 G&amp;R Automotive Ltd t/a Gasmonkey competitions (‘Promoter’,
				    ‘our(s)’) operates various skill based competitions resulting in the
				    allocation of prizes in accordance with these terms and conditions on the
				    website (‘Prize’ or ‘Prizes’) www.gasmonkeycompetitions.co.uk (the
				    ‘Website’) – (each and all such competitions being referred to herein
				    respectively as the ‘Competition’ or ‘Competitions’).
				</p>
				<p>
				    1.2 Competitions are open to persons aged 16 or over anyone where in the
				    world. Employees of the Promoter or any person connected with the Promoter
				    (through family, professional or commercial association) are restricted
				    from participating in the Competition.
				</p>
				<p>
				    1.3 The Promoter reserves the right to close a Customer’s account at any
				    time, if they feel the Customer is abusing the services, being abusive to
				    other Customers or staff or they have the belief that it is not genuinely
				    the Customer that is entering.
				</p>
				<p>
				    2. Legal Undertaking
				</p>
				<p>
				    2.1 By entering a Competition the entrant (‘Entrant’, ‘you’, ‘your(s)’
				    and/or ‘Customer’) will be deemed to have legal capacity to do so, you will
				    have read and understood and accepted these terms and conditions and you
				    will be bound by them and by any other requirements set out in any of the
				    Promoter’s related promotional material.
				</p>
				<p>
				    2.2 Competitions are governed by English law and all and/or any matters or
				    disputes relating to the Competition will be dealt with and/or resolved
				    under English Law and the Courts of England shall have exclusive
				    jurisdiction.
				</p>
				<p>
				    2.3 In the event that you participate in a Competition online via the
				    Website, and by accepting these terms and conditions you hereby confirm
				    that you are not breaching any laws in your country of residence regarding
				    the legality of entering our Competitions. The Promoter will not be held
				    responsible for any Entrant entering any of our Competitions unlawfully. If
				    in any doubt you should
				</p>
				<p>
				    immediately leave the Website and check with the relevant authorities in
				    your country.
				</p>
				<p>
				    3. Competition Entry
				</p>
				<p>
				    3.1 Competitions may be entered via the Website
				    gasmonkeycompetitions.co.uk. Multiple Competitions may be operated at the
				    same time by the Promoter and each Competition will have a specific Prize.
				</p>
				<p>
				    3.2 Availability and pricing of Competitions is at the discretion of the
				    Promoter and will be specified at the point of sale on the Website. There
				    is no requirement to pay to enter any Competition, as each Competition has
				    a free entry route available – see rule below for details of how to enter
				    for free. The availability of a free entry route to enter each Competition
				    means that the Competition does not fall within the definition of a lottery
				    under the Gambling Act 2005 and can be operated legally in Great Britain
				    without any need for a licence.
				</p>
				<p>
				    3.3 In order to enter a Competition, you will need to register an account
				    with us. You can register an account online. To register an account online
				    you will be asked to provide an email address or sign in via a social media
				    account, such as Facebook, Twitter or Gmail (‘Social Media Account’)
				</p>
				<p>
				    3.4 When playing a Competition online via the Website, you must follow the
				    on- screen instructions to: (a) select the Competition you wish to enter
				    and when you are ready to purchase your ticket(s) to the Competition
				    (‘Tickets’), provide your contact and payment details. You will need to
				    check your details carefully and tick the declaration, confirming you have
				    read and understood the Competition terms and conditions; (b) once you have
				    purchased your Tickets, register to play the relevant Competition and when
				    your payment has cleared we will then contact you by email to confirm your
				    entry into the Competition. Please note that when entering online and/or by
				    post you will not be deemed entered into the Competition until we confirm
				    your entry which can be confirmed in your account when you login (and any
				    such entry referred to herein as an ‘Entry’ or ‘Entries’).
				</p>
				<p>
				    3.5 The Promoter reserves the right to refuse or disqualify any incomplete
				    Entry if it has reasonable grounds for believing that an Entrant has
				    contravened any of these terms and conditions.
				</p>
				<p>
				    3.7 To the extent permitted by applicable law, all Entries become the
				    Promoter’s property and will not be returned.
				</p>
				<p>
				    3.8 The Entrant can enter each Competition up to a maximum of 25 times.
				</p>
				<p>
				    3.8 (a) Each account can have unlimited amount of entries, providing they
				    are purchased on that account on behalf of other people.
				</p>
				<p>
				    3.9 Each Competition closes when the last number is taken, no more Entries
				    after this point will be accepted.
				</p>
				<p>
				    3.10 All Entrants (including those entering for free) must create an
				    account prior to entering and supply an email address to proceed in the
				    Competition.
				</p>
				<p>
				    4. Promotion Periods
				</p>
				<p>
				    4.1 Each Competition will run for a specified period. Please see each
				    Competition for details of start and end times and dates (‘Promotion
				    Period(s)’).
				</p>
				<p>
				    5. Competition Judgement
				</p>
				<p>
				    5.1 Gasmonkey competitions’ guaranteed 100% random ball spinning machine
				    will determine the winner of each Competition. However many Entrants have
				    entered the Competition will determine how many balls are entered into the
				    machine. Each Competition will have a minimum of 49 balls and a maximum of
				    100,000 balls. The result will be live streamed on Facebook Live (or such
				    other live streamed internet channel as the Promoter chooses).
				</p>
				<p>
				    5.2 Due to the nature of the selection, there will only be one Winner per
				    Competition, unless the Promoter states otherwise.
				</p>
				<p>
				    5.3 The Promoter will attempt to contact winners of Competitions (referred
				    to herein as ‘Winner(s)’) using the telephone numbers and email address
				    provided at the time of Entry (or as subsequently updated) and held
				    securely in our
				</p>
				<p>
				    database. It is the Entrant’s responsibility to ensure that these details
				    are accurate, up to date and complete. If for any reason these details are
				    taken down, inputted and/or submitted and/or recorded in any way by you
				    incorrectly, the Promoter will not be held responsible for any consequences
				    of this of whatever nature and howsoever arising. Entrants must carefully
				    check their contact details have been recorded correctly.
				</p>
				<p>
				    5.4 If for any reason the Promoter is unable to contact a Winner within 5
				    working days (which may be extended at the sole discretion of the Promoter)
				    of the end of a Competition, or the Winner fails for whatever reason or
				    cause to confirm acceptance of the Prize and/or the Winner is disqualified
				    as a result of not complying with or contravening any of these terms and
				    conditions, the Winner hereby agrees that it will immediately, irrevocably
				    and automatically forfeit the Prize and the Prize will remain in the
				    possession and ownership of the Promoter.
				</p>
				<p>
				    5.5 In the event that the Promoter closes a Competition early, the Winner
				    will be selected from all valid and eligible Entries received by the
				    Promoter prior to the date of closure, except that the Promoter reserves
				    the right, at its sole discretion, to close a Competition early without
				    selecting a Winner. In the event that a Competition is closed without
				    selecting a Winner, the Promoter will give all Entrants game credit to
				    enable them to replay equivalent Tickets in a subsequent Competition.
				</p>
				<p>
				    5.6 Entrants who specifically consent to marketing communications will be
				    entered onto the Promoter’s database for the purpose of conveying
				    information as to the status of their Competition, as well as any future
				    promotions or Competitions offered by the Promoter.
				</p>
				<p>
				    5.7 The Promoter also reserves the right at its sole discretion to extend
				    the closing date of any Competition. Each Competition can have the closing
				    time extended by the Promoter up to 4 times. If the Competition is not sold
				    out after the 4th extension of time, then the Prize that will be awarded
				    will be as follows:
				</p>
				<p>
				    70% of the value of paid Entries to the Competition. Only the Competition
				    Entrants (including free Entries) will be entered into this draw.
				</p>
				<p>
				    6. Winner’s Details
				</p>
				<p>
				    6.1 The Winner will be required to show proof of identification on delivery
				    of the Prize. Any failure to meet this requirement may result in the Winner
				    being disqualified and the Promoter retaining the Prize.
				</p>
				<p>
				    6.2 All Winners will be asked for their consent by the Promoter to provide
				    photographs and/or pose for photographs and videos and have their personal
				    details (including details of any Prize won by them) included in marketing
				    material. If a Winner consents to the above, the foregoing photographs,
				    videos and
				</p>
				<p>
				    marketing material may be used in future marketing and public relations by
				    the Promoter in connection with the Competition and in identifying them as
				    a Winner of a Competition.
				</p>
				<p>
				    6.3 Following receipt and verification of the details requested above by
				    the Promoter and provided that the Winner has satisfied these terms and
				    conditions, the Winners will be contacted in order to make arrangements for
				    delivery of the Prize.
				</p>
				<p>
				    7. Competition Prizes
				</p>
				<p>
				    7.1 The Prizes are determined, selected by all and/or some of the directors
				    of the company and are owned by the Promoter from the date of the
				    Competition going live on the Website to the date that the Winner receives
				    the Prize. Details of each Prize can be found on the Website on the
				    Competitions pages. Gasmonkey Competitions take no responsibility for the
				    Prize awarded after delivery has taken place. Once the Winner receives the
				    Prize, the Promoter does not insure the Prize. No insurance comes with the
				    Prizes and the Promoter is not responsible for the Prize once it has been
				    handed over to the Winner.
				</p>
				<p>
				    7.2 Delivery of the Prize to the Winner’s home address in Great Britain is
				    free. The Promoter has a right to and/or may charge the Winner delivery
				    fees if they require the Prize to be delivered to an address outside Great
				    Britain.
				</p>
				<p>
				    7.3 All Entrant expenses to collect the Prize are the sole responsibility
				    of the Winner.
				</p>
				<p>
				    7.4 The Winner hereby agrees that all Prizes are subject to and are
				    conditional on the terms and conditions of the Promoter, Prize provider,
				    manufacturer and/or supplier and/or anyone that is involved in the
				    provision or delivery of the Prize to the Winner.
				</p>
				<p>
				    7.5 Each Prize must be accepted as awarded and is non-transferable or
				    convertible to other substitutes and cannot be used in conjunction with any
				    other vouchers, offers or discounts, including without limitation any
				    vouchers or offers of the Promoter or other Prize suppliers and/or third
				    parties.
				</p>
				<p>
				    8. Storage
				</p>
				<p>
				    The Promoter can store the chosen Prize free of charge for up to 15 days
				    after notifying the Winner, at the end of which time the Prize will be
				    delivered to the Winner. If the Prize needs to be stored by the Promoter
				    for more than 15 days then this shall be at the entire cost of the Winner
				    where such cost will need to be paid by the Winner to the Promoter before
				    the Winner receives the Prize.
				</p>
				<p>
				    9. Winners’ Personal Data
				</p>
				<p>
				    9.1 Subject to the Winner’s consent, the Winner may be asked to have their
				    photo and video taken by the Promoter for promotional purposes (Public
				    Relations and Marketing).
				</p>
				<p>
				    9.2 When entering a Competition, the Entrant will be asked to consent to
				    use of their name, address, and/or photograph or other likeness, as well as
				    your appearance at publicity events without any additional compensation
				    (save for reasonable travel expenses that are approved in writing in
				    advance by the Promoter) and as required by the Promoter.
				</p>
				<p>
				    10. Limits of Liability
				</p>
				<p>
				    10.1 The Promoter makes or gives no representations and/or warranties
				    and/or assurances of whatever nature and howsoever arising (and whether in
				    writing or otherwise) as to the quality, suitability and/or fitness for any
				    particular purpose of any of the goods or services advertised, offered
				    and/or provided as Prizes. Except for liability for death or personal
				    injury caused by the negligence of the Promoter and/or for any fraudulent
				    misrepresentations and/or for any events and/or circumstances to the extent
				    that they cannot be excluded or limited by law. The Promoter shall not be
				    liable for any loss suffered or sustained to person or property including,
				    but not limited to, consequential (including economic) loss by reason of
				    any act or omission by the Promoter, or its servants or agents, in
				    connection with the arrangement for supply, or the supply, of any goods by
				    any person to the Prize Winner(s) and, where applicable, to any
				    family/persons accompanying the Winner(s), or in connection with any of the
				    Competitions promoted by the Promoter.
				</p>
				<p>
				    10.2 The total maximum aggregate liability of the Promoter to each Winner
				    shall be limited to the total value of each Prize that has been won by the
				    relevant Winner.
				</p>
				<p>
				    10.3 The total maximum aggregate liability of the Promoter to you shall (if
				    you are not a Winner) be limited to the amount that you have paid to enter
				    Competitions in the first 12 months of you playing any Competition.
				</p>
				<p>
				    10.4 Nothing in these terms and conditions shall prevent you making claims
				    to the extent that you are exercising your statutory rights.
				</p>
				<p>
				    11. Electronic Communications No responsibility will be accepted by the
				    Promoter for failed, partial or garbled computer transmissions, for any
				    computer, telephone, cable, network, electronic or internet hardware or
				    software malfunctions, failures, connections, availability, for the acts or
				    omissions of any service provider, internet accessibility or availability
				    or for traffic congestion or unauthorised human act, including any errors
				    or mistakes. The Promoter shall use its reasonable endeavours to award the
				    Prize for a Competition to the correct Entrant. If due to reasons of
				    hardware, software or other computer related failure,
				</p>
				<p>
				    or due to human error, the Prize is awarded incorrectly, the Promoter
				    reserves the right to reclaim the Prize and award it to the correct
				    Entrant, at its sole discretion and without admission of liability and the
				    Entrant that has been incorrectly awarded the Prize will immediately at the
				    Entrant’s own cost and expense return it to the Promoter and/or pay the
				    Promoter for that Prize (at the option of the Promoter). The Promoter shall
				    not be liable for any economic and/or other loss and/or consequential loss
				    suffered or sustained to any persons to whom an award has been incorrectly
				    made, and no compensation shall be due to such persons. The Promoter shall
				    use its reasonable endeavours to ensure that the software and Website(s)
				    used to operate its Competitions performs correctly and accurately across
				    the latest versions of popular internet, tablet and mobile browsers. For
				    the avoidance of doubt, only the Ticket recorded in our systems, howsoever
				    displayed or calculated, shall be entered into the relevant Competition and
				    the Promoter shall not be held liable for any Competition Entries that
				    occur as a result of malfunctioning software or other event.
				</p>
				<p>
				    12. Data Protection Notice Any personal data that you supply to the
				    Promoter or authorise the Promoter to obtain from a third party, for
				    example, a credit card company, will be used by the Promoter to administer
				    the Competition and fulfil Prizes where applicable. In order to process,
				    record and use your personal data the Promoter may disclose it to (i) any
				    credit card company whose name you give; (ii) any person to whom the
				    Promoter proposes to transfer any of the Promoter’s rights and/or
				    responsibilities under any agreement the Promoter may have with you; (iii)
				    any person to whom the Promoter proposes to transfer its business or any
				    part of it; (iv) comply with any legal or regulatory requirement of the
				    Promoter in any country; and (v) prevent, detect or prosecute fraud and
				    other crime. In order to process, use, record and disclose your personal
				    data the Promoter may need to transfer such information outside the United
				    Kingdom, in which event the Promoter is responsible for ensuring that your
				    personal data continues to be adequately protected during the course of
				    such transfer.
				</p>
				<p>
				    13. To enter any Competition draw each month for free, you must first
				    create an account and then send your name, address, date of birth, e- mail
				    address and contact telephone number on a postcard to the Promoter Marked
				    Free entries and posted to: Gasmonkey Competitions, 19 Juliet drive,
				    Brackley, Northants NN136GJ and must arrive by the Competition closing
				    date. First or second-class postage must be paid. Postal Entries are
				    limited to one Entry per person to each Competition, each entry is to be
				    submitted individually by postcard. The Entrant must specify which
				    Competition they wish to enter. Random number/s will be allocated to each
				    free entry by the Promoter. All free Entries will be treated in the exact
				    same way as a paid Entries. Where applicable, these Competition terms and
				    conditions also apply to free Entries. All free entries are processed on
				    Tuesday’s of each week.
				</p>
				<p>
				    14. Validation
				</p>
				<p>
				    14.1 The Promoter hereby reserves the right not give or make a Prize until
				    it is satisfied that (a) the Winner has a validly registered Website
				    account and/or is not in breach of these terms and conditions, (b) any
				    and/or all amounts due or owing by you to the Promoter have been paid in
				    full, (c) the identity of the Winner and his or her entitlement to receive
				    the Prize has been established to the Promoter’s satisfaction (in
				    particular, the Promoter reserves the right to request documentary proof of
				    identity and not to give or make a Prize until satisfied appropriate proof
				    of identity has been provided), and (d) the Promoter may require proof of
				    age to be produced before giving or making a Prize. Prizes will not be
				    given or made to Winners found to be under below the age of 16.
				</p>
				<p>
				    14.2 Without prejudice to rule 14.1 above, the Promoter reserves the right
				    not to make or give a Prize if it reasonably suspects the occurrence of
				    fraud in relation to a Competition and/or Elite Club draw.
				</p>
				<p>
				    14.3 The Promoter may, at its absolute and sole discretion, give or make a
				    Prize to a person whom it is satisfied is the duly authorised
				    representative of the Winner acting under a lawfully executed power of
				    attorney or other equivalent authorisation.
				</p>
				<p>
				    15. Your account
				</p>
				<p>
				    15.1 You must keep your account password secure and secret at all times and
				    take steps to prevent it being used without your permission. You must (a)
				    memorise your password and never tell it to anyone, (b) never write your
				    password down (including on your computer or other electronic device) or
				    record it in a way that can be understood by someone else, (c) destroy any
				    communications from the Promoter in relation to your password as soon as
				    you have read them, (d) avoid using a password that is easy to guess, (e)
				    ensure that no-one else (apart from you) uses your account while you and/or
				    your devices are logged on to the Website (including by logging on to your
				    devices through a mobile, Wi-Fi or shared access connection they are
				    using), (f) log off or exit from your account when not using it, and (g)
				    keep your password or other access information secret,
				</p>
				<p>
				    15.2 Your password and log in details are personal to you and should not be
				    given to anyone else and/or used to provide shared access e.g. over a
				    network. You must use a password which is unique to your account, and
				    maintain good internet security
				</p>
				<p>
				    15.3 You must contact the Promoter immediately if you believe, suspect or
				    know that anyone apart from you has used your account and/or given any
				    instruction in relation to it without your permission, or if you believe,
				    suspect or know someone else knows your password.
				</p>
				<p>
				    15.4 If you forget your password, you can reset it by following the
				    instructions on the Website (as long as you can provide the relevant
				    security information requested or required by the Promoter).
				</p>
				<p>
				    15.5 The Promoter shall not be responsible and/or liable for any and/or all
				    consequences arising out of and/or relating to any and/or all breaches of
				    this rule 15.5 by you. Furthermore, the Promoter shall not in any event be
				    responsible and/or liable for any actions and/or inactions that you may
				    take and/or consequences that you may suffer and/or incur as a result of
				    using and/or in connection with the Website.
				</p>
				<p>
				    16. Use of the Website
				</p>
				<p>
				    16.1 You hereby agree that (a) the Website, the Competitions draws are for
				    your own personal, non-commercial use, and (b) you are only allowed to use
				    your account and the Website, enter Competitions draws via your account, as
				    set out in these terms and conditions.
				</p>
				<p>
				    16.2 You hereby agree to indemnify the Promoter against any and/or all
				    costs, losses, damages and expenses which the Promoter may suffer and/or
				    incur arising out of and/or in relation to any claim, legal proceeding
				    and/or demand made by any third party due to and/or arising out of your
				    unlawful, wrongful and/or negligent access and/or use of your account, the
				    Website and/or the Promoter’s software and/or IT systems, and/or breach by
				    you of these terms and conditions.
				</p>
				<p>
				    16.3 There is no guarantee that the Website will display correctly on all
				    devices it can be viewed on.
				</p>
				<p>
				    16.4 The Promoter is the owner or licensee of all the copyright, trademarks
				    and other intellectual property rights in, to and in respect of the
				    Competitions, and the Website, and you will not acquire any rights in any
				    of these.
				</p>
				<p>
				    16.5 Trademarks, service marks, logos, trade names, source identifiers
				    and/or proprietary designations (“Trademarks”) of the Promoter used on
				    and/or in connection with the Website, the Competitions are trademarks of
				    the Promoter. Trademarks of third parties used on and/or in connection with
				    the Website are used for identification purposes only and may be the
				    property of their respective owners.
				</p>
				<p>
				    16.6 You must not (a) copy, disclose, transmit and/or otherwise make
				    available and/or remove or change any material available on the Website,
				    (b) reverse engineer or decompile (whether in whole or in part) any
				    software used in connection with the Website and/or the provision of the
				    Competitions.
				</p>
				<p>
				    17. Discretion The exercise by the Promoter of any discretion provided for
				    in these terms and conditions will be final and binding.
				</p>
				<p>
				    18. General
				</p>
				<p>
				    18.1 If the Promoter fails and/or delays to enforce a provision of the
				    terms and conditions, this failure and/or delay is not a waiver of the
				    Promoter’s right to do so later on.
				</p>
				<p>
				    18.2 If any provision (or part of a provision) of these terms and
				    conditions is decided by a court of competent jurisdiction to be void
				    and/or unenforceable, that decision will only affect the particular
				    provision (or part of the provision) and will not, in itself, make the
				    other provisions void or unenforceable.
				</p>
				<p>
				    18.3 These terms and conditions constitute the entire agreement between you
				    and the Promoter regarding the subject matter of these terms conditions and
				    supersede and replace any other prior and/or contemporaneous agreements,
				    and/or terms and conditions applicable to the subject matter of these terms
				    and conditions.
				</p>
				<p>
				    18.4 A person who is not a party to these terms and conditions has no
				    rights under the Contracts (Rights of Third Parties) Act 1999 (as amended
				    or re-enacted from time to time, and any subordinate legislation made under
				    that act) or otherwise to enforce any provision of these terms and
				    conditions.
				</p>
				<p>
				    18.5 The Promoter will not be liable for any delay or failure to perform
				    any obligation under these terms and conditions where the delay or failure
				    results from any cause beyond its reasonable control, including acts of
				    God, labour disputes or other industrial disturbances, electrical or power
				    outages, utilities or other telecommunication/network failures, earthquake,
				    storms, or other elements of nature, blockages, embargoes, riots, acts or
				    orders of government, acts of terrorism, or war.
				</p>
				<p>
				    18.6 The Website may contain hyperlinks to websites operated by parties
				    other than us. Such hyperlinks are provided for your reference and
				    convenience only. We do not control such websites and are not responsible
				    for their content and/or the privacy or other practices of such websites.
				    It is up to you to take precautions to ensure that whatever links they
				    select and/or software you download from such websites are free of viruses.
				    Our inclusion of hyperlinks to such websites does not imply any endorsement
				    of the material on such websites, association, sponsorship and/or
				    partnership with their operators. You must not create a text hyperlink to
				    the Website without our prior written consent.
				</p>
				<p>
				    18.7 Gasmonkey Competitons may send a list of entries with names and
				    surnames to all entrants via Facebook, email or it’s website.
				</p>
				<p>
				    Promoter: G&amp;R Automotive Ltd T/A Gasmonkey Competitions,
				</p>
				<p>
				    118 Holly Road, Northampton, NN14QP
				</p>
				<p>
				    info@gasmonkeycompetitions.co.uk
				</p>
				<p>
				    Mob: 07785996506
				</p>
				<p>
				    Website: https://www.gasmonkeycompetitions.co.uk
				</p>
				</div>
        </div>
    </div>
</main>
<!-- /main -->
@endsection
