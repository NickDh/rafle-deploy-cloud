@extends('core.base')

@section('content')

@if(isset($order))
    @if($order->isSuccessful())
        <div class="container margin_60_35">
            <div class="row justify-content-center text-center">
                <div class="col-md-5">
                    <div id="confirm">
                        <div class="icon icon--order-success svg add_bottom_15">
                            <svg xmlns="http://www.w3.org/2000/svg" width="72" height="72">
                                <g fill="none" stroke="#8EC343" stroke-width="2">
                                    <circle cx="36" cy="36" r="35" style="stroke-dasharray:240px, 240px; stroke-dashoffset: 480px;"></circle>
                                    <path d="M17.417,37.778l9.93,9.909l25.444-25.393" style="stroke-dasharray:50px, 50px; stroke-dashoffset: 0px;"></path>
                                </g>
                            </svg>
                        </div>
                    <h2>Order Successful!</h2>
                    <p>A confirmational email is on it's way to you. You can find details about your order below or inside your account.</p>
                    </div>
                </div>
            </div>
            <!-- /row -->
            <div class="row order_wrap">
                <div class="col-md-12">
                    <div class="card shadow mb-3">
                        @include('orders.parts._order-items-details', ['order' => $order])
                    </div>
                </div>
                <div class="col-lg-12">
                    <!-- Card -->
                    <div class="card mb-3 shadow">
                    @include('orders.parts._order-details', ['order' => $order, 'btnLink' => route('order.details', $order->id)])
                    </div>
                    <!-- Card -->
                </div>
            </div>
        </div>
        <!-- /container -->
        @push('before-scripts')
        <script>
            window.dataLayer = window.dataLayer || [];
            dataLayer.push(@json($order->present()->googleTagManagerStructure));
        </script>
        @endpush 
    @else
        <div class="container margin_60_35">
            <div class="row justify-content-center text-center">
                <div class="col-md-5">
                    <div id="confirm" class="add_top_60 add_bottom_60">
                        <div class="icon icon--order-success svg add_bottom_15">
                            <svg  xmlns="http://www.w3.org/2000/svg" width="72" height="72">
                                <g fill="none" stroke="#E20707" stroke-width="2">
                                    <circle cx="36" cy="36" r="35" style="stroke-dasharray:240px, 240px; stroke-dashoffset: 480px;"></circle>
                                    <path d="M23.6,47.7l25.4-25.4" style="stroke-dasharray:50px, 50px; stroke-dashoffset: 0px;" />
                                    <path d="M23.8,22.3l25.4,25.4" style="stroke-dasharray:50px, 50px; stroke-dashoffset: 0px;" />
                                </g>
                            </svg>
                        </div>
                    <h2>Order Unsuccessful!</h2>
                    <p>Unfortunately, we could not process this order. This could be due to several different reasons, please try again and reconfirm your payment details.</p>
                    <a href="{{route('live-competitions')}}" class="btn_1">View Live Competitions</a>
                    </div>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    @endif
    @else
        <div class="container margin_60_35">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="add_top_60 add_bottom_60">
                    <h2>Your basket is empty!</h2>
                    <a href="{{route('live-competitions')}}" class="btn_1">View Live Competitions</a>
                    </div>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    @endif
@endsection
