@extends('core.base')

@section('content')

    @if($status === 'success')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <div id="confirm">
                    <div class="icon icon--order-success svg add_bottom_15">
                        <svg xmlns="http://www.w3.org/2000/svg" width="72" height="72">
                            <g fill="none" stroke="#8EC343" stroke-width="2">
                                <circle cx="36" cy="36" r="35" style="stroke-dasharray:240px, 240px; stroke-dashoffset: 480px;"></circle>
                                <path d="M17.417,37.778l9.93,9.909l25.444-25.393" style="stroke-dasharray:50px, 50px; stroke-dashoffset: 0px;"></path>
                            </g>
                        </svg>
                    </div>
                <h2>Order Successful!</h2>
                <p>A confirmational email is on it's way to you. You can find details about your order below or inside your account.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="{{route('order.details', $order->id)}}" class="btn_1 full-width">View Order Details</a>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
    @else
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <div id="confirm" class="add_top_60 add_bottom_60">
                    <div class="icon icon--order-success svg add_bottom_15">
                        <svg  xmlns="http://www.w3.org/2000/svg" width="72" height="72">
                            <g fill="none" stroke="#E20707" stroke-width="2">
                                <circle cx="36" cy="36" r="35" style="stroke-dasharray:240px, 240px; stroke-dashoffset: 480px;"></circle>
                                <path d="M23.6,47.7l25.4-25.4" style="stroke-dasharray:50px, 50px; stroke-dashoffset: 0px;" />
                                <path d="M23.8,22.3l25.4,25.4" style="stroke-dasharray:50px, 50px; stroke-dashoffset: 0px;" />
                            </g>
                        </svg>
                    </div>
                <h2>Order Unsuccessful!</h2>
                <p>Unfortunately, we could not process this order. This could be due to several different reasons, please try again and reconfirm your payment details.</p>
                <a href="{{url('/live-competitions')}}" class="btn_1">View Live Competitions</a>
                </div>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
    @endif
@endsection
