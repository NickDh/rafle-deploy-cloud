<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Raffle Labs">
    <title>GasMonkey Competitions</title>
    <meta property="og:title" content="GasMonkey Competitions - Petrol Head & Lifestyle Prizes">
    <meta property="og:description" content="The home to petrol head and lifestyle prizes! View our live competitions today. ">
    <meta property="og:image" content="{{asset('img/meta-image.jpg')}}">

    <!-- Favicons-->
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="180x180" href="{{asset('img/apple-touch-icon.png')}}">
    
    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    {{--###Styles###--}}
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css?version=2020-11-07T13:37:37.840Z')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/custom.css?version=2020-11-07T13:37:37.840Z')}}">
    {{--###Styles###--}}

    @include('parts/_google-tag-manager')

    @stack('before-scripts')

    <!--Head all -->
    @if(!app()->environment('local'))
        @include('parts/_head-all')
    @endif
    <!-- End Head all -->
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-P87R399');</script>
    <!-- End Google Tag Manager -->

    <!-- TrustBox script -->
    <script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>
    <!-- End TrustBox script -->
</head>

<body class="{{$body_class}}">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P87R399"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

    <div class="page_wrapper">
        {{--Header--}}
        @include('parts/_header')

        {{--Content--}}
        @section('content')
        @show

        {{--Footer--}}
        @include('parts/_footer')
    </div>

    @include('parts/_banner-bottom')

{{--Forms--}}
@include('parts/_forms')

@stack('before-js-scripts')

{{--###Scripts###--}}
<script src="{{asset('js/libs.js?version=2021-01-28T12:34:42.753Z')}}"></script>
<script src="{{asset('js/script.js?version=2021-01-28T12:34:42.753Z')}}"></script>
{{--###Scripts###--}}

@stack('after-scripts')

<div id="_token-csrf" class="__hidden">{{csrf_token()}}</div>
@if(!app()->environment('local'))
    @include('parts/_body-all')
@endif
</body>
</html>
