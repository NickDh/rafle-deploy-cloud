@extends('core.base')

@section('content')

<div class="top_banner">
    <div class="opacity-mask d-flex align-items-center">
        <div class="container">
            <h1>Contact Us</h1>
        </div>
    </div>
</div>

<div class="container margin_60_35">
                <div class="row">
                <!--Grid column-->
                <div class="col-md-6">
                    <div class="card shadow">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <form action="{{route('contact.send')}}" method="POST" class="js_form-contacts">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <h3>Contact Form</h3>
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" class="form-control" name="name">
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" class="form-control" name="email" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Message</label>
                                            <textarea class="form-control" rows="3" name="message"></textarea>
                                        </div>
                                        <div class="form-group form-check">
                                            <input type="checkbox" class="form-check-input" id="gdpr" name="policy">
                                            <label class="form-check-label" for="gdpr">I consent in submitting my
                                                details inline with your privacy policy.
                                            </label>
                                        </div>
                                        <input type="hidden" name="recaptcha" id="recaptcha">
                                        <button class="btn_1 full-width" type="submit">Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-md-6">
                    <div class="card shadow">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>FAQ</h3>
                                    <div class="faq">
                                        <div class="question">HOW DO I PURCHASE TICKET NUMBERS?</div>
                                        <div class="answer">Playing is easy, first you need to create an account by clicking here. Once you aree logged into your account navigate to the competition you wish to enter, select your numbers or use our lucky dip, answer the question and proceed to checkout.</div>
                                        <div class="question">WHAT HAPPENS IF THE TIMER RUNS OUT AND ALL NUMBERS ARN'T SOLD?</div>
                                        <div class="answer">Dont worry,  we will always hold a draw for each competition,  we will keep draw date extensions to the minimum, See our <a href="{{route('static.terms-conditions')}}">Terms & Conditions</a> for full details.</div>
                                        <div class="question">HOW DO YOU DRAW THE WINNER OF THE COMPETITION?</div>
                                        <div class="answer">We use our specialist random ball machine in order to pick our competition winner. These draws can be watched live via our facebook page.</div>
                                    </div>
                                    <p class="mt-4"><a href="{{route('static.how-to-play')}}">View more FAQ and how to play instructions.</a></p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!--Grid column-->
            </div>
            <!--Grid row-->

@endsection
@push('after-scripts')
    @include('parts/_recaptcha')
@endpush
