@extends('core.base')

@section('content')

    <div class="top_banner">
        <div class="opacity-mask d-flex align-items-center">
            <div class="container">
                <h1>Basket</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row order_wrap add_top_60 pb-4 js_cart-wrap-cnt">
        @if(isset($cart) && $cart->getContent()->count())
            <!--Grid column-->
                <div class="col-lg-8">
                    <!-- Card -->
                    <div class="card shadow mb-3">
                        <div class="card-body">
                            <h2 class="mb-4">Competition Entries</h2>
                            @foreach($cart->getContent() as $competition_id => $item)
                                <div class="row mb-4 align-items-center js_item-wrap">
                                    <div class="col-md-5 col-lg-3 col-xl-3">
                                        <div class="mb-3 mb-md-0 text-center">
                                            <img class="img-fluid" src="{{$item['image']}}" alt="{{$item['title']}}">
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-lg-9 col-xl-9">
                                        <div>
                                            <div class="d-flex justify-content-between">
                                                <div>
                                                    <h3>{{$item['title']}}</h3>
                                                    <p class="mb-2 text-muted text-uppercase small">Answer
                                                                                                    : {{$item['answer']}}</p>
                                                    <p class="mb-2 text-muted text-uppercase small">Entries
                                                                                                    : {{$item['ticket_number']}}</p>
                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div>
                                                    <a href="#" type="button"
                                                       data-action="{{route('cart.delete', $competition_id)}}"
                                                       data-amount="{{$item['qty']}}"
                                                       class="card-link-secondary small text-uppercase mr-3 js_cart-delete-item">
                                                        <i class="fas fa-trash-alt mr-1"></i> Remove entry </a>
                                                </div>
                                                <p class="mb-0"><span><strong>£{{formatValue($item['price'])}}</strong></span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="mb-4">
                            @endforeach
                        </div>
                    </div>
                    <!-- Card -->
                </div>
                <div class="col-lg-4">
                    <!-- Card -->
                    <div class="card mb-3 shadow">
                        <div class="card-body js_cart-total">
                            @include('parts/_cart-total', [
                                                            'cart' => $cart,
                                                            'isSavedCardExists' => $isSavedCardExists,
                                                            'savedCard4digits' => $savedCard4digits
                                                            ])
                        </div>
                    </div>
                    <div class="card mb-3 shadow">
                        <div class="card-body">
                            <a class="dark-grey-text d-flex justify-content-between" data-toggle="collapse"
                               href="#coupon-tab"
                               aria-expanded="false" aria-controls="collapseExample1">
                                Add a discount code (optional)
                                <span><i class="fas fa-chevron-down pt-1"></i></span>
                            </a>
                            <div class="collapse" id="coupon-tab">
                                <div class="mt-3">
                                    <form action="{{route('cart.check-coupon')}}" method="POST"
                                          class="js_form-promocode">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <div class="input-group mb-3">
                                            <input type="text" name="name" class="form-control"
                                                   placeholder="Discount Code" required>
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-secondary" type="submit">Apply</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="col-md-12">
                    <h4>Your basket is empty.</h4>
                    <a href="{{route('live-competitions')}}" class="btn_1">View Live Competitions</a>
                </div>
            @endif
        </div>
    </div>

@endsection
