
<div class="card-body">
    <h5 class="mb-3">Order Details</h5>
    <ul class="list-group list-group-flush">
        <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
            Order Number
            <span>#{{$order->id}}</span>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
            Date
            <span>{{$order->present()->prettyCreatedDate()}}</span>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
            Entries
            <span>£{{$order->present()->subtotal()}}</span>
        </li>
        @if($order->isDiscountCodeApplied())
            <li class="list-group-item d-flex justify-content-between align-items-center px-0">
                Coupon Code
                <span>- £{{$order->present()->discount()}}</span>
            </li>
        @endif
        <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 mb-3">
            <div>
                <strong>Total</strong>
            </div>
            <span><strong>£{{$order->present()->total()}}</strong></span>
        </li>
    </ul>
    <a href="{{route('orders')}}" class="btn_1 full-width">Return To Account</a>
</div>