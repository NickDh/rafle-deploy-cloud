@extends('core.base')

@section('content')

    <!-- Content starts -->
    <section class="content_wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="section_title v2">
                        <h2>Results</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($results as $result)
                    <div class="col-lg-3 col-md-4">
                        <div class="card shadow mb-3">
                        <div class="card-body single_results_item">
                            <img src="{{$result->present()->image}}" alt="Image">
                            <h4>{{$result->getFormattedData()}}</h4>
                            <a href="{{route('result-details', $result->id)}}" class="btn winner_btn">View All Results</a>
                        </div>
                    </div>
                    </div>
                @endforeach
            </div>
            @include('parts/_pagination', ['paginator' => $results])
        </div>
    </section>
    <!-- Content ends -->

    @include('parts/_promo-blk')

@endsection
