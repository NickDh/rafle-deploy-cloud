@extends('core.base')

@section('content')

<div class="top_banner">
    <div class="opacity-mask d-flex align-items-center">
        <div class="container">
            <h1>Forgot Password</h1>
        </div>
    </div>
</div>
<!-- /top_banner -->
<div class="container margin_30">
    <!-- /page_header -->
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-8 offset-md-3">
            <div class="box_account">
                <div class="form_container">
                    <h2>Reset Password</h2>
                    <form action="{{route('password.email')}}" method="post" class="js_form-forgot-password">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" placeholder="Email Address" name="email" required="required">
                    </div>
                    <div class="text-center"><button type="submit" class="btn_1 full-width">Reset Password</button></div>
                    </form>
                </div>
                <!-- /form_container -->
                <p class="float-right pt-2">No account yet? <a href="{{route('user.register')}}">Create one here.</a></p>
            </div>
            <!-- /box_account -->
        </div>
    </div>
</div>
<!-- /container -->

@endsection
