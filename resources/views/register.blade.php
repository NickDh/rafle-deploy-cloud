@extends('core.base')

@section('content')
    <div class="top_banner">
        <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0.3)">
            <div class="container">
                <h1>Create Account</h1>
            </div>
        </div>
    </div>
    <!-- /top_banner -->
    <div class="container margin_30">
    <!-- /page_header -->
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-8 offset-md-3">
                <div class="box_account">
                    <div class="form_container">
                        <div class="row">
                            <div class="col-md-6">
                                <h2>Login Details</h2>
                            </div>
                            <div class="col-md-6">
                                <p class="text-right pt-2"><small>* Required Fields</small></p>
                            </div>
                        </div>
                        <form action="{{url('register')}}" method="POST" class="form-login">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="first_name" placeholder="First Name" value="{{ old('first_name') }}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="last_name" placeholder="Last Name" value="{{ old('last_name') }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="username" placeholder="Username"  value="{{ old('username') }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Password" name="password" required>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <h2>Account Details</h2>
                        <div class="private box">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="number" min="0" class="form-control" placeholder="Phone" name="phone" value="{{ old('phone') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="First Line Address" name="address" value="{{ old('address') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="City/Town" name="town" value="{{ old('town') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Post Code" name="post_code" value="{{ old('post_code') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group wrap-checkbox">
                                        <input class="form-checkbox" type="checkbox" name="subscription" id="assent">
                                        <label for="assent"><i class="checked-ic"></i><span>Keep me updated with all the news, special offers and discount codes</span></label>
                                    </div>
                                </div>
                                <input type="hidden" name="recaptcha" id="recaptcha">
                            </div>
                            <!-- /row -->
                        </div>
                            @if($errors->any())
                                @foreach ($errors->all() as $error)
                                    <div id="form-status" class="with_error">{{ $error }}</div>
                                @endforeach
                            @endif
                        <hr>
                        <div class="text-center"><button type="submit" class="btn_1 full-width">Register</button></div>
                    </div>
                    <!-- /form_container -->
                    <p class="float-right pt-2">Already have an account? <a href="{{route('user.login')}}">Login here.</a></p>
                </div>
                <!-- /box_account -->
            </div>
        </div>
    </div>
    <!-- /container -->

@endsection
@push('after-scripts')
    @include('parts/_recaptcha')
@endpush
