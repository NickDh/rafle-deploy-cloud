@extends('core.base')

@section('content')

<main>
    <div class="top_banner">
        <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0.3)">
            <div class="container">
                <h1>Privacy & Cookie Policy</h1>
            </div>
        </div>
    </div>
    <!-- /top_banner -->
    <div class="container margin_60_35 account_wrap">
        <div class="row">
            <div class="col-md-12 privacy-text">    
                    <p>This policy details he obligations of G&R Automotive Ltd t/a Gasmonkey Competitions regarding data protection and your rights under current EU Regulations “General Data Protection Regulations.” (GDPR)</p>

                    <p>Your data will be processed, lawfully, fairly, and transparently and only collected for specific, explicit, and legitimate purposes, and not processed further for any incompatible purposes other than the original purpose for collection.</p>

                    <p>In this Privacy notice, “Personal data” means any information relating to an individual who can be identified, directly or indirectly, in particular by reference to an identifier such as names, an identification number such as a National Insurance number, location data or an online identifier such as an IP address.</p>

                    <p>Personal data can also refer to one or more factors specific to the physiological, genetic, mental economic, cultural, or social identity of an individual.</p>

                    <h3>1. Purposes for which we collect, and process resonates data.</h3>

                    <p>We operate and run competitions which can be entered by any member of the UK public who is 18 years or older.</p>

                    <p>The data we collect depends upon the level of involvement you have on our website, and we only process personal data for the purpose for which it was collected.</p>

                    <p>There are two different user groups when visiting our website 1) account holders and 2) visitors. These two different user groups require two different levels of data collection.</p>

                    <p><strong>Account Holders</strong></p>

                    <p>Information which you provide to us voluntarily when creating an account will include:</p>

                    <ul><li>Full Name</li><li>Email Address</li><li>Contact information such as mobile number, email address and other telephone numbers.</li><li>Demographic information such as first line of address, post code and county</li></ul>

                    <p>Such information is provided by you on a voluntary basis and you acknowledge and agree that such information may be processed by us.</p>

                    <p>This information will be stored within our database throughout the duration of your account being active. You have the right to request an audit of the information we hold as well as requesting deletion of this information where your account will be deleted, and your information removed from our records (please see section 5).</p>

                    <p><strong>Visitors</strong></p>

                    <p>The data we collect from our visitors will be in the form of cookies. To understand more please view the cookie policy below.</p>

                    <p><strong>Cookies</strong></p>

                    <p>What are these cookies? Cookies are small files that a site or its service provider transfers to your computer’s hard drive through your Web browser (if you allow) that enables the site’s or service provider’s systems to recognize your browser and capture and remember certain information.</p>

                    <p><strong>Website Analytics</strong></p>

                    <p>We use a third party service, Google Analytics, to collect standard internet log information and details of visitor behaviour patterns. We do this to find out things such as the number of visitors to the various parts of the site. This information is only processed in a way which does not identify anyone. We do not make, and do not allow Google to make, any attempt to find out the identities of those visiting our website.</p>

                    <p><strong>Advertising and Remarketing</strong></p>

                    <p>To better understand our marketing campaigns and our visitor behaviour we will be using marketing tracking tools such as Facebook Pixel and Google Tag Manager to 1) understand which competitions and ad visuals perform the best and 2) to retarget/remarket our visitors with further exclusive discounts or materials. This will be done both using cookies for visitors and email addresses for our account holders.</p>

                    <p>Overall, we collect this information to improve the services and products/competitions supplied to you and it enables us to better understand the visitors to our site.</p>

                    <p>The purposes for which we collect and/or process your personal data as a visitor to our site;-</p>

                    <p>To assist in administering and managing our site.</p>

                    <ul><li>For site security for example to authenticate your identity and to prevent unauthorised access to the site.</li><li>To more personalise your visits to our site, so we may enhance your experience.</li><li>To analyse visitor data so as to enhance our marketing and other communications.</li><li>To understand which feature of the site visitors use.</li><li>To assist us in monitoring and enforcing all relevant regulations and applicable compliance.</li><li>To assist us in continual risk management assessment.</li></ul>

                    <p>Legal Grounds for processing personal data of visitors to the site;-</p>

                    <ul><li>For the effective and lawful operation of our business.</li><li>To improve and develop our site to enhance visitor experience.</li><li>Any matter for which we have been given your explicit consent.</li></ul>

                    <p>If you would like to know more about cookies please go to <a href="http://www.allaboutcookies.org">www.allaboutcookies.org</a></p>

                    <p>Other purposes we may collect personal data from you can be; –</p>

                    <table cellpadding="10" cellspacing="10"><tbody><tr><td>Purpose/Activity </td><td>Type of data </td><td>Lawful basis for processing including basis of legitimate interest </td></tr><tr><td>To register you as a new customer </td><td>(a) Identity (b) Contact </td><td>Performance of a contract with you </td></tr><tr><td>To process and deliver your order including: (a) Manage payments, fees and charges (b) Collect and recover money owed to us </td><td>(a) Identity (b) Contact (c) Financial (d) Transaction (e) Marketing and Communications </td><td>(a) Performance of a contract with you (b) Necessary for our legitimate interests </td></tr><tr><td>To manage our relationship with you which will include: (a) Notifying you about changes to our terms or Privacy policy (b) Asking you to leave a review or take a survey </td><td>(a) Identity (b) Contact (c) Profile (d) Marketing and Communications </td><td>(a) Performance of a contract with you (b) Necessary to comply with a legal obligation (c) Necessary for our legitimate interests (to keep our records updated and to study how customers use our products/services) </td></tr><tr><td>To enable you to partake in a prize draw, competition or complete a survey </td><td>(a) Identity (b) Contact (c) Profile (d) Usage (e) Marketing and Communications </td><td>(a) Performance of a contract with you (b) Necessary for our legitimate interests (to study how customers use our products/services, to develop them and grow our business) </td></tr><tr><td>To administer and protect our business and this website (including troubleshooting, data analysis, testing, system maintenance, support, reporting and hosting of data) </td><td>(a) Identity (b) Contact (c) Technical </td><td>(a) Necessary for our legitimate interests (for running our business, provision of administration and IT services, network security, to prevent fraud and in the context of a business reorganisation or group restructuring exercise) (b) Necessary to comply with a legal obligation </td></tr><tr><td>To deliver relevant website content and advertisements to you and measure or understand the effectiveness of the advertising we serve to you </td><td>(a) Identity (b) Contact (c) Profile (d) Usage (e) Marketing and Communications (f) Technical </td><td>Necessary for our legitimate interests (to study how customers use our products/services, to develop them, to grow our business and to inform our marketing strategy) </td></tr><tr><td>To use data analytics to improve our website, products/services, marketing, customer relationships and experiences </td><td>(a) Technical (b) Usage </td><td>Necessary for our legitimate interests (to define types of customers for our products and services, to keep our website updated and relevant, to develop our business and to inform our marketing strategy) </td></tr><tr><td>To make suggestions and recommendations to you about goods or services that may be of interest to you </td><td>(a) Identity (b) Contact (c) Technical (d) Usage (e) Profile (f) Marketing and Communications</td><td>Necessary for our legitimate interests (to develop our products/services and grow our business) </td></tr></tbody></table></figure>

                    <h3>2. Service Providers</h3>

                    <p>We may employ third party companies and individuals to facilitate our Service (“Service Providers”), to provide the Service on our behalf, to perform Service-related services or to assist us in analysing how our Service is used.</p>

                    <p>These third parties have access to your Personal Data only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.</p>

                    <h3>3. Transfer of Personal Data and its Security</h3>

                    <p>We take all appropriate security and legal precautions to safeguard the safety and integrity of all your personal data that is collected and used within the company. Your personal data will only be accessed by persons within the company or our trusted service providers who have a legitimate need to do so.</p>

                    <p>Access to the confidential data we collect is limited and we have policies and procedures in place to safeguard your information from loss, miss use and improper disclosure.</p>

                    <p>All our employees are subject to a company privacy and confidentiality policy which ensures that they are contracted to understand your confidentiality requirements and will work to the best of their ability in line with this policy.</p>

                    <h3>4. Your Rights and our Complaints Procedure</h3>

                    <p>You have the following rights in relation to your personal data:</p>

                    <ul><li>You may access the data we hold about you</li><li>If you note that your data is incorrect or incomplete you have the right to have that data corrected.</li><li>You may opt out of any of the services provided, but please note some services we may be unable to provide should you do so.</li><li>You may request that we delete your personal data (this subject to any legal requirement we may have to retain such data).</li><li>You may request a copy of your personal data held; this may take up to thirty (30) days.</li><li>You have the right to withdraw your consent to the use of any of your personal data for which you have previously given your consent to the use of.</li><li>You have the right to complain to the Data Protection Authority, such a complaint should be directed to the authority in your country or a relevant court of competent jurisdiction.</li></ul>

                    <p>We do however have a complaints procedure which we will deal with any complaints you may have, any such complaint should be directed to the attention our Data Protection officer info@gasmonkeycompetitions.co.uk. Who will acknowledge your complaint and ensure it is investigated honestly and fairly and inform you how it will be handled.</p>

                    <p>If you have any other queries or wish to exercise any of your rights in respect of your personal data please contact our Data Protection officer.</p>

                    <p>You may also complain directly to <a href="https://ico.org.uk/">https://ico.org.uk/</a></p>

                    <h3>5. Who we disclose your Personal Information to.</h3>

                    <p>We will disclose your personal information to the following;-</p>

                    <p>As described in clause 2 of this policy</p>

                    <ul><li>If required by law</li><li>If we believe disclosure is appropriate to enforce any of our terms and conditions, to protect and defend our rights, property, or safety.</li><li>In compliance of any court order, proceeding or under any other legal obligation, regulatory or government requirement where we are specifically directed to do so.</li><li>With your consent.</li></ul>

                    <p>We are obliged under current laws and jurisdiction to report suspicious transactions and any other such activity to the relevant regulatory authorities under Anti Money Laundering, terrorist financing, or related legislation. We will also report any suspected criminal activity to the relevant law enforcement body. In some circumstances we may not be permitted to inform you about this in advance of any disclosure, or at all.</p>

                    <p>Third Party Recipients of Personal Data include:</p>

                    <ul><li>Professional Advisors such as law firms, tax advisors or auditor all of whom are subject to privacy and confidentiality laws and regulations.</li><li>Regulatory and other such bodies.</li><li>Providers of identity verification services.</li><li>The Courts, police and other relevant law enforcement agencies</li><li>Relevant Government departments and agencies.</li><li>Our Service Providers.</li></ul>

                    <h3>6. How long we retain your Personal Information</h3>

                    <p>We retain your personal information only as long as it is needed by us (see clause 1.), thereafter we only retain any information as long as it is required under the regulatory requirements we are subject to.</p>

                    <p>To ensure we meet our legal liabilities we may retain some information for a significant time. Examples of the reason for this could be, to protect, defend or exercise our legal rights or for archiving and historical purposes.</p>

                    <h3>7. Data Security and Breaches</h3>

                    <p>We have put in place appropriate security measures to prevent your personal data from being accidentally lost, used, or accessed in an unauthorised way, altered or disclosed. In addition, we limit access to your personal data to those employees, agents, contractors and other third parties who have a business need to know. They will only process your personal data on our instructions, and they are subject to a duty of confidentiality.</p>

                    <p>We have put in place procedures to deal with any suspected personal data breach and will notify you and any applicable regulator of a breach where we are legally required to do so.</p>

                    <p>If there is a security breach, or a suspected security breach we will inform you of the breach or suspected breach immediately it is known to us and report it to the appropriate regulatory body.</p>

                    <p>Once such a breach is discovered we will use all reasonable business measures to correct the breach and prevent any further breaches and recover or delete any lost information.</p>

                    <h3>8. Changes to this Policy</h3>

                    <p>We will, from time to time, make changes to this policy. This may be to ensure that we continue to be in line with the legal requirements and any regulatory changes made in law. We may also change our practices to better serve our and your needs. We will revise the, “last updated’” date at the top of this notice and will, if such changes are material, post a prominent notice of the changes on the website.</p>

                    <p>We always request that you read this policy from time to time and keep your personal information up to date.</p>
            </div>
        </div>
    </div>
</main>
<!-- /main -->
@endsection
