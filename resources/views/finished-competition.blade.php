@extends('core.base')

@section('content')



<div class="container margin_60">
    <div class="row align-items-center">
        <div class="col-md-6">
        <img src="{{$competition->present()->main_image}}" alt="{{$competition->title}}" style="width:100%" />
        </div>
        <div class="col-md-6">
            <div class="comp_info">
                <h1>{{$competition->title}}</h1>
              <div class="finished-details mt-4">  
                <h3>Competition Closed : <span>{{date('d/m/Y \@ H:i', strtotime($competition->deadline))}}</span></h3>
                @if($competition->winner_name || $competition->winner_number)
                <h3>Competition Winner: <span>{{$competition->winner_name}}</span></h3>
                <h3>Winning Ticket Number : <span>{{$competition->winner_number}}</span></h3>
                @endif
              </div>
            </div>
          <div>
            <!-- /prod_info -->
        </div>
    </div>
    <!-- /row -->

</div>
<!-- /container -->
<div class="enter-competition mt-4">
    <div class="container">
        <div class="main_title">
            <h2>Competition Entries</h2>
        </div>
        <div class="row">
            <div class="col-md-12 add_bottom_25">
                <table id="entry-table" class="table table-striped">
                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>Ticket Number</th>
                            <th>Purchased</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($competition->tickets as $ticket)
                        <tr>
                            <td>{{$ticket->user->username}}</td>
                            <td>{{$ticket->number}}</td>
                            <td>{{date('jS F Y \@ H:i a', strtotime($ticket->created_at))}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Username</th>
                            <th>Ticket Number</th>
                            <th>Purchased</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@push('after-scripts')
    <script src="{{asset('js/datatables.min.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('js/dataTables.fixedHeader.min.js')}}"></script>
    <script src="{{asset('js/dataTables.responsive.min.js')}}"></script>
@endpush
