@extends('core.base')

@section('content')
    {{--@include('parts/_widget-top', ['class'=>'mt-40 mb-0'])--}}

    <!-- Content starts -->
    <section class="content_wrap error-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center mt-4">
                    <h1>OOPS!</h1>
                    <h2>404 Error - This page cannot be found</h2>
                    <a href="{{route('live-competitions')}}" class="btn v3">BACK TO LIVE COMPETITONS <i class="fas fa-angle-double-right"></i></a>
                </div>
            </div>
        </div>
    </section>
    <!-- Content ends -->
@endsection
