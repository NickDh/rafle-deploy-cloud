<?php



Route::group([
    'as' => 'dashboard::',
    'prefix' => 'dashboard',
    'namespace' => 'Dashboard',
    'middleware' => ['auth', 'admin'],
], function () {

    /*
    * Dashboard
    */
    Route::get('/', [
        'as' => 'index',
        'uses' => 'MainController@index',
    ]);

    /*
    * Competitions
    */
    Route::resources(['competitions' => 'CompetitionController']);
    Route::post('competitions/{competition}/status/{status}', [
        'as' => 'competitions.status',
        'uses' => 'CompetitionController@changeStatus',
    ]);
    Route::post('competitions/position', [
        'as' => 'competitions.position',
        'uses' => 'CompetitionController@positionUpdate',
    ]);
    Route::post('competitions/tickets/status/live/{ticket}', [
        'as' => 'competitions.ticket.status',
        'uses' => 'CompetitionController@changeTicketStatusToLive',
    ]);

    /**
     * Competition entries
     */
    Route::get('competition-entries', [
       'as' => 'competition-entries.index',
       'uses' => 'CompetitionController@finishedCompetitions'
    ]);
    Route::get('competition-entries/{competition}/find-winner', [
        'as' => 'competition-entries.find-winner',
        'uses' => 'CompetitionController@findWinnerPage',
    ]);
    Route::post('competition-entries/{competition}/find-winner', [
        'as' => 'competition-entries.find-winner',
        'uses' => 'CompetitionController@findWinner',
    ]);
    Route::get('competition-entries/{competition_id?}', [
        'as' => 'competition-entries.table',
        'uses' => 'CompetitionEntriesController@table'
    ]);
    Route::get('competition-entries/export/{competition_id?}', [
        'as' => 'competition-entries.export',
        'uses' => 'CompetitionEntriesController@export'
    ]);

    /*
    * Questions
    */
    Route::resources(['questions' => 'QuestionController'], ['except' => ['show']]);

    /*
    * Promo Codes
    */
    Route::resources(['promo-codes' => 'PromoCodeController'], ['except' => ['show']]);

    /*
    * Past Winners
    */
    Route::resources(['past-winners' => 'PastWinnerController'], ['except' => ['show']]);

    /*
    * Live Draws
    */
    Route::resources(['live-draws' => 'LiveDrawController'], ['except' => ['show']]);

    /*
    * Users
    */
    Route::resources(['users' => 'UserController'], ['except' => ['show']]);
    Route::post('users/change-suspend/{user_id}', 'UserController@changeSuspend')->name('users.suspend');
    /*
    * Results
    */
    Route::resources(['results' => 'ResultController'], ['except' => ['show']]);

    /*
    * Order
    */
    Route::resources(['orders' => 'OrderController'], ['only' => ['show', 'index']]);
    Route::post('orders/{competition}/cancel', [
        'as' => 'orders.cancel',
        'uses' => 'OrderController@cancel',
    ]);

    /*
     * Settings
     */
    Route::group([
        'prefix' => 'settings',
        'as' => 'settings.',
    ], function () {
        Route::get('/', 'SettingsController@edit')->name('edit');
        Route::put('/', 'SettingsController@update')->name('update');
    });

    /**
     * Pages
     */
    Route::group([
        'prefix' => 'pages',
        'as' => 'pages.'
    ], function (){
       Route::resource('page', 'PagesController')->except(['show']);
    });

    /*
     * Statistic
     */
    Route::group([
        'prefix' => 'statistics',
        'as' => 'statistics.',
    ], function () {
        Route::get('/', 'StatisticController@index')->name('index');
    });

    /*
     * Trash
     */
    Route::group([
        'prefix' => 'trash',
        'as' => 'trash.',
    ], function () {
        Route::get('', 'TrashController@index')->name('index');
        Route::post('{competition}/restore', 'TrashController@restoreCompetition')->name('restore-competition');
        Route::delete('{competition}', 'TrashController@destroyCompetition')->name('destroy-competition');
    });

    /*
     * Marketing/SMS
     */
    Route::group([
        'prefix' => 'sms',
        'as' => 'sms.',
    ], function () {
        Route::get('/', 'SmsController@index')->name('index');
        Route::get('/history', 'SmsHistoryController@index')->name('history.index');

        Route::post('/{user_id}', 'SmsController1@exclude')->name('exclude');

        Route::get('/create', 'SmsController@create')->name('create');
        Route::get('/store', 'SmsController@store')->name('store');

    });


    /*
     * Analytics
     */
    Route::group([
        'prefix' => 'analytics',
        'as' => 'analytics.',
    ], function () {
        Route::get('/', 'AnalyticsController@edit')->name('edit');
        Route::put('/', 'AnalyticsController@update')->name('update');
    });
});

