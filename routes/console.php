<?php

use App\Enums\OrderStatusEnum;
use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('resolve-old-held-tickets', function (){
   $this->info('Tickets unlocking started');

   $timeDelay = now()->subHour();
   /** @var \App\Tickets\Ticket[]|\Illuminate\Support\Collection $tickets */
   $tickets = \App\Tickets\Ticket::where('status', 'Held')->where('updated_at', '<', $timeDelay)->get();
   $analytic = [];
   foreach ($tickets as $ticket){
       $analytic[$ticket->id] = [
            'ticket_id' => $ticket->id,
            'ticket_created_at' => $ticket->created_at->toString(),
            'ticket_competition_id' => $ticket->competition_id,
            'ticket_user_id' => $ticket->user_id,
            'no_orders' => false,
            'no_orders_with_such_competition' => false,
            'no_orders_with_such_competition_and_number' => false,
            'all_orders_failed' => false,
            'should_be_unlocked' => false
       ];

       $orders = \App\Ecommerce\Orders\Order::where('user_id', $ticket->user_id);
       if($orders->count() == 0){
           $analytic[$ticket->id]['no_orders'] = true;
           $analytic[$ticket->id]['should_be_unlocked'] = true;
           continue;
       }

       $analytic[$ticket->id]['no_orders_with_such_competition'] = true;
       $orders->each(function ($order) use ($ticket, $analytic) {
           if(isset($order->cart_data[$ticket->competition_id])) {
               $analytic[$ticket->id]['no_orders_with_such_competition'] = false;

               $analytic[$ticket->id]['no_orders_with_such_competition_and_number'] = true;
               $ticketNumbers = explode(', ', $order->cart_data[$ticket->competition_id]['ticket_number']);
               if (!in_array($ticket->number, $ticketNumbers)) {
                   $analytic[$ticket->id]['no_orders_with_such_competition_and_number'] = false;
                   return true;
               }

               if ($order->status == OrderStatusEnum::PAID) {
                   $analytic[$ticket->id]['all_orders_failed'] = true;
                   return false;
               }
           }
       });

       $analytic[$ticket->id]['should_be_unlocked'] = $analytic[$ticket->id]['no_orders_with_such_competition'] ||  $analytic[$ticket->id]['no_orders_with_such_competition_and_number'] || $analytic[$ticket->id]['all_orders_failed'];
   }

    // Unlock tickets
    $unlockedTicketsCount = 0;
    foreach ($tickets as $ticket){
        if($analytic[$ticket->id]['should_be_unlocked']){
            $message = "Ticket manually unlocked".PHP_EOL;
            $message .= "Details: " . json_encode($analytic[$ticket->id]);

            logger($message);

            $ticket->delete();

            $unlockedTicketsCount++;
        }
    }

    $this->info(count($analytic) . " tickets where analyzed");
    $this->info("$unlockedTicketsCount tickets where unlocked");
});

Artisan::command('find-order', function (){
    $competitionId = 419;
    $number = 303;

    $orders = \App\Ecommerce\Orders\Order::where('cart_data', 'like', "%competition_id\";i:$competitionId%")->where('cart_data', 'like', "%$number%")->get();

    $orders->each(function ($order) use ($number) {
        $cartData = $order->cart_data;
        foreach ($cartData as $competitionDetails){
            $ticketNumbers = explode(', ', $competitionDetails['ticket_number'] );
            if(in_array($number, $ticketNumbers)){
                dump($order->id);
            }
        }
    });

});

// Restore competition tickets states based on orders
Artisan::command('restore', function () {
    $competitionId = 154;
    $this->info("Start restoring tickets from orders for competition ID - $competitionId");

//    $orders = \App\Ecommerce\Orders\Order::rightJoin('orders_tickets', 'orders_tickets.order_id', '=', 'orders.id')
//        ->where('orders.status', 'Paid')
//        ->where('orders_tickets.competition_id', $competitionId)
//        ->select('orders.*')
//        ->get();

    $orders = \App\Ecommerce\Orders\Order::where('status','Paid')->where('cart_data', 'like', "%i:$competitionId;%")->get();

    // Extract data about tickets for competition
    $count = 0;
    $tickets = [];
    $duplicates = [];
    $numbers = [];
    foreach ($orders as $order) {
        if(empty($order->cart_data[$competitionId])){
            continue;
        }

        $extractedIds = explode(';', $order->cart_data[$competitionId]['ticket_id']);
        $extractedNumbers = explode(', ', $order->cart_data[$competitionId]['ticket_number']);

        foreach ($extractedIds as $key => $id) {
            // Fast solution for filtering duplicated records after JOIN
            if (key_exists($id, $tickets)) {
                continue;
            }

            $number = $extractedNumbers[$key];

            if(key_exists($number, $numbers)){
                $duplicates[$number][] = $numbers[$number]->id;
                $duplicates[$number][] = $order->id;
                continue;
            }

            $numbers[$number] = $order;

            $tickets[$id] = new \App\Tickets\Ticket([
                'id'             => $id,
                'number'         => $extractedNumbers[$key],
                'status'         => \App\Enums\TicketStatusEnum::SOLD_OUT(),
                'competition_id' => $competitionId,
                'user_id'        => $order->user_id,
                'created_at'     => $order->created_at,
                'updated_at'     => $order->updated_at,
            ]);
            $tickets[$id]->save();
            $this->info("Restored ticket $extractedNumbers[$key]");

            $count++;
        }
    }
    dump('Duplicates: ', $duplicates);

    $this->info("Done! Restored $count tickets");
});

Artisan::command('check-duplicated-orders', function () {
    $this->info('Start generation report');

    $from = '2020-06-23 12:00';
    $to = '2020-07-09 12:00';
    $orders = \App\Ecommerce\Orders\Order::where('created_at', '>', $from)
        ->where('created_at', '<', $to)
        ->where('cart_data', 'like', '%i:154;%')
        ->whereIn('status', ['Paid', 'Collision'])
        ->get();

    $competitionsData = [];

    foreach ($orders as $order) {
        $payedNumbers = $order->cart_data;

        // Process all competitions on order
        foreach ($payedNumbers as $competitionId => $payedNumbersForCompetition) {
            $ticketNumbers = explode(', ', $payedNumbersForCompetition['ticket_number']);

            // Process all numbers related to one competition
            // Collect ids for all orders related to numbers in competitions
            foreach ($ticketNumbers as $number) {
                $competitionsData[$competitionId][$number][] = $order->id . ' | ' . $order->created_at . ' | ' . $order->customer_name . ' | ' . $order->customer_email;
            }
        }
    }

    // Check if we have somewhere more than on order per ticket
    $duplicates = [];
    $data = "Duplicates from $from to $to " . PHP_EOL . PHP_EOL;
    foreach ($competitionsData as $competitionId => $oneCompetitionData) {
        foreach ($oneCompetitionData as $number => $orders) {
            if (count($orders) > 1) {
                $duplicates[$competitionId][$number] = $orders;
                $data .= 'Competition - ' . $competitionId . PHP_EOL;
                $data .= 'Number - ' . $number . PHP_EOL;
                $data .= implode(PHP_EOL, $orders);
                $data .= PHP_EOL . PHP_EOL;
            }
        }
    }

//    dd($duplicates);

    $reportFilePath = "storage/logs/duplicates($from - $to).txt";
    file_put_contents($reportFilePath, $data);

    $this->info("Report generated. Check it in $reportFilePath");

})->describe('Check for orders with duplicated numbers');
