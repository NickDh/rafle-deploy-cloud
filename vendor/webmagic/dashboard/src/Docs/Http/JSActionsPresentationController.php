<?php


namespace Webmagic\Dashboard\Docs\Http;



use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;
use Webmagic\Dashboard\Elements\Forms\Elements\FormGroup;
use Webmagic\Dashboard\Elements\Links\LinkButton;

class JSActionsPresentationController
{
    /**
     * Tooltips functionality presentation
     *
     * @param Dashboard $dashboard
     *
     * @return Dashboard
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function tooltips(Dashboard $dashboard)
    {
        $content = view()->file(__DIR__ . '/../../../docs/js-actions/tooltips.md');

        $addTooltip = (new FormGroup())->labelTxt('Checkbox with tooltip')->element()->checkbox()->js()->tooltip()->regular('This is an input with tooltip')->parent();
        $hideAddedTooltip = (new FormGroup())->labelTxt('Checkbox with hidden tooltip')->element()->checkbox()->js()->tooltip()->regular('Hidden tooltip')->js()->tooltip()->hide()->parent();


        $dashboard->page()
            ->setPageTitle('Tooltips functionality')
            ->addElement()->tabs()->addTab()->title('Description')->content($content)->active()
            ->parent()->addTab()->title('Example')->content([$addTooltip, $hideAddedTooltip]);

        return $dashboard;
    }

    /**
     * Tooltips functionality presentation
     *
     * @param Dashboard $dashboard
     *
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function confirmationPopup(Dashboard $dashboard)
    {
        $content = view()->file(__DIR__ . '/../../../docs/js-actions/confirmation-popup.md');

        $btn = (new LinkButton())->content('Action element')
            ->js()->sendRequestOnClick()->regular(url()->current())
            ->js()->confirmationPopup()->regular(
                'Confirm this action',                          // title
                'Please, make sure you want to do this',        // content
                'I confirm',                                 // confirm button
                'Do not do this'                                // cancel button
            );

        $dashboard->page()
            ->setPageTitle('Confirm popup functionality')
            ->addElement()->box()->makeSimple()->content($content)
            ->parent()
            ->addElement()->box()->makeSimple()->content($btn);

        return $dashboard;
    }
}
