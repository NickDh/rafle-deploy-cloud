<?php

namespace App\LiveDraws;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Components\TablePageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;

class LiveDrawDashboardPresenter
{

    /**
     * @param $items
     * @param Dashboard $dashboard
     * @param Request $request
     * @return mixed|Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function getTablePage($items, Dashboard $dashboard, Request $request)
    {
        (new TablePageGenerator($dashboard->page()))
            ->title('Live Draws')
            ->tableTitles('ID', 'Draw', 'Date', 'Actions')
            ->showOnly('id', 'draw', 'date')
            ->setConfig([
                'date' => function (LiveDraw $item) {
                    return date('d/n/Y @ H:i', strtotime($item->date));
                },
            ])
            ->items($items)
            ->withPagination($items, route('dashboard::live-draws.index',$request->all()))
            ->createLink(route('dashboard::live-draws.create'))
            ->setEditLinkClosure(function (LiveDraw $item) {
                return route('dashboard::live-draws.edit', $item);
            })
        ;

        if ($request->ajax()) {
            return $dashboard->page()->content()->toArray()['box_body'];
        }

        return $dashboard;
    }


    /**
     * @return FormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function getCreateForm()
    {
        return (new FormPageGenerator())
            ->title('Add Live Draw')
            ->action(route('dashboard::live-draws.store'))
            ->ajax(true)
            ->method('POST')
            ->textInput('draw', false, 'Draw', true)
            ->dateTimePickerJS('date', false, 'Draw Date & Time', true)
            ->submitButtonTitle('Create');
    }


    /**
     * @param Model $item
     * @return FormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function getEditForm(Model $item)
    {
        $formPageGenerator = (new FormPageGenerator())
            ->title("Live Draw editing" )
            ->action(route('dashboard::live-draws.update', $item))
            ->method('PUT')
            ->textInput('draw', $item, 'Draw', true)
            ->dateTimePickerJS('date', $item, 'Draw Date & Time', true)
            ->submitButtonTitle('Update');


        $formPageGenerator->getBox()
            ->addElement('box_footer')
            ->linkButton()
            ->icon('fa-trash')
            ->content('Delete')
            ->class('btn-danger js_ajax-by-click-btn pull-left')
            ->dataAttr('action', route('dashboard::live-draws.destroy', $item->id))
            ->dataAttr('method', 'DELETE')
            ->dataAttr('confirm', true);

        return $formPageGenerator;
    }
}
