<?php

namespace App\LiveDraws;

use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Webmagic\Core\Entity\EntityRepo;

class LiveDrawRepo extends EntityRepo
{
	protected $entity = LiveDraw::class;

    /**
     * Get all with paginate and sorting
     *
     * @param null $per_page
     * @param null $page
     * @param string $sortBy
     * @param string $sort
     * @return LengthAwarePaginator|Builder[]|Collection
     * @throws \Exception
     */
    public function getByFilter($per_page = null, $page = null, $sortBy = 'created_at', $sort = 'desc')
    {
        $query = $this->query();


        $query->orderBy($sortBy, $sort);

        if (is_null($per_page) && is_null($page)) {
            return $query->get();
        }


        return $query->paginate($per_page, ['*'], 'page', $page);
    }

    public function getAllActive($perPage = null)
    {
        $query = $this->query();
        $query->where('date', '>', Carbon::now()->toDateTimeString());
        $query->orderBy('date', 'asc');

        return $this->realGetMany($query);
    }
}
