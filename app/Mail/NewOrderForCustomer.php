<?php

namespace App\Mail;

use App\Ecommerce\Orders\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewOrderForCustomer extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    /**
     * Create a new message instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     */
    public function build()
    {
        return $this->view('emails.order-user')
            ->to($this->order->customer_email)
            ->bcc(env('MAIL_COPY_ADDRESS'))
            ->with(['order' => $this->order])
            ->subject('Your GasMonkey Order Confirmation');
    }
}
