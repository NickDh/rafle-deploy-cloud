<?php

namespace App\Mail;

use App\Settings\SettingsRepo;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewOrderForAdmin extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    /**
     * Create a new message instance.
     *
     * @param Model $order
     */
    public function __construct(Model $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     * @throws BindingResolutionException
     * @throws Exception
     */
    public function build()
    {
        $settingsRepo = app()->make(SettingsRepo::class);
        $email = $settingsRepo->getAdminEmail();

        return $this->view('emails.order-admin')
            ->to($email)
            ->with(['order' => $this->order])
            ->subject('You’ve received a new order');
    }
}
