<?php

namespace App\Enums;

use MadWeb\Enum\Enum;

/**
 * @method static CompetitionTypeEnum NUMBERS()
 * @method static CompetitionTypeEnum LETTERS()
 */
final class CompetitionTypeEnum extends Enum
{
    const NUMBERS = 'Numbers';
    const LETTERS = 'Letters';
}
