<?php

namespace App\Enums;

use MadWeb\Enum\Enum;

/**
 * @method static TicketStatusEnum SOLD_OUT()
 * @method static TicketStatusEnum HELD()
 */
final class TicketStatusEnum extends Enum
{
    const SOLD_OUT = 'Sold Out';
    const HELD = 'Held';

}
