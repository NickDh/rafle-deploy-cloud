<?php

namespace App\Enums;

use MadWeb\Enum\Enum;

/**
 * @method static PromoCodeStatusTypesEnum EXPIRED()
 * @method static PromoCodeStatusTypesEnum LIVE()
 */
final class PromoCodeStatusTypesEnum extends Enum
{
    const __default = self::LIVE;

    const LIVE = 'Live';
    const EXPIRED = 'Expired';
}
