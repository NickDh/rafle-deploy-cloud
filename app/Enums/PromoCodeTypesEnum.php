<?php

namespace App\Enums;

use MadWeb\Enum\Enum;

/**
 * @method static PromoCodeTypesEnum PERCENT()
 * @method static PromoCodeTypesEnum MONEY()
 */
final class PromoCodeTypesEnum extends Enum
{
    const __default = self::PERCENT;

    const PERCENT = '%';
    const MONEY = '£';
}

