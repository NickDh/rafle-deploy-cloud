<?php

namespace App\Enums;

use MadWeb\Enum\Enum;

/**
 * @method static OrderStatusEnum PAID()
 * @method static OrderStatusEnum FAILED()
 * @method static OrderStatusEnum CANCELLED()
 */
final class OrderStatusEnum extends Enum
{
    const PAID = 'Paid';
    const PROCESSING = 'Processing';
    const FAILED = 'Failed';
    const CANCELLED = 'Cancelled';
    const COLLISION = 'Collision';
}
