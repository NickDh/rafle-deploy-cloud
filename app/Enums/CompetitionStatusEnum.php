<?php

namespace App\Enums;

use MadWeb\Enum\Enum;

/**
 * @method static CompetitionStatusEnum UNPUBLISHED()
 * @method static CompetitionStatusEnum LIVE()
 * @method static CompetitionStatusEnum PAUSED()
 * @method static CompetitionStatusEnum SOLD_OUT()
 * @method static CompetitionStatusEnum FINISHED()
 */
final class CompetitionStatusEnum extends Enum
{
    const __default = self::UNPUBLISHED;

    const UNPUBLISHED = 'Unpublished';
    const LIVE = 'Live';
    const PAUSED = 'Paused';
    const SOLD_OUT = 'Sold out';
    const FINISHED = 'Finished';
}
