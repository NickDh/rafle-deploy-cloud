<?php

namespace App\Users;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Components\TablePageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;
use Webmagic\Dashboard\Elements\Forms\Elements\Switcher;

class UserDashboardPresenter
{

    /**
     * @param $items
     * @param Dashboard $dashboard
     * @param Request $request
     * @return mixed|Dashboard
     * @throws NoOneFieldsWereDefined
     * @throws FieldUnavailable
     */
    public function getTablePage($items, Dashboard $dashboard, Request $request, UserFilter $filter = null)
    {
        $data['items'] = $request->get('items', 10);
        $data['page'] = $request->get('page', 1);
        $data['keyword'] = $filter ? $filter->getSearchPhrase() : $request->get('keyword', '');

        $tablePageGenerator = (new TablePageGenerator($dashboard->page()))
            ->title('Users')
            ->tableTitles('ID', 'Username', 'Name', 'Email', 'Suspend', 'Actions')
            ->showOnly('id', 'username', 'name', 'email', 'suspend')
            ->setConfig([
                'name' => function (User $item) {
                    if($item->first_name || $item->last_name){
                        return $item->present()->prepareFullName;
                    }

                    return '--';
                },
                'suspend' => function (User $user) {
                    return (new Switcher())->checked((bool)$user->suspend)
                        ->js()->sendRequestOnChange()
                        ->regular(
                            route('dashboard::users.suspend',$user->getKey()),
                            [],
                            'POST',
                            true,
                            true
                        );
                },
            ])
            ->items($items)
            ->withPagination($items, route('dashboard::users.index', $data))
            ->createLink(route('dashboard::users.create'))
            ->setEditLinkClosure(function (User $item) {
                return route('dashboard::users.edit', $item);
            })
        ;

        $tablePageGenerator->addFiltering()
            ->action(route('dashboard::users.index'))
            ->method('GET')
            ->textInput('keyword', $data['keyword'], '', false)
            ->submitButton('Search');

        if ($request->ajax()) {
            return $dashboard->page()->content()->toArray()['box_body'];
        }

        return $dashboard;
    }


    /**
     * @return FormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     * @throws \Exception
     */
    public function getCreateForm()
    {
        return (new FormPageGenerator())
            ->title('Add User')
            ->action(route('dashboard::users.store'))
            ->ajax(true)
            ->method('POST')
            ->textInput('username', false, 'Username', true)
            ->emailInput('email', false, 'Email', true)
            ->textInput('password', false, 'Password', true)
            ->submitButtonTitle('Create');
    }


    /**
     * @param Model $item
     * @return FormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function getEditForm(Model $item)
    {
        $formPageGenerator = (new FormPageGenerator())
            ->title("User editing")
            ->action(route('dashboard::users.update', $item))
            ->method('PUT')
            ->textInput('username', $item, 'Username', true)
            ->emailInput('email', $item, 'Email', true)
            ->textInput('password', null, 'Password', false)
            ->textInput('first_name', $item, 'First Name', false)
            ->textInput('last_name', $item, 'Last Name', false)
            ->textInput('phone', $item, 'Telephone', false)
            ->textInput('address', $item, 'Address 1 Line', false)
            ->textInput('town', $item, 'Town', false)
            ->textInput('post_code', $item, 'Post Code', false)
            ->submitButtonTitle('Update');

        $formPageGenerator->getBox()
            ->addElement('box_footer')
            ->linkButton()
            ->icon('fa-trash')
            ->content('Delete')
            ->class('btn-danger js_ajax-by-click-btn pull-left')
            ->dataAttr('action', route('dashboard::users.destroy', $item->id))
            ->dataAttr('method', 'DELETE')
            ->dataAttr('confirm', true);

        return $formPageGenerator;
    }
}
