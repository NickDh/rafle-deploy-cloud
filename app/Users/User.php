<?php

namespace App\Users;


use App\Ecommerce\Payment\FirstData\PaymentMethod\PaymentMethod;
use App\Enums\OrderStatusEnum;
use App\Enums\TicketStatusEnum;
use App\Notifications\ResetPassword;
use App\Ecommerce\Orders\Order;
use App\Settings\SettingsRepo;
use App\Tickets\Ticket;
use App\Users\Roles\Role;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Webmagic\Core\Presenter\PresentableTrait;


/**
 * App\Users\User
 *
 * @property int $id
 * @property string $username
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $phone
 * @property string $email
 * @property string $password
 * @property string $address
 * @property string $town
 * @property string $post_code
 * @property int $role_id
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $county
 * @property int $suspend
 * @property string|null $full_name
 * @property int $subscription
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read Collection|Order[] $orders
 * @property-read int|null $orders_count
 * @property-read Collection|Order[] $paid_orders
 * @property-read int|null $paid_orders_count
 * @property-read PaymentMethod|null $paymentMethod
 * @property-read Role $role
 * @property-read Collection|Ticket[] $tickets
 * @property-read int|null $tickets_count
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCounty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePostCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSubscription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSuspend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTown($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUsername($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable implements CanResetPassword
{
    use Notifiable, PresentableTrait;

    protected $presenter = UserPresenter::class;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'first_name',
        'last_name',
        'full_name',
        'phone',
        'email',
        'password',
        'role_id',
        'address',
        'town',
        'post_code',
        'suspend',
        'subscription'
    ];

    /**
     * @param $val
     */
    public function setFirstNameAttribute($val)
    {
        $this->attributes['first_name'] = $val;

        $this->updateFullName();
    }

    /**
     * @param $val
     */
    public function setLastNameAttribute($val)
    {
        $this->attributes['last_name'] = $val;

        $this->updateFullName();
    }

    /**
     * Update full name based on first name and last name attributes
     */
    protected function updateFullName()
    {
        $this->attributes['full_name'] = ucfirst($this->first_name) .' '. ucfirst($this->last_name);
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'role_id'
    ];

    /**
     * @param $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }


    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }


    /**
     * Check one role
     * @param string $role
     * @return bool
     */
    public function hasRole($role)
    {
        return null !== $this->role()->where('name', $role)->first();
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->hasRole('Admin');
    }

    /**
     * @return BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * @return HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class)->orderBy('created_at', 'desc');
    }

    /**
     * @return HasMany
     */
    public function tickets()
    {
        return $this->hasMany(Ticket::class)->orderBy('created_at', 'desc');
    }

    /**
     * @return HasMany
     */
    public function paid_orders()
    {
        return $this->orders()->where('status', OrderStatusEnum::PAID);
    }

    /**
     * Count how many user can picked tickets in competition
     *
     * @param int $competitionMaxPerUser
     * @param int $competition_id
     * @return int
     */
    public function availableTicketsToBuy(int $competitionMaxPerUser, int $competition_id)
    {
        $user_tickets = $this->tickets->where('competition_id', $competition_id)->count();

        return $competitionMaxPerUser - $user_tickets;
    }

    /**
     * Max count for random generation
     *
     * @param int $competitionMaxPerUser
     * @param int $competition_id
     *
     * @return int
     * @throws \Exception
     */
    public function availableForRandomizer(int $competitionMaxPerUser, int $competition_id)
    {
        $ticketsToBuy = $this->availableTicketsToBuy($competitionMaxPerUser, $competition_id);
        $ticketsRandomLimit = (new SettingsRepo())->getSettings()->random_generator_value;

        return $ticketsToBuy < $ticketsRandomLimit ? $ticketsToBuy : $ticketsRandomLimit;
    }

    /**
     * @param int $competition_id
     * @return Collection
     */
    public function ticketsBoughtByCompetition(int $competition_id)
    {
        return $this->tickets()
            ->where('competition_id', $competition_id)
            ->where('status', TicketStatusEnum::SOLD_OUT)
            ->get();
    }
}
