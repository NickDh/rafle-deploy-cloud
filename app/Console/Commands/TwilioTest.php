<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Marketing\Twilio\TwilioMessage;

class TwilioTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twilio:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send test message to Twilio';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(TwilioMessage $twiliomessage)
    {
        $response = $twiliomessage->messageSent('+79829935006','This is a test message');

        var_dump($response);
//        try {
//            $codeService->checkAndUpdateStatuses();
//        } catch (Exception $exception){
//            Log::error($exception);
//        }
    }




//
//object(Twilio\Rest\Api\V2010\Account\MessageInstance)#1889 (6) {
//  ["_media":protected]=>
//  NULL
//  ["_feedback":protected]=>
//  NULL
//  ["version":protected]=>
//  object(Twilio\Rest\Api\V2010)#1884 (28) {
//    ["_accounts":protected]=>
//    NULL
//    ["_account":protected]=>
//    object(Twilio\Rest\Api\V2010\AccountContext)#1885 (27) {
//      ["_addresses":protected]=>
//      NULL
//      ["_applications":protected]=>
//      NULL
//      ["_authorizedConnectApps":protected]=>
//      NULL
//      ["_availablePhoneNumbers":protected]=>
//      NULL
//      ["_balance":protected]=>
//      NULL
//      ["_calls":protected]=>
//      NULL
//      ["_conferences":protected]=>
//      NULL
//      ["_connectApps":protected]=>
//      NULL
//      ["_incomingPhoneNumbers":protected]=>
//      NULL
//      ["_keys":protected]=>
//      NULL
//      ["_messages":protected]=>
//      object(Twilio\Rest\Api\V2010\Account\MessageList)#1886 (3) {
//        ["version":protected]=>
//        *RECURSION*
//        ["solution":protected]=>
//        array(1) {
//          ["accountSid"]=>
//          string(34) "AC51440915adef7b33bb543c659bd2f644"
//        }
//        ["uri":protected]=>
//        string(58) "/Accounts/AC51440915adef7b33bb543c659bd2f644/Messages.json"
//      }
//      ["_newKeys":protected]=>
//      NULL
//      ["_newSigningKeys":protected]=>
//      NULL
//      ["_notifications":protected]=>
//      NULL
//      ["_outgoingCallerIds":protected]=>
//      NULL
//      ["_queues":protected]=>
//      NULL
//      ["_recordings":protected]=>
//      NULL
//      ["_signingKeys":protected]=>
//      NULL
//      ["_sip":protected]=>
//      NULL
//      ["_shortCodes":protected]=>
//      NULL
//      ["_tokens":protected]=>
//      NULL
//      ["_transcriptions":protected]=>
//      NULL
//      ["_usage":protected]=>
//      NULL
//      ["_validationRequests":protected]=>
//      NULL
//      ["version":protected]=>
//      *RECURSION*
//      ["solution":protected]=>
//      array(1) {
//        ["sid"]=>
//        string(34) "AC51440915adef7b33bb543c659bd2f644"
//      }
//      ["uri":protected]=>
//      string(49) "/Accounts/AC51440915adef7b33bb543c659bd2f644.json"
//    }
//    ["_addresses":protected]=>
//    NULL
//    ["_applications":protected]=>
//    NULL
//    ["_authorizedConnectApps":protected]=>
//    NULL
//    ["_availablePhoneNumbers":protected]=>
//    NULL
//    ["_balance":protected]=>
//    NULL
//    ["_calls":protected]=>
//    NULL
//    ["_conferences":protected]=>
//    NULL
//    ["_connectApps":protected]=>
//    NULL
//    ["_incomingPhoneNumbers":protected]=>
//    NULL
//    ["_keys":protected]=>
//    NULL
//    ["_messages":protected]=>
//    NULL
//    ["_newKeys":protected]=>
//    NULL
//    ["_newSigningKeys":protected]=>
//    NULL
//    ["_notifications":protected]=>
//    NULL
//    ["_outgoingCallerIds":protected]=>
//    NULL
//    ["_queues":protected]=>
//    NULL
//    ["_recordings":protected]=>
//    NULL
//    ["_signingKeys":protected]=>
//    NULL
//    ["_sip":protected]=>
//    NULL
//    ["_shortCodes":protected]=>
//    NULL
//    ["_tokens":protected]=>
//    NULL
//    ["_transcriptions":protected]=>
//    NULL
//    ["_usage":protected]=>
//    NULL
//    ["_validationRequests":protected]=>
//    NULL
//    ["domain":protected]=>
//    object(Twilio\Rest\Api)#1875 (3) {
//      ["_v2010":protected]=>
//      *RECURSION*
//      ["client":protected]=>
//      object(Twilio\Rest\Client)#1882 (32) {
//        ["username":protected]=>
//        string(34) "AC51440915adef7b33bb543c659bd2f644"
//        ["password":protected]=>
//        string(32) "66e62561a2236141b1d4dae2cfb15b75"
//        ["accountSid":protected]=>
//        string(34) "AC51440915adef7b33bb543c659bd2f644"
//        ["region":protected]=>
//        NULL
//        ["httpClient":protected]=>
//        object(Twilio\Http\CurlClient)#1877 (4) {
//          ["curlOptions":protected]=>
//          array(0) {
//          }
//          ["debugHttp":protected]=>
//          bool(false)
//          ["lastRequest"]=>
//          array(8) {
//            [10002]=>
//            string(91) "https://api.twilio.com/2010-04-01/Accounts/AC51440915adef7b33bb543c659bd2f644/Messages.json"
//            [42]=>
//            bool(true)
//            [19913]=>
//            bool(true)
//            [14]=>
//            NULL
//            [10023]=>
//            array(5) {
//              [0]=>
//              string(42) "User-Agent: twilio-php/5.41.1 (PHP 7.3.22)"
//              [1]=>
//              string(21) "Accept-Charset: utf-8"
//              [2]=>
//              string(47) "Content-Type: application/x-www-form-urlencoded"
//              [3]=>
//              string(24) "Accept: application/json"
//              [4]=>
//              string(113) "Authorization: Basic QUM1MTQ0MDkxNWFkZWY3YjMzYmI1NDNjNjU5YmQyZjY0NDo2NmU2MjU2MWEyMjM2MTQxYjFkNGRhZTJjZmIxNWI3NQ=="
//            }
//            [13]=>
//            int(60)
//            [47]=>
//            bool(true)
//            [10015]=>
//            string(81) "To=%2B79829935006&From=%2B18702294499&Body=This+is+a+test+message&StatusCallback="
//          }
//          ["lastResponse"]=>
//          object(Twilio\Http\Response)#1888 (3) {
//            ["headers":protected]=>
//            array(17) {
//              ["Date"]=>
//              string(30) " Fri, 29 Jan 2021 15:04:05 GMT"
//              ["Content-Type"]=>
//              string(32) " application/json; charset=utf-8"
//              ["Content-Length"]=>
//              string(4) " 824"
//              ["Connection"]=>
//              string(11) " keep-alive"
//              ["Twilio-Concurrent-Requests"]=>
//              string(2) " 1"
//              ["Twilio-Request-Id"]=>
//              string(35) " RQ332ccf75d2e141dbbec0607b853c2a38"
//              ["Twilio-Request-Duration"]=>
//              string(6) " 0.125"
//              ["Access-Control-Allow-Origin"]=>
//              string(2) " *"
//              ["Access-Control-Allow-Headers"]=>
//              string(101) " Accept, Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since"
//              ["Access-Control-Allow-Methods"]=>
//              string(27) " GET, POST, DELETE, OPTIONS"
//              ["Access-Control-Expose-Headers"]=>
//              string(5) " ETag"
//              ["Access-Control-Allow-Credentials"]=>
//              string(5) " true"
//              ["X-Powered-By"]=>
//              string(8) " AT-5000"
//              ["X-Shenanigans"]=>
//              string(5) " none"
//              ["X-Home-Region"]=>
//              string(4) " us1"
//              ["X-API-Domain"]=>
//              string(15) " api.twilio.com"
//              ["Strict-Transport-Security"]=>
//              string(17) " max-age=31536000"
//            }
//            ["content":protected]=>
//            string(824) "{"sid": "SMe71e718b99bd4228845a9680144b5e0a", "date_created": "Fri, 29 Jan 2021 15:04:05 +0000", "date_updated": "Fri, 29 Jan 2021 15:04:05 +0000", "date_sent": null, "account_sid": "AC51440915adef7b33bb543c659bd2f644", "to": "+79829935006", "from": "+18702294499", "messaging_service_sid": null, "body": "Sent from your Twilio trial account - This is a test message", "status": "queued", "num_segments": "1", "num_media": "0", "direction": "outbound-api", "api_version": "2010-04-01", "price": null, "price_unit": "USD", "error_code": null, "error_message": null, "uri": "/2010-04-01/Accounts/AC51440915adef7b33bb543c659bd2f644/Messages/SMe71e718b99bd4228845a9680144b5e0a.json", "subresource_uris": {"media": "/2010-04-01/Accounts/AC51440915adef7b33bb543c659bd2f644/Messages/SMe71e718b99bd4228845a9680144b5e0a/Media.json"}}"
//            ["statusCode":protected]=>
//            int(201)
//          }
//        }
//        ["_account":protected]=>
//        NULL
//        ["_accounts":protected]=>
//        NULL
//        ["_api":protected]=>
//        *RECURSION*
//        ["_authy":protected]=>
//        NULL
//        ["_autopilot":protected]=>
//        NULL
//        ["_chat":protected]=>
//        NULL
//        ["_conversations":protected]=>
//        NULL
//        ["_fax":protected]=>
//        NULL
//        ["_flexApi":protected]=>
//        NULL
//        ["_insights":protected]=>
//        NULL
//        ["_ipMessaging":protected]=>
//        NULL
//        ["_lookups":protected]=>
//        NULL
//        ["_messaging":protected]=>
//        NULL
//        ["_monitor":protected]=>
//        NULL
//        ["_notify":protected]=>
//        NULL
//        ["_preview":protected]=>
//        NULL
//        ["_pricing":protected]=>
//        NULL
//        ["_proxy":protected]=>
//        NULL
//        ["_serverless":protected]=>
//        NULL
//        ["_studio":protected]=>
//        NULL
//        ["_sync":protected]=>
//        NULL
//        ["_taskrouter":protected]=>
//        NULL
//        ["_trunking":protected]=>
//        NULL
//        ["_verify":protected]=>
//        NULL
//        ["_video":protected]=>
//        NULL
//        ["_voice":protected]=>
//        NULL
//        ["_wireless":protected]=>
//        NULL
//      }
//      ["baseUrl":protected]=>
//      string(22) "https://api.twilio.com"
//    }
//    ["version":protected]=>
//    string(10) "2010-04-01"
//  }
//  ["context":protected]=>
//  NULL
//  ["properties":protected]=>
//  array(20) {
//    ["accountSid"]=>
//    string(34) "AC51440915adef7b33bb543c659bd2f644"
//    ["apiVersion"]=>
//    string(10) "2010-04-01"
//    ["body"]=>
//    string(60) "Sent from your Twilio trial account - This is a test message"
//    ["dateCreated"]=>
//    object(DateTime)#1890 (3) {
//      ["date"]=>
//      string(26) "2021-01-29 15:04:05.000000"
//      ["timezone_type"]=>
//      int(1)
//      ["timezone"]=>
//      string(6) "+00:00"
//    }
//    ["dateUpdated"]=>
//    object(DateTime)#1891 (3) {
//      ["date"]=>
//      string(26) "2021-01-29 15:04:05.000000"
//      ["timezone_type"]=>
//      int(1)
//      ["timezone"]=>
//      string(6) "+00:00"
//    }
//    ["dateSent"]=>
//    NULL
//    ["direction"]=>
//    string(12) "outbound-api"
//    ["errorCode"]=>
//    NULL
//    ["errorMessage"]=>
//    NULL
//    ["from"]=>
//    string(12) "+18702294499"
//    ["messagingServiceSid"]=>
//    NULL
//    ["numMedia"]=>
//    string(1) "0"
//    ["numSegments"]=>
//    string(1) "1"
//    ["price"]=>
//    NULL
//    ["priceUnit"]=>
//    string(3) "USD"
//    ["sid"]=>
//    string(34) "SMe71e718b99bd4228845a9680144b5e0a"
//    ["status"]=>
//    string(6) "queued"
//    ["subresourceUris"]=>
//    array(1) {
//      ["media"]=>
//      string(110) "/2010-04-01/Accounts/AC51440915adef7b33bb543c659bd2f644/Messages/SMe71e718b99bd4228845a9680144b5e0a/Media.json"
//    }
//    ["to"]=>
//    string(12) "+79829935006"
//    ["uri"]=>
//    string(104) "/2010-04-01/Accounts/AC51440915adef7b33bb543c659bd2f644/Messages/SMe71e718b99bd4228845a9680144b5e0a.json"
//  }
//  ["solution":protected]=>
//  array(2) {
//    ["accountSid"]=>
//    string(34) "AC51440915adef7b33bb543c659bd2f644"
//    ["sid"]=>
//    string(34) "SMe71e718b99bd4228845a9680144b5e0a"
//  }
//}
}

