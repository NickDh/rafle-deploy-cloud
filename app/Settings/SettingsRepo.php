<?php


namespace App\Settings;


use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Webmagic\Core\Entity\EntityRepo;

class SettingsRepo extends EntityRepo
{
    protected $entity = Settings::class;

    /**
     * @return Builder|Model|object|null
     * @throws Exception
     */
    public function getSettings()
    {
        return $this->query()->first();
    }

    /**
     * Get signature
     *
     * @return Model|null
     * @throws Exception
     */
    public function getCartLifetime()
    {
        $query = $this->query();
        $query = $query->where('cart_lifetime', '!=', null)->select('cart_lifetime');

        return $this->realGetOne($query)->cart_lifetime;
    }


    /**
     * Get admin email
     *
     * @return Model|null
     * @throws Exception
     */
    public function getAdminEmail()
    {
        $query = $this->query();
        $query = $query->where('admin_email', '!=', null)->select('admin_email');

        return $this->realGetOne($query)->admin_email;
    }

    /**
     * @return Model|null
     * @throws Exception
     */
    public function getViewSortBy()
    {
        $query = $this->query();
        $query = $query->where('view_sort_by', '!=', null)->select('view_sort_by');

        return $this->realGetOne($query)->view_sort_by ?? 10;
    }

    /**
     * @return Model|null
     * @throws Exception
     */
    public function getGoogleAnalytics()
    {
        $query = $this->query();
        $query = $query->where('google_analytics', '!=', null)->select('google_analytics');

        return $this->realGetOne($query)->google_analytics;
    }
}
