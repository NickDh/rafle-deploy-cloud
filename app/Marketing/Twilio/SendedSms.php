<?php

namespace App\Marketing\Twilio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;


class SendedSms extends Model
{
    protected $table = 'twilio_sended_sms';
//    /** @var string[]  */
//    protected $fillable = [
//        ''
//    ];
    protected $guarded = [];

    /**

     * @return HasOne
     */
    public function message()
    {
        return $this->hasOne(TwilioMessage::class);
    }


}
