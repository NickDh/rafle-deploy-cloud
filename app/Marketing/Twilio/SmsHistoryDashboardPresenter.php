<?php

namespace App\Marketing\Twilio;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Components\TablePageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;
use Webmagic\Dashboard\Elements\Forms\Elements\Switcher;

class SmsHistoryDashboardPresenter
{

    /**
     * @param $items
     * @param Dashboard $dashboard
     * @param Request $request
     * @return mixed|Dashboard
     * @throws NoOneFieldsWereDefined
     * @throws FieldUnavailable
     */
    public function getTablePage($items, Dashboard $dashboard, Request $request, UserFilter $filter = null)
    {
        $data['items'] = $request->get('items', 10);
        $data['page'] = $request->get('page', 1);
        $data['keyword'] = $filter ? $filter->getSearchPhrase() : $request->get('keyword', '');

        $tablePageGenerator = (new TablePageGenerator($dashboard->page()))
            ->title('SMS History')
            ->tableTitles('ID', 'message_id','messaging_service_sid', 'Phone','Status', 'error_code','error_message', 'created_at')
            ->showOnly('id', 'message_id','messaging_service_sid','phone','status', 'error_code','error_message','created_at' )
            ->setConfig([
//                'message' => function (Message $item) {
//                    if($item->text || $item->last_name){
//                        return $item->present()->prepareText;
//                    }
//
//                    return '--';
//                },
//                'exclude' => function (User $user) {
//                    return (new Switcher())->checked((bool)$user->suspend)
//                        ->js()->sendRequestOnChange()
//                        ->regular(
//                            route('dashboard::sms.exclude',$user->getKey()),
//                            [],
//                            'POST',
//                            true,
//                            true
//                        );
//                },
            ])
            ->items($items)
            ->withPagination($items, route('dashboard::sms.history.index', $data))
//            ->setEditLinkClosure(function (User $item) {
//                return route('dashboard::users.edit', $item);
//            })
        ;

        $tablePageGenerator->addFiltering()
            ->action(route('dashboard::sms.history.index'))
            ->method('GET')
            ->textInput('keyword', $data['keyword'], '', false)
            ->submitButton('Search');

        if ($request->ajax()) {
            return $dashboard->page()->content()->toArray()['box_body'];
        }

        return $dashboard;
    }

}
