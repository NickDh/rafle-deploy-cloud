<?php

namespace App\Marketing\Twilio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Components\TablePageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;
use Webmagic\Dashboard\Elements\Forms\Elements\Switcher;
use App\Users\User;

use App\Marketing\Twilio\Dashboard\SmsUserFilter as UserFilter;

class UserSmsDashboardPresenter
{

    /**
     * @param $items
     * @param Dashboard $dashboard
     * @param Request $request
     * @return mixed|Dashboard
     * @throws NoOneFieldsWereDefined
     * @throws FieldUnavailable
     */
    public function getTablePage($items, Dashboard $dashboard, Request $request, UserFilter $filter = null)
    {
        $data['items'] = $request->get('items', 10);
        $data['page'] = $request->get('page', 1);
        $data['keyword'] = $filter ? $filter->getSearchPhrase() : $request->get('keyword', '');

        $tablePageGenerator = (new TablePageGenerator($dashboard->page()))
            ->title('SMS')
            //, 'Last Seen'
            //ID | Username | Name | Phone | Last Seen
            ->tableTitles('ID', 'Exclude','Username', 'Name', 'Phone')
            ->showOnly('id', 'exclude','username', 'name', 'phone')
            ->setConfig([
                'name' => function (User $item) {
                    if($item->first_name || $item->last_name){
                        return $item->present()->prepareFullName;
                    }

                    return '--';
                },
                'exclude' => function (User $user) {
                    return (new Switcher())->checked((bool)$user->suspend)
                        ->js()->sendRequestOnChange()
                        ->regular(
                            route('dashboard::sms.exclude',$user->getKey()),
                            [],
                            'POST',
                            true,
                            true
                        );
                },
            ])
            ->items($items)
            ->withPagination($items, route('dashboard::sms.index', $data))
            ->createLink(route('dashboard::sms.create'))
//            ->setEditLinkClosure(function (User $item) {
//                return route('dashboard::users.edit', $item);
//            })
        ;

        $tablePageGenerator->addFiltering()
            ->action(route('dashboard::sms.index'))
            ->method('GET')
            ->textInput('keyword', $data['keyword'], '', false)
            ->submitButton('Search');

        if ($request->ajax()) {
            return $dashboard->page()->content()->toArray()['box_body'];
        }

        return $dashboard;
    }

//Create message


//    /**
//     * @param Dashboard $dashboard
//     * @return mixed
//     */
//    public function getCreateForm(Dashboard $dashboard)
//    {
//        return $dashboard->addContent(view('dashboard.sms_create_form'));
//    }


    /**
     * @return FormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     * @throws \Exception
     */
    public function getCreateForm()
    {
        return (new FormPageGenerator())
            ->title('Add Sms Message')
            ->action(route('dashboard::sms.store'))
            ->ajax(true)
            ->method('POST')
            ->textInput('phone', false, 'Phone', true)
            ->textInput('text', false, 'Text', true)
            ->submitButtonTitle('Send Message');
    }

//    /**
//     * @param Dashboard $dashboard
//     * @param TwilioMessage|Model $question
//     * @return mixed
//     */
//    public function getEditForm(Dashboard $dashboard, TwilioMessage message)
//    {
//        return $dashboard->addContent(view('dashboard.sms_create_form', compact('message')));
//    }
}
