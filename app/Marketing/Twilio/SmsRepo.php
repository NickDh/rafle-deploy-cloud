<?php

namespace App\Marketing\Twilio;

use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Webmagic\Core\Entity\EntityRepo;

class SmsRepo extends EntityRepo
{
    protected $entity = SendedSms::class;

    /**
     * Get all with paginate and filter
     *
     * @param null $keyword
     * @param null $per_page
     * @param null $page
     * @param string $sortBy
     * @param string $sort
     * @return LengthAwarePaginator|Builder[]|Collection
     * @throws Exception
     */
    public function getByFilter($keyword = null, $per_page = null, $page = null, $sortBy = 'created_at', $sort = 'desc',$message_id = null )
    {
        $query = $this->query();

        if (isset($message_id)){
            $query->where('message_id', $message_id);
        }

//        if ($keyword) {
//            $searchQuery = "%$keyword%";
//
//            $query
//                ->where('username', 'like', $searchQuery)
//                ->orWhere('full_name', 'like', $searchQuery)
//                ->orWhere('email', 'like', $searchQuery);
//        }

        $query->orderBy($sortBy, $sort);

        if (is_null($per_page) && is_null($page)) {
            return $query->get();
        }


        return $query->paginate($per_page, ['*'], 'page', $page);
    }

    /**
     * Create new entity
     *
     * @param array $data
     *
     * @return Model
     * @throws Exception
     */
    public function createMessage(array $data,$message_id)
    {
        $query = $this->query();

        $data['role_id'] = Role::where('name', 'user')->first()->id;

        return $query->create($data);

    }

    public function createTwilioSms(){

    }
}
