<?php


namespace App\Pages\Dashboard;


use App\Pages\Page;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;

class PageFormPageGenerator extends FormPageGenerator
{
    public function __construct()
    {
        parent::__construct(null);
    }

    /**
     * @param Page|null $page
     *
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function prepare(Page $page = null)
    {
        $this->form->addSendAllCheckbox(true);

        $this
            ->title($page ? "$page->title edit" : 'Create new page')
            ->action($page ? route('dashboard::pages.page.update', $page->id) : route('dashboard::pages.page.store'))
            ->method($page ? 'PUT' : 'POST')
            ->textInput('title', $page, 'Title', true)
            ->slugInput('slug', 'title', $page, 'Slug', true)
            ->checkbox('header_link', $page, 'Header link')
            ->checkbox('footer_link', $page, 'Footer link')
            ->select('footer_column', ['1' => 'Column 1', '2' => 'Column 2'], $page, 'Footer column')
            ->visualEditor('content', $page, 'Content');
    }
}