<?php

namespace App\Competitions;


use App\Enums\CompetitionStatusEnum;
use App\Enums\TicketStatusEnum;
use App\Questions\Question;
use App\Settings\SettingsRepo;
use App\Tickets\Ticket;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rutorika\Sortable\SortableTrait;
use Webmagic\Core\Presenter\PresentableTrait;
use Webmagic\Core\Presenter\Presenter;

/**
 * App\Competitions\Competition
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $slug
 * @property string|null $short_description
 * @property string|null $full_description
 * @property float|null $price
 * @property float|null $sale_price
 * @property string|null $competition_type
 * @property-read int|null $tickets_count
 * @property string|null $tickets_sort_by
 * @property string|null $letters_to
 * @property string|null $deadline
 * @property int|null $auto_extend
 * @property int|null $hours_extend
 * @property int|null $max_tickets_per_user
 * @property string|null $main_image
 * @property int|null $question_id
 * @property string|null $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $position
 * @property string $photos
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property string|null $winner_name
 * @property string|null $winner_number
 * @property-read Question|null $question
 * @property-read \Illuminate\Database\Eloquent\Collection|Ticket[] $sold_tickets
 * @property-read int|null $sold_tickets_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Ticket[] $tickets
 * @method static \Illuminate\Database\Eloquent\Builder|Competition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Competition newQuery()
 * @method static \Illuminate\Database\Query\Builder|Competition onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Competition query()
 * @method static \Illuminate\Database\Eloquent\Builder|Competition sorted()
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereAutoExtend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereCompetitionType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereDeadline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereFullDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereHoursExtend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereLettersTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereMainImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereMaxTicketsPerUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition wherePhotos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereSalePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereShortDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereTicketsCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereTicketsSortBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereWinnerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereWinnerNumber($value)
 * @method static \Illuminate\Database\Query\Builder|Competition withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Competition withoutTrashed()
 * @mixin \Eloquent
 */
class Competition extends Model
{
    use PresentableTrait, SortableTrait, SoftDeletes;

    /** @var  Presenter class that using for present model */
    protected $presenter = CompetitionPresenter::class;

    protected $fillable = [
        'title',
        'slug',
        'short_description',
        'full_description',
        'price',
        'sale_price',
        'competition_type',
        'tickets_count',
        'tickets_sort_by',
        'deadline',
        'auto_extend',
        'hours_extend',
        'main_image',
        'status',
        'max_tickets_per_user',
        'question_id',
        'letters_to',
        'position',
        'photos',
        'winner_name',
        'winner_number'
    ];


    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->status = $model->status ?? CompetitionStatusEnum::UNPUBLISHED;
        });
    }

    /**
     * @return bool
     */
    public function isFinished():bool
    {
        return CompetitionStatusEnum::FINISHED()->is($this->status);
    }

    /**
     * @return int
     * @throws \Exception
     */
    public function maxTicketsForRandomGeneration()
    {
        $limit = (new SettingsRepo())->getSettings()->random_generator_value;

        $limit = $this->max_tickets_per_user < $limit ? $this->max_tickets_per_user : $limit;

        $availableTicketsCount = $this->availableTicketsCount();

        return $limit < $availableTicketsCount ? $limit : $availableTicketsCount;
    }

    /**
     * @return int
     */
    public function availableTicketsCount(): int
    {
       return $this->tickets_count - $this->tickets->count();
    }

    /**
     * @return BelongsTo
     */
    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    /**
     * @return HasMany
     */
    public function tickets()
    {
        return $this->hasMany(Ticket::class)->with('user');
    }

    /**
     * Find Sold Ticket
     *
     * @param string $number
     *
     * @return Ticket|Model|HasMany|object|null
     */
    public function findSoldTicket(string $number)
    {
           return $this->tickets()
                ->where('number', $number)
                ->where('status', TicketStatusEnum::SOLD_OUT)
                ->first();
    }

    /**
     * @return HasMany
     */
    public function sold_tickets()
    {
        return $this->tickets()
            ->where('status', TicketStatusEnum::SOLD_OUT)
            ->orderBy('created_at', 'desc');
    }

    /**
     * @return int
     */
    public function soldTicketsCount()
    {
        if(!isset($this->countOfSoldTickets)){
            $this->countOfSoldTickets = $this->sold_tickets()->count();
        }

        return $this->countOfSoldTickets;
    }

    /**
     * Get deadline time with extended hours if exist
     *
     * @return Carbon
     */
    public function realDeadline()
    {
        if($this->auto_extend && $this->isMainDeadlineExpired()){
            return Carbon::parse($this->deadline)->addHours($this->hours_extend);
        }

        return Carbon::parse($this->deadline);
    }

    /**
     * @return bool
     */
    public function isMainDeadlineExpired()
    {
        $deadline = Carbon::parse($this->deadline);

        return $deadline->diffInSeconds(Carbon::now(), false) >= 0;
    }

    /**
     * Check if competition deadline expired
     *
     * @return bool
     */
    public function isDeadlineExpired()
    {
        $realDeadline = $this->realDeadline();

        return $realDeadline->diffInSeconds(Carbon::now(), false) >= 0;
    }

    /**
     * Check if all tickets sold
     *
     * @return bool
     */
    public function isAllTicketsSold()
    {
        $soldTicketsCount = $this->sold_tickets()->count();

        return $this->tickets_count == $soldTicketsCount;
    }

    /**
     * @return int|mixed
     */
    public function ticketsLeft()
    {
        return $this->tickets_count - $this->soldTicketsCount();
    }
}
