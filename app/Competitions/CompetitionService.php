<?php

namespace App\Competitions;


use App\Enums\CompetitionStatusEnum;
use App\Jobs\FinishCompetitionStatus;
use App\Services\ImagesPrepare;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\Exceptions\PresenterException;

class CompetitionService
{
    /**
     * @var CompetitionRepo
     */
    protected $repo;

    /**
     * CompetitionService constructor.
     */
    public function __construct()
    {
        $this->repo = new CompetitionRepo();
    }

    /**
     * @param Model $competition
     */
    public function addJobTorFinishCompetition(Model $competition)
    {
        dispatch(new FinishCompetitionStatus($competition->id))
            ->delay($competition->realDeadline()->diffInSeconds(Carbon::now()))->onQueue('competition');
    }

    /**
     * @param array $ids
     * @throws Exception
     */
    public function checkAndSetSoldOutStatus(array $ids)
    {
        foreach ($ids as $id){
            $competition = $this->repo->getByID($id);

            if($competition->sold_tickets->count() == $competition->tickets_count){
                $this->repo->update($id, ['status' => CompetitionStatusEnum::SOLD_OUT]);
            }
        }
    }

    /**
     * @param int $id
     * @throws Exception
     */
    public function setLiveStatusIfCan(int $id)
    {
        if($competition = $this->repo->getByID($id)){

            $hasUnsoldTickets = $competition->sold_tickets->count() != $competition->tickets_count;

            if(!CompetitionStatusEnum::FINISHED()->is($competition->status) && $hasUnsoldTickets){
                $this->repo->update($id, ['status' => CompetitionStatusEnum::LIVE]);
            }
        }

    }

    /**
     * @param Competition|Model $competition
     * @throws PresenterException
     */
    public function realDestroy(Competition $competition)
    {
        $competition->tickets()->delete();

        $imagesPrepare = new ImagesPrepare();
        $imagesPrepare->deleteLocalFile($competition->present()->getLocalPath($competition->main_image));

        $competition->forceDelete();
    }
}
