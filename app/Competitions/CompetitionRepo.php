<?php

namespace App\Competitions;

use App\app\Competitions\Dashboard\CompetitionsFilter;
use App\Enums\CompetitionStatusEnum;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Webmagic\Core\Entity\EntityRepo;

class CompetitionRepo extends EntityRepo
{
	protected $entity = Competition::class;

	protected $sorting_fields = [
	    'created_at',
	    'title',
	    'price',
        'deadline'
    ];

    /**
     * @param int|null                $perPage
     * @param CompetitionsFilter|null $filter
     *
     * @return LengthAwarePaginator|Collection
     * @throws Exception
     */
    public function getUnfinishedCompetitions(int $perPage = null, CompetitionsFilter $filter = null)
    {
        $query = $this->query();

        if($filter){
            $this->applyFilter($query, $filter);
        }

        $this->applyUnfinishedFilter($query);

        return $this->realGetMany($query, $perPage);
	}

    /**
     * @param int|null                $perPage
     * @param CompetitionsFilter|null $filter
     *
     * @return LengthAwarePaginator
     * @throws Exception
     */
    public function getFinishedCompetitions(int $perPage = null, CompetitionsFilter $filter = null): LengthAwarePaginator
    {
        $query = $this->query();

        if($filter){
            $this->applyFilter($query, $filter);
        }

        $this->applyFinishedFilter($query);

        return $this->realGetMany($query, $perPage);
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    protected function applyFinishedFilter(Builder $query)
    {
        return $query->whereIn('status', [
            CompetitionStatusEnum::FINISHED
        ]);
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    protected function applyUnfinishedFilter(Builder $query)
    {
        return $query->whereIn('status', [
            CompetitionStatusEnum::LIVE,
            CompetitionStatusEnum::UNPUBLISHED,
            CompetitionStatusEnum::PAUSED,
            CompetitionStatusEnum::SOLD_OUT
        ]);
	}

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    protected function addOrdering(Builder $query): Builder
    {
        return $this->positionSorting($query);
    }


    /**
     * @param Builder            $builder
     * @param CompetitionsFilter $filter
     *
     * @return Builder
     */
    protected function applyFilter(Builder $builder, CompetitionsFilter $filter)
    {
        if($filter->isSearchPhraseDefined()){
            $builder->where('title', 'LIKE', '%'.$filter->getSearchPhrase().'%');
        }

        return $builder;
	}

    /**
     * Get all with paginate and filter
     *
     * @param null $keyword
     * @param null $per_page
     * @param null $page
     * @param string $sortBy
     * @param string $sort
     * @param bool $active_filter
     * @param bool $with_sold_tickets
     * @param bool $only_trashed
     * @param bool $finished
     * @param array $without
     * @return LengthAwarePaginator|Builder[]|Collection
     * @throws Exception
     */
    public function getByFilter(
        $keyword = null,
        $per_page = null,
        $page = null,
        $sortBy = 'position',
        $sort = 'asc',
        $active_filter = false,
        $with_sold_tickets = true,
        bool $only_trashed = false,
        bool $finished = false,
        array $without = []
    ){
        $query = $this->query();

        if ($keyword) {
            $query->where('title', 'LIKE', '%'.$keyword.'%');
        }

        if (in_array($sortBy, $this->sorting_fields)){
            $query->orderBy($sortBy, $sort);
        }

        if ($sortBy == 'tickets'){
            $query->withCount('tickets as remain_tickets')
                ->orderByRaw('(tickets_count - remain_tickets) desc');
        }

        if ($sortBy == 'position'){
            $this->positionSorting($query);
        }

        if ($sortBy == 'sold_out'){
            $this->soldOutSorting($query, $sort);
            $this->positionSorting($query);
        }

        if ($active_filter) {
            $this->addActiveFilter($query);
        }

        if ($with_sold_tickets){
            $query->with('sold_tickets');
        }

        if ($only_trashed) {
            $query->onlyTrashed();
        }
        if ($finished) {
            $query->where('status',CompetitionStatusEnum::FINISHED());
        } else {
            $query->where('status','!=',CompetitionStatusEnum::FINISHED());
        }
        if ($without) {
            $query->whereNotIn('id', $without);
        }

        if (is_null($per_page) && is_null($page)) {
            return $query->get();
        }

        return $query->paginate($per_page, ['*'], 'page', $page);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    protected function positionSorting(Builder $query): Builder
    {
        $sortableField = $this->entity::getSortableField();
        return $query->orderBy($sortableField);
    }

    /**
     * @param Builder $query
     * @param $sort
     * @return Builder
     */
    protected function soldOutSorting(Builder $query, $sort): Builder
    {
        return $query->orderByRaw("FIELD(status , 'Sold out') $sort");
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    protected function addActiveFilter(Builder $query): Builder
    {
        return $query->whereIn('status', [
            CompetitionStatusEnum::LIVE,
            CompetitionStatusEnum::SOLD_OUT,
            CompetitionStatusEnum::PAUSED,
            CompetitionStatusEnum::FINISHED,
      ]);
    }

    /**
     * @param $status
     * @param bool $with_sold_tickets
     * @return LengthAwarePaginator|Collection
     * @throws Exception
     */
    public function getByStatus($status, $with_sold_tickets = false)
    {
        $query = $this->query();
        if(is_array($status)){
            $query->whereIn('status', $status);
        } else {
            $query->where('status', $status);
        }

        $query->orderBy('position', 'asc');
//        $query->orderBy('created_at', 'desc');

        if($with_sold_tickets){
            $query->with('sold_tickets');
        }

        return $this->realGetMany($query);
    }


    /**
     * Get entity by ID
     *
     * @param $id
     *
     * @param bool $with_tickets
     * @param bool $with_trashed
     * @return Model|null
     * @throws Exception
     */
    public function getByID($id, bool $with_tickets = false, bool $with_trashed = false)
    {
        $query = $this->query();
        $query->where('id', $id);

        if($with_tickets){
            $query->with('tickets');
        }

        if ($with_trashed){
            $query->withTrashed();
        }

        return $this->realGetOne($query);
    }

    /**
     * Get entity by slug
     *
     * @param string $slug
     * @param bool $with_tickets
     * @return Model|null
     * @throws Exception
     */
    public function getBySlug(string $slug, $with_tickets = false)
    {
        $query = $this->query();
        $query->where('slug', $slug);

        if($with_tickets){
            $query->with('tickets');
        }

        return $this->realGetOne($query);
    }

    /**
     * Get first entity by position field
     *
     * @return Model|null
     * @throws Exception
     */
    public function getFirstByPosition()
    {
        $query = $this->query();

        $query->orderBy($this->entity::getSortableField());

        return $this->realGetOne($query);
    }
}
