<?php


namespace App\Competitions\Dashboard;


use App\Competitions\Competition;
use Exception;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Components\TablePageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Core\Renderable;

class FindWinnerPageGenerator extends FormPageGenerator
{
    /**
     * FindWinnerPageGenerator constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct(null);
    }

    /**
     * @param Competition $competition
     *
     * @return $this
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function prepare(Competition $competition)
    {
        $this
            ->title("$competition->title entries")
            ->method('POST')
            ->action(route('dashboard::competition-entries.find-winner', $competition->id))
            ->ajax(true)
            ->textInput('number', $competition, 'Enter a number', true)
            ->submitButtonTitle('Find');

        $this->getBox()->boxTitle("Find winner for competition \"$competition->title\"")->headerAvailable(true);

        $this->getForm()->content()
            ->addElement()->stringElement()->addClass('my-form-result');

        $this->getForm()->attr('data-result', '.my-form-result');


        return $this;
    }

    /**
     * @param TablePageGenerator $pageGenerator
     *
     * @return $this
     * @throws NoOneFieldsWereDefined
     */
    public function addEntriesTable(TablePageGenerator $pageGenerator)
    {
        $this->getPage()->addContent($pageGenerator->getBox());

        return $this;
    }
}