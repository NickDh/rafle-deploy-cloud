<?php

namespace App\Ecommerce\Cart;


use Carbon\Carbon;
use Illuminate\Support\Collection;

class CartStorageRepo
{
    /**
     * @param $key
     *
     * @return mixed
     */
    public function has($key)
    {
        return CartStorage::find($key);
    }

    /**
     * @param $key
     *
     * @return array|Collection
     */
    public function get($key)
    {
        if ($this->has($key)) {
            return new Collection(CartStorage::find($key)->cart_data);
        } else {
            return [];
        }
    }

    /**
     * @param $key
     * @param $value
     */
    public function put($key, $value)
    {
        if ($row = CartStorage::find($key)) {
            $row->cart_data = $value;
            $row->save();
        } else {
            CartStorage::create([
                'id' => $key,
                'cart_data' => $value
            ]);
        }
    }

    /**
     * @param $key
     */
    public function remove($key)
    {
        if ($this->has($key)) {
            CartStorage::destroy($key);
        }
    }

    /**
     * @param     $key
     * @param int $lifetime
     *
     * @return int|null
     */
    public function getTimeLeft($key, $lifetime = 5)
    {
        if ($this->has($key)){
            $time = CartStorage::find($key)->updated_at;
            if ($time) {
                $finishTime = $time->addMinutes($lifetime);
                $now = Carbon::now();
                $totalDuration = $finishTime->diff($now);
                if ($totalDuration->invert == 1) {
                    return $totalDuration->format('%i:%S');
                }
                return 0;
            }
        }

        return null;
    }

    /**
     * @param     $key
     * @param int $lifetime
     *
     * @return bool
     */
    public function checkIfLifetimeIsOver($key, $lifetime = 5)
    {
        if($this->getTimeLeft($key, $lifetime) == 0){
            return true;
        }
         return false;
    }
}
