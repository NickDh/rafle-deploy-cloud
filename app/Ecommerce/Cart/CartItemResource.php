<?php

namespace App\Ecommerce\Cart;


use App\Questions\Answer;
use App\Tickets\Ticket;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Laracasts\Presenter\Exceptions\PresenterException;


class CartItemResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Ticket $ticket
     * @param Answer $answer
     *
     * @return Collection
     * @throws PresenterException
     */
    public static function collect(Ticket $ticket, Answer $answer)
    {
        return collect([
            'qty'               => 1,
            'image'             => $ticket->competition->present()->main_image,
            'price'             => $ticket->competition->present()->actual_price,
            'item_price'        => $ticket->competition->present()->actual_price,
            'title'             => $ticket->competition->title,
            'ticket_number'     => $ticket->number,
            'ticket_id'         => $ticket->id,
            'competition'       => $ticket->competition->title,
            'competition_slug'  => $ticket->competition->slug,
            'competition_id'    => $ticket->competition->id,
            'answer'            => $answer->present()->stringAnswer(),
            'answer_id'         => $answer->id,
            'is_answer_correct' => $answer->is_correct
        ]);
    }
}
