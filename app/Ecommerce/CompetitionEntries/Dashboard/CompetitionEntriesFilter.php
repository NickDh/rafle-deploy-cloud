<?php

namespace App\Ecommerce\CompetitionEntries\Dashboard;


use App\Core\SessionFilter;
use Illuminate\Http\Request;

class CompetitionEntriesFilter extends SessionFilter
{
    /** @var null|string  */
    protected $searchPhrase = null;

    /** @var null|bool */
    protected $answerStatus = 'Any';

    /**
     * @param Request|null $request
     */
    public function initiate(Request $request = null)
    {
        $request = $request ?? request();

        $this->searchPhrase = $request->get('keyword', null);

        $this->answerStatus = $request->get('answer_status', 'Any');

        $this->updateSessionStorage('searchPhrase', 'answerStatus');
    }

    /**
     * Load params from session if exists
     */
    public function initiateFromSession()
    {
        $this->loadFromSessionStorage('searchPhrase', 'answerStatus');
    }

    /**
     * @return bool
     */
    public function hasSearchPhrase(): bool
    {
        return $this->searchPhrase != null;
    }

    /**
     * @return string|null
     */
    public function getSearchPhrase()
    {
        return $this->searchPhrase;
    }

    /**
     * @return bool
     */
    public function isAnswerStatusOptionActivated(): bool
    {
        return !in_array($this->answerStatus, ['Any', 'any']);
    }

    /**
     * @return bool|string|null
     */
    public function getAnswerStatus()
    {
        return $this->answerStatus;
    }

    /**
     * @return bool
     */
    public function showOnlyCorrectAnswers(): bool
    {
        return $this->answerStatus == 'Correct';
    }
}