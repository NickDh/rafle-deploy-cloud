<?php


namespace App\Ecommerce\CompetitionEntries;


use Illuminate\Support\Collection;
use Laracasts\Presenter\Exceptions\PresenterException;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class CompetitionEntriesExport implements FromCollection, WithMapping, WithHeadings
{
    /** @var Collection */
    protected $entries;

    /**
     * CompetitionEntriesExport constructor.
     *
     * @param Collection $entries
     */
    public function __construct(Collection $entries)
    {
        $this->entries = $entries;
    }

    /**
     * @inheritDoc
     */
    public function collection()
    {
        return $this->entries;
    }

    /**
     * @return string[]
     */
    public function headings(): array
    {
        return [
            'Order #', 'User', 'Tickets` numbers', 	'Answer', 'Answer status', 'Date'
        ];
    }

    /**
     * @param CompetitionEntry $competitionEntry
     *
     * @return array
     * @throws PresenterException
     */
    public function map($competitionEntry): array
    {
        return [
            $competitionEntry->order_id,
            $competitionEntry->user->present()->prepareFullName(),
            $competitionEntry->number,
            $competitionEntry->answer,
            $competitionEntry->present()->answerStatus(),
            $competitionEntry->present()->date()
        ];
    }
}