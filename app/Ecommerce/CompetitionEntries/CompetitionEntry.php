<?php

namespace App\Ecommerce\CompetitionEntries;

use App\app\Ecommerce\CompetitionEntries\CompetitionEntryPresenter;
use App\Competitions\Competition;
use App\Ecommerce\Orders\Order;
use App\Ecommerce\SelectedTickets\SelectedTicket;
use App\Enums\OrderStatusEnum;
use App\Users\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Webmagic\Core\Presenter\PresentableTrait;

/**
 * Class CompetitionEntry
 *
 * @package App\Ecommerce\CompetitionEntries
 * @method CompetitionEntryPresenter present()
 * @property int $id
 * @property string $competition_title
 * @property int $competition_id
 * @property int $user_id
 * @property string $question
 * @property int $question_id
 * @property string $answer
 * @property int $answer_id
 * @property int $is_answer_right
 * @property int $order_id
 * @property float $entry_price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Competition $competition
 * @property-read \Illuminate\Database\Eloquent\Collection|SelectedTicket[] $selectedTickets
 * @property-read int|null $selected_tickets_count
 * @property-read User $user
 * @method static \Illuminate\Database\Eloquent\Builder|CompetitionEntry newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CompetitionEntry newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CompetitionEntry query()
 * @method static \Illuminate\Database\Eloquent\Builder|CompetitionEntry whereAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CompetitionEntry whereAnswerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CompetitionEntry whereCompetitionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CompetitionEntry whereCompetitionTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CompetitionEntry whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CompetitionEntry whereEntryPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CompetitionEntry whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CompetitionEntry whereIsAnswerRight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CompetitionEntry whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CompetitionEntry whereQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CompetitionEntry whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CompetitionEntry whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CompetitionEntry whereUserId($value)
 * @mixin \Eloquent
 */
class CompetitionEntry extends Model
{
    use PresentableTrait;

    /** @var string  */
    protected $presenter = CompetitionEntryPresenter::class;

    /** @var string  */
    protected $table='competition_entries';

    /** @var string[]  */
    protected $fillable = [
        'competition_title',
        'competition_id',
        'user_id',
        'question',
        'question_id',
        'answer',
        'answer_id',
        'is_answer_right',
        'order_id',
        'entry_price'
    ];

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeWhereOrderPaid($query)
    {
        return $query->leftJoin('orders', 'competition_entries.order_id', '=', 'orders.id')
            ->where('orders.status', OrderStatusEnum::PAID());
    }

    /**
     * @return mixed
     */
    public function selectedTickets()
    {
        return $this->hasMany(SelectedTicket::class, 'competition_entry_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function competition()
    {
        return $this->belongsTo(Competition::class);
    }

    /**
     * @return bool
     */
    public function isAnswerCorrect(): bool
    {
        return (bool) $this->is_answer_right;
    }

    /**
     * @return BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}