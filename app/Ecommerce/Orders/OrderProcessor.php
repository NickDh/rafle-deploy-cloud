<?php


namespace App\Ecommerce\Orders;


use App\Ecommerce\Cart\SessionCart;
use App\Ecommerce\CompetitionEntries\CompetitionEntriesRepo;
use App\Ecommerce\Orders\Exceptions\CartOrTickersDeletedException;
use App\Ecommerce\Orders\Exceptions\NoUsablePaymentMethodException;
use App\Ecommerce\Orders\Exceptions\NumberAreTakenException;
use App\Ecommerce\Payment\FirstData\Transactions\Transaction;
use App\Users\User;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Throwable;
use Webmagic\Core\Entity\Exceptions\EntityNotExtendsModelException;
use Webmagic\Core\Entity\Exceptions\ModelNotDefinedException;

class OrderProcessor
{
    /**
     * @param User        $user
     * @param SessionCart $cart
     *
     * @return Model | Order
     * @throws Throwable
     */
    public function placeNewOrderAndClearCart(User $user, SessionCart $cart)
    {
        // Check if user has usable payment method
//        throw_if(!$user->hasUsablePaymentMethod(), NoUsablePaymentMethodException::class);

        $orderService = app(OrderService::class);

        // Check if cart and tickets exists
        throw_if(!$orderService->checkCartStatement($cart), CartOrTickersDeletedException::class);

        // Check if tickets taken in any active orders
        $orderRepo = app(OrderRepo::class);
        throw_if($orderRepo->isNumbersTaken(null, ... $cart->getTickets()), NumberAreTakenException::class);

        // Create order
        /** @var Order $order */
        $order = $orderService->createOrder($user, $cart);
        Log::channel('payment')->info("User $user->id order processing started");

        // Create competition entries and save selected tickets
        (new CompetitionEntriesRepo())->createBasedOnOrder($cart, $order);

        // Clear cart
        $cart->clear();
        Log::channel('payment')->info("User $user->id cart cleaned");

        return $order;
    }

    /**
     * @param Order       $order
     * @param Transaction $transaction
     *
     * @return Order|Model
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     * @throws Exception
     */
    public function processOrderByFinalizedTransaction(Order $order, Transaction $transaction)
    {
        $orderService = app(OrderService::class);

        // Process transactions approved without 3D secure
        if($transaction->isApproved()){
            Log::channel('payment')->info("User $order->user_id order $order->id confirmed");

            return $orderService->executeSuccessOrderScenario($order);
        }

        // Process transactions declined without 3D secure
        if($transaction->isDeclined() || $transaction->isFailed()){
            $order = $orderService->executeFailOrderScenario($order);
            Log::channel('payment')->info("User $order->user_id order $order->id marked as failed because of transaction was declined. All tickets returned");

            return $order;
        }

        return $order;
    }

    /**
     * @param Order $order
     *
     * @return Model
     * @throws Exception
     */
    public function processFreeOrder(Order $order)
    {
        $orderService = app(OrderService::class);

        // Update order variable for future funcitonality
        $order = $orderService->executeSuccessOrderScenario($order);

        Log::channel('payment')->error("Order $order->id processed without payment");

        return $order;
    }
}