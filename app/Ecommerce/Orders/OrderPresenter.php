<?php


namespace App\Ecommerce\Orders;


use Illuminate\Support\Carbon;
use Webmagic\Core\Presenter\Presenter;

class OrderPresenter extends Presenter
{
    /**
     * @return string
     */
    public function prettyCartDetails()
    {
        $details = '';

        foreach ($this->entity->cart_data as $competitionDetails){
            $details .= "\"{$competitionDetails['competition']}\": {$competitionDetails['ticket_number']};".PHP_EOL;
        }

        return $details;
    }

    /**
     * @return string
     */
    public function total()
    {
        return number_format($this->entity->total, 2);
    }

    /**
     * @return string
     */
    public function subtotal()
    {
        return number_format($this->entity->subtotal, 2);
    }

    /**
     * @return string
     */
    public function discount()
    {
        return number_format($this->entity->discount_value, 2);
    }

    /**
     * @return string
     */
    public function prettyCreatedDate(): string
    {
        return Carbon::parse($this->entity->created_at)->format('d/m/Y');
    }

    /**
     * @return string
     */
    public function prettyCreatedDateTime(): string
    {
        return Carbon::parse($this->entity->created_at)->format('d/m/Y @ H:i');
    }

    /**
     * Prepare structure for sending to Google Tag Manager
     *
     * @return array
     */
    public function googleTagManagerStructure()
    {
        $order = $this->entity;
        $data = [
            'event' => 'purchase',
        ];

        {
            $data['ecommerce']['purchase'] = [
                'transaction_id' => $order->id,
                'value' => $order->total,
                'currency' => 'GBP',
            ];
        }

        foreach($order->cart_data as $competitionId => $item){
            $data['ecommerce']['purchase']['items'][] = [
                'item_id' =>  $competitionId,
                'item_name' => $item['competition'],
                'item_price' => $item['item_price'],
                'quantity' => $item['qty']
            ];
        }

        return $data;
    }
}