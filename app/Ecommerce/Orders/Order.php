<?php

namespace App\Ecommerce\Orders;


use App\Ecommerce\CompetitionEntries\CompetitionEntry;
use App\Enums\OrderStatusEnum;
use App\Tickets\Ticket;
use App\Users\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany as BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Webmagic\Core\Presenter\PresentableTrait;

/**
 * Class Order
 *
 * @package App\Ecommerce\Orders
 * @method 
 * @property int $id
 * @property string|null $status
 * @property int|null $user_id
 * @property mixed $cart_data
 * @property float $subtotal
 * @property float $total
 * @property string|null $discount_code
 * @property string|null $discount_value
 * @property int|null $promo_code_id
 * @property string|null $customer_name
 * @property string|null $customer_email
 * @property string|null $customer_phone
 * @property string|null $customer_address
 * @property string|null $customer_county
 * @property string|null $customer_city
 * @property string|null $customer_post_code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $xref
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $viewed_notification
 * @property-read \Illuminate\Database\Eloquent\Collection|CompetitionEntry[] $competitionEntries
 * @property-read int|null $competition_entries_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Ticket[] $tickets
 * @property-read int|null $tickets_count
 * @property-read User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order newQuery()
 * @method static \Illuminate\Database\Query\Builder|Order onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCartData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCustomerAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCustomerCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCustomerCounty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCustomerEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCustomerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCustomerPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCustomerPostCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDiscountCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDiscountValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePromoCodeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereSubtotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereViewedNotification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereXref($value)
 * @method static \Illuminate\Database\Query\Builder|Order withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Order withoutTrashed()
 * @mixin \Eloquent
 */
class Order extends Model
{
    use SoftDeletes, PresentableTrait;

    /** @var string  */
    protected $presenter = OrderPresenter::class;

    protected $fillable = [
        'total',
        'subtotal',
        'discount_code',
        'discount_value',
        'cart_data',
        'status',
        'user_id',
        'promo_code_id',
        'customer_name',
        'customer_email',
        'customer_phone',
        'customer_address',
        'customer_county',
        'customer_city',
        'customer_post_code',
        'xref',
        'viewed_notification',
        'created_at'
    ];

    /**
     * @return HasMany
     */
    public function competitionEntries()
    {
        return $this->hasMany(CompetitionEntry::class);
    }

    /**
     * Check if order should be paid
     *
     * @return bool
     */
    public function shouldBePaid(): bool
    {
        return $this->total > 0;
    }

    /**
     * @return bool
     */
    public function isDiscountCodeApplied(): bool
    {
        return $this->discount_code != '';
    }

    /**
     * @param $value
     */
    public function setCartDataAttribute($value)
    {
        $this->attributes['cart_data'] = serialize($value);
    }

    /**
     * @return BelongsToMany
     */
    public function tickets()
    {
        return $this->belongsToMany(Ticket::class, 'orders_tickets', 'order_id','ticket_id',  'id');
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getCartDataAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Find all tickets id from cart field
     *
     * @return mixed
     */
    public function ticketsIDs()
    {
        foreach ($this->cart_data as $competition){
            $tickets = explode(';', $competition['ticket_id']);
            foreach ($tickets as $ticket){
                $data[] = (int)$ticket;
            }
        }
        return $data ?? [];
    }

    /**
     * Return only IDs for tickets where correct answer was selected
     *
     * @return array
     */
    public function ticketIDsWithCorrectAnswer(): array
    {
        foreach ($this->cart_data as $competition){
            if(!$competition['is_answer_correct']){
                continue;
            }

            $tickets = explode(';', $competition['ticket_id']);
            foreach ($tickets as $ticket){
                $data[] = (int)$ticket;
            }
        }
        return $data ?? [];
    }

    /**
     * Return only IDs for tickets where wrong answer was selected
     *
     * @return array
     */
    public function ticketIDsWithWrongAnswer(): array
    {
        foreach ($this->cart_data as $competition){
            if($competition['is_answer_correct']){
                continue;
            }

            $tickets = explode(';', $competition['ticket_id']);
            foreach ($tickets as $ticket){
                $data[] = (int)$ticket;
            }
        }
        return $data ?? [];
    }


    /**
     * Find all tickets id from cart field
     *
     * @return mixed
     */
    public function ticketsForChecking()
    {
        $tickets = [];
        foreach ($this->cart_data as $competition){
            $numbers = explode(', ', $competition['ticket_number']);

            foreach ($numbers as $number){
                $tickets[] = new Ticket(['number' => $number, 'competition_id' => $competition['competition_id']]);
            }
        }
        return $tickets;
    }

    /**
     * Check if order is in status Failed
     *
     * @return bool
     */
    public function isFailed()
    {
        return $this->status == OrderStatusEnum::FAILED;
    }

    /**
     * Check if order is in status Failed
     *
     * @return bool
     */
    public function isSuccessful()
    {
        return $this->status == OrderStatusEnum::PAID;
    }

    /**
     * @return bool
     */
    public function isProcessed(): bool
    {
        return $this->isSuccessful() || $this->isFailed();
    }

    /**
     * Check if order is in status Failed
     *
     * @return bool
     */
    public function isInProcessing()
    {
        return $this->status == OrderStatusEnum::PROCESSING;
    }

    /**
     * Calculate count of Competition Records with correct answer
     *
     * @return mixed
     */
    public function successfulEntriesCount()
    {
        $count = $this->competitionEntries->reduce(function ($carry, CompetitionEntry $item){
            return (int) $item->isAnswerCorrect();
        });

        return $count;
    }
}
