<?php


namespace App\Ecommerce\Orders\Exceptions;


class OrdersEmailsSendingFailed extends \Exception
{
    /** @var string  */
    protected $message = 'Orders emails sending failed';
}