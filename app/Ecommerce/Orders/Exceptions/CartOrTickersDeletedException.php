<?php


namespace App\Ecommerce\Orders\Exceptions;


class CartOrTickersDeletedException extends \Exception
{
    /** @var string */
    protected $message = 'Cart or tickets has already deleted';
}