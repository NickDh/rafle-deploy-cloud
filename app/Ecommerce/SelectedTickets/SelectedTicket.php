<?php


namespace App\Ecommerce\SelectedTickets;


use App\Ecommerce\CompetitionEntries\CompetitionEntry;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Ecommerce\SelectedTickets\SelectedTicket
 *
 * @property int $id
 * @property int $competition_entry_id
 * @property string $ticket
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|SelectedTicket newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SelectedTicket newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SelectedTicket query()
 * @method static \Illuminate\Database\Eloquent\Builder|SelectedTicket whereCompetitionEntryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SelectedTicket whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SelectedTicket whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SelectedTicket whereTicket($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SelectedTicket whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SelectedTicket extends Model
{
    /** @var string[]  */
    protected $fillable = [
          'competition_entry_id',
          'ticket'
    ];
}