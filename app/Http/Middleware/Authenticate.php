<?php

namespace App\Http\Middleware;

use \Illuminate\Auth\Middleware\Authenticate as BaseAuthenticate;
use Illuminate\Http\Request;

class Authenticate extends BaseAuthenticate
{

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if(strpos($request->path(), 'dashboard') !== false){
            return route('login');
        }

        return route('user.login');
    }
}
