<?php

namespace App\Http\Controllers;


use App\Competitions\CompetitionRepo;
use App\Ecommerce\CompetitionEntries\CompetitionEntriesRepo;
use App\Enums\CompetitionStatusEnum;
use App\HtmlPages\HtmlPageRepo;
use App\LiveDraws\LiveDrawRepo;
use App\Mail\ContactFormMail;
use App\Pages\PagesRepo;
use App\PastWinners\PastWinnerRepo;
use App\Results\ResultRepo;
use App\Tickets\TicketService;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;
use App\Settings\SettingsRepo;

class MainController extends Controller
{
    /**
     * @param CompetitionRepo $competitionRepo
     * @param LiveDrawRepo $liveDrawRepo
     * @param PastWinnerRepo $pastWinnerRepo
     * @param Request $request
     * @return Factory|View
     * @throws Exception
     */
    public function main(CompetitionRepo $competitionRepo, LiveDrawRepo $liveDrawRepo, PastWinnerRepo $pastWinnerRepo, Request $request)
    {
        $competitionsCount = config('project.competitions_for_home_page');

        $competitions = $competitionRepo->getByStatus([CompetitionStatusEnum::LIVE, CompetitionStatusEnum::SOLD_OUT])->take($competitionsCount);

        $live_draw = $liveDrawRepo->getAllActive()->first();

        $winners = $pastWinnerRepo->getByFilter(
            $request->get('items', 10)
        );

        return view('main', compact('competitions', 'live_draw', 'winners'));
    }

    /**
     * @param LiveDrawRepo $liveDrawRepo
     * @return JsonResponse
     * @throws Exception
     */
    public function getNextAvailableLiveDraw(LiveDrawRepo $liveDrawRepo)
    {
        $live_draw = $liveDrawRepo->getAllActive()->first();

        if(! $live_draw){
            return response()->json([]);
        }
        return response()->json([
            "time" => $live_draw ? convertDateForJsTimer($live_draw->date) : '',
            "text" => $live_draw ? 'NEXT LIVE DRAW ' .date('d/m/y \A\T H:i', strtotime($live_draw->date)). $live_draw->draw : ''
        ]);
    }


    protected $frontSortTranslate = [
        'Default' => 'position',
        'Name' => 'title',
        'Price' => 'price',
        'Remaining Tickets' => 'tickets',
        'Sold Out' => 'sold_out',
    ];

    /**
     * @param CompetitionRepo $competitionRepo
     * @param SettingsRepo    $settingsRepo
     *
     * @return Factory|View
     * @throws Exception
     */
    public function competitions(CompetitionRepo $competitionRepo, SettingsRepo $settingsRepo)
    {
        $perPage = $settingsRepo->getViewSortBy();

        $competitions = $competitionRepo->getByFilter('', $perPage, \request()->get('page', null), 'position', 'asc',true,true);

        $competitions = $competitions->links()->paginator;

        return view('live-competitions', compact('competitions'));
    }

    /**
     * @param CompetitionRepo $competitionRepo
     * @param Request $request
     * @param SettingsRepo $settingsRepo
     * @return Application|Factory|View
     * @throws Exception
     */
    public function finishedCompetitions(CompetitionRepo $competitionRepo, Request $request, SettingsRepo $settingsRepo)
    {
        $viewSortBySelect = $settingsRepo->getViewSortBy();
        $viewSortByOptions = config('project.view_sort_by');

        $validator = Validator::make($request->all(), [
            'items' => 'int',
            'sort_by' => 'string|in:deadline',
            'page' => 'int'
        ]);

        if (!$validator->passes()) {
            $filter = [
                'items' => $viewSortBySelect,
                'page' => 1,
                'sort_by' => 'deadline'
            ];
        } else{
            $filter = [
                'items' => $request->get('items', $viewSortBySelect),
                'sort_by' => 'deadline',
                'page' => $request->get('page', 1),
            ];
        }

        $sortOptions = $this->frontSortTranslate;

        $competitions = $competitionRepo->getByFilter(
            '',
            $filter['items'],
            $filter['page'],
            $filter['sort_by'],
            'desc',
            true,
            true,
            false,
            true
        );

        $competitions = $competitions->appends($filter)->links()->paginator;

        return view('finished-competitions', compact('competitions', 'filter', 'viewSortByOptions', 'sortOptions'));
    }

    /**
     * @param string $slug
     * @param CompetitionRepo $competitionRepo
     * @param TicketService $ticketService
     * @return Factory|View
     * @throws Exception
     */
    public function competition(string $slug, CompetitionRepo $competitionRepo, TicketService $ticketService)
    {
        if(!$competition = $competitionRepo->getBySlug($slug, true)){
            abort(404);
        }

        if(CompetitionStatusEnum::UNPUBLISHED()->is($competition->status)){
            abort(404);
        }

        $competition->grouped_tickets = $ticketService->prepareGroupedTicketsForCompetition($competition);
        $competitions = $competitionRepo->getByFilter(
            null,
            3,
            1,
            'position',
            'desc',
            true,
            true,
            false,
            false,
            [
                $competition->getKey(),
            ]
        );

        $maxRandomValues = $competition->maxTicketsForRandomGeneration();

        if(auth()->user()){
            $ticketsAvailableToBuyForUser = auth()->user()->availableTicketsToBuy($competition->max_tickets_per_user, $competition->id);
            $maxRandomValuesForNoTicketsFeature = $ticketsAvailableToBuyForUser < $maxRandomValues ? $ticketsAvailableToBuyForUser : $maxRandomValues;
        } else{
            $maxRandomValuesForNoTicketsFeature = $maxRandomValues;
            $ticketsAvailableToBuyForUser = $maxRandomValues;
        }

        return view('competition', compact('competition', 'competitions', 'maxRandomValues', 'maxRandomValuesForNoTicketsFeature', 'ticketsAvailableToBuyForUser'));
    }

    /**
     * @param int $item_id
     * @param CompetitionRepo $competitionRepo
     * @return JsonResponse|null
     * @throws Exception
     */
    public function checkCompetitionDeadline(int $item_id, CompetitionRepo $competitionRepo)
    {
        if(!$competition = $competitionRepo->getByID($item_id, true)){
            abort(404);
        }

        return response()->json(["time" => convertDateForJsTimer($competition->realDeadline())]);
    }

    /**
     * @param PastWinnerRepo $winnerRepo
     * @param Request $request
     * @return Factory|View
     * @throws Exception
     */
    public function winners(PastWinnerRepo $winnerRepo, Request $request)
    {
        $winners = $winnerRepo->getByFilter(
            $request->get('items', config('project.items_count_default')),
            $request->get('page', 1),
            $request->get('sort_by', 'created_at'),
            $request->get('sort', 'desc')
        );

        return view('winners', compact('winners'));
    }

    /**
     * Send message from contact from
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function contactForm(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'policy' => 'required',
            'message' => 'required',
            'email' => 'required|email',
        ]);

        if (! $validator->passes()) {
            return response()->json(['error'=> $validator->errors()->all()],500);
        }

        $validator = Validator::make($request->all(), [
            'recaptcha' => 'recaptcha'
        ]);

        if (! $validator->passes()) {
            return response()->json(['error'=> $validator->errors()->all()],500);
        }

        try {
            Mail::queue(new ContactFormMail($request->email, $request->message, $request->get('name')));
        } catch (Exception $ex) {
            return response()->json(['error'=> ['Something went wrong']],500);
        }


        return response()->json(['message' => 'Thank you for your email we will reply to your query as soon as possible.'], 200);
    }


    /**
     * @param Request $request
     * @param ResultRepo $resultRepo
     * @return Application|Factory|View
     * @throws Exception
     */
    public function results(Request $request, ResultRepo $resultRepo)
    {
        $results = $resultRepo->getByFilter(
            $request->get('items', config('project.items_count_default')),
            $request->get('page', 1)
        );

        return view('results', compact('results'));
    }

    /**
     * @param $item_id
     * @param ResultRepo $resultRepo
     * @return Application|Factory|View
     * @throws Exception
     */
    public function resultDetails($item_id, ResultRepo $resultRepo)
    {
        if(!$result = $resultRepo->getByID($item_id)){
            abort(404);
        }

        return view('result-details', compact('result'));
    }

    /**
     * @param string    $slug
     * @param PagesRepo $repo
     *
     * @return Application|Factory|View
     * @throws Exception
     */
    public function page(string $slug, PagesRepo $repo)
    {
        $page = $repo->getBySlug($slug);

        return view('page', compact('page'));
    }

    /**
     * @param string $slug
     * @param CompetitionRepo $competitionRepo
     * @param TicketService $ticketService
     * @return Application|Factory|View
     * @throws Exception
     */
    public function finishedCompetition(string $slug, CompetitionRepo $competitionRepo, TicketService $ticketService)
    {
        if(!$competition = $competitionRepo->getBySlug($slug, true)){
            abort(404);
        }

        if(CompetitionStatusEnum::UNPUBLISHED()->is($competition->status)){
            abort(404);
        }
        $competition->grouped_tickets = $ticketService->prepareGroupedTicketsForCompetition($competition);

        return view('finished-competition', compact('competition'));
    }

    /**
     * @param CompetitionEntriesRepo $entriesRepo
     *
     * @param Request                $request
     *
     * @return Application|Factory|View
     * @throws Exception
     */
    public function entries(CompetitionEntriesRepo $entriesRepo, Request $request)
    {
        $userId = auth()->user()->id;
        $perPage = 10;
        $entries = $entriesRepo->getLiveEntriesWithCorrectAnswerForUser($userId, $perPage);
        $finishedEntries = $entriesRepo->getFinishedEntriesWithCorrectAnswerForUser($userId, $perPage);

        if($request->ajax()){
            if($request->get('type', 'live') == 'finished'){
                $entries = $finishedEntries;
            }

            return view('parts._entries-table', ['entries' => $entries]);
        }

        return view('entries', compact('entries', 'finishedEntries'));
    }
}
