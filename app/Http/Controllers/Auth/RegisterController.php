<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\RegisterNewAccountMail;
use App\Users\Roles\Role;
use App\Users\User;
use App\Users\UserRepo;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Validation\ValidationException;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'user';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['guest']);
    }

    /**
     * @param Request  $request
     * @param UserRepo $userRepo
     *
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function register(Request $request, UserRepo $userRepo)
    {
        $validData = $this->validate($request, [
            'username'   =>  ['required', 'string', 'max:255', 'unique:users'],
            'email'      => 'required|string|email|max:255|unique:users',
            'password'   => 'required|string|min:6',
            'recaptcha'  => 'required|recaptcha',
            'first_name' => 'required|string',
            'last_name'  => 'required|string',
            'address'    => 'required|string',
            'town'       => 'required|string',
            'post_code'  => 'required|string',
            'phone'      => 'required|string',
            'subscription' => ['sometimes', 'in:on,off']
        ], [
            'recaptcha.recaptcha' => 'Wait for recaptcha to upload',
            'recaptcha.required'  => 'Wait for recaptcha to upload',
        ]);

        $validData['subscription'] = $request->get('subscription', 'off') === 'on' ? 1 : 0;

        $user = $userRepo->createUser($validData);

        event(new Registered($user));

        Mail::queue(new RegisterNewAccountMail($user));
        $this->guard()->login($user);

        return redirect()->route('user.info');
    }
}
