<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Marketing\Twilio\SmsRepo;
use App\Marketing\Twilio\SmsHistoryDashboardPresenter;
use App\Users\UserFilter;


use Exception;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Webmagic\Core\Controllers\AjaxRedirectTrait;
use Webmagic\Core\Entity\Exceptions\EntityNotExtendsModelException;
use Webmagic\Core\Entity\Exceptions\ModelNotDefinedException;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;

class SmsHistoryController extends Controller
{
    use AjaxRedirectTrait;


    /**
     * @param SmsRepo               $smsRepo
     * @param Dashboard              $dashboard
     * @param Request                $request
     * @param SmsHistoryDashboardPresenter $dashboardPresenter
     * @param UserFilter              $filter
     *
     * @return mixed|Dashboard
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function index(
        SmsRepo $smsRepo,
        Dashboard $dashboard,
        Request $request,
        SmsHistoryDashboardPresenter $dashboardPresenter,
        UserFilter $filter
    )
    {
        if($request->ajax()){
            $filter->initFromRequest();
        } else {
            $filter->initFromSession();
        }

        // get only with subsctions
        // check filter


        $items = $smsRepo->getByFilter(
            $filter->getSearchPhrase(),
            $request->get('items', 10),
            $request->get('page', 1)
        );

        $filter = null;

        return $dashboardPresenter->getTablePage($items, $dashboard, $request, $filter);
    }
//
//    /**
//     * @param UserDashboardPresenter $dashboardPresenter
//     * @return FormPageGenerator
//     * @throws FieldUnavailable
//     * @throws NoOneFieldsWereDefined
//     */
//    public function create(UserDashboardPresenter $dashboardPresenter)
//    {
//        return $dashboardPresenter->getCreateForm();
//    }

    /**
     * @param Request $request
     * @param UserRepo $userRepo
     * @return ResponseFactory|JsonResponse|RedirectResponse|Response|Redirector
     * @throws Exception
     */
    public function store(Request $request, UserRepo $userRepo)
    {
//        $request->validate([
//            'email' => 'required|email|unique:users,email',
//            'username' => 'required|unique:users,username'
//        ]);
//
//        if (!$userRepo->createUser($request->all())) {
//            return response('Error on creating', 500);
//        }

        return $this->redirect(route('dashboard::sms.history'));
    }


    /**
     * @param int $item_id
     * @param UserRepo $userRepo
     * @param Request $request
     * @return ResponseFactory|Response|void
     * @throws Exception
     */
    public function update(int $item_id, UserRepo $userRepo, Request $request)
    {
        // Status
//        $request->validate([
//            'email' => 'required|email|unique:users,email,'.$item_id,
//            'username' => 'required|unique:users,username,'.$item_id,
//        ]);

        if (!$item = $userRepo->getByID($item_id)) {
            return abort(404);
        }

        if(is_null($request->password)){
            $data = $request->except('password');
        } else{
            $data = $request->all();
        }

        if (!$userRepo->update($item->id, $data)) {
            return response('Error on updating', 500);
        }
    }

    /**
     * @param int $item_id
     * @param UserRepo $userRepo
     * @return JsonResponse|RedirectResponse|Redirector
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     * @throws Exception
     */
    public function destroy(int $item_id, UserRepo $userRepo)
    {
        if (!$user = $userRepo->getByID($item_id)) {
            abort(404);
        };

        $user->tickets()->delete();

        if (!$userRepo->destroy($item_id)) {
            abort(500, 'Error on destroying');
        }

        return $this->redirect(route('dashboard::users.index'));
    }


}
