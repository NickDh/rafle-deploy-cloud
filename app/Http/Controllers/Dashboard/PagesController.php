<?php


namespace App\Http\Controllers\Dashboard;


use App\Pages\Dashboard\PageFormPageGenerator;
use App\Pages\Dashboard\PagesTablePageGenerator;
use App\Pages\PagesRepo;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Webmagic\Core\Controllers\Controller;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;

class PagesController extends Controller
{
    /**
     * @param PagesRepo               $repo
     * @param PagesTablePageGenerator $pageGenerator
     *
     * @return PagesTablePageGenerator
     * @throws Exception
     */
    public function index(PagesRepo $repo, PagesTablePageGenerator $pageGenerator)
    {
        $perPage = 25;
        $pages = $repo->getAll( $perPage);

        $pageGenerator->itemsWithPagination($pages);

        if(\request()->ajax()){
            return $pageGenerator->getTable();
        }

        return $pageGenerator;
    }

    /**
     * @param PageFormPageGenerator $pageGenerator
     *
     * @return PageFormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function create(PageFormPageGenerator $pageGenerator)
    {
        $pageGenerator->prepare();

        return $pageGenerator;
    }

    /**
     * @param Request   $request
     * @param PagesRepo $repo
     *
     * @throws Exception
     */
    public function store(Request $request, PagesRepo $repo)
    {
        $validaData = $this->validateRequest($request);

        $repo->create($validaData);
    }

    /**
     * @param int                   $pageId
     * @param PageFormPageGenerator $pageGenerator
     *
     * @return PageFormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function edit(int $pageId, PageFormPageGenerator $pageGenerator)
    {
        $page = $this->getPageByIdOrAbort($pageId);

        $pageGenerator->prepare($page);

        return $pageGenerator;
    }

    /**
     * @param string    $pageId
     * @param Request   $request
     * @param PagesRepo $repo
     *
     * @throws Exception
     */
    public function update(string $pageId, Request $request, PagesRepo $repo)
    {
        $page = $this->getPageByIdOrAbort($pageId);
        $validaData = $this->validateRequest($request, $page->id);

        $repo->update($page->id, $validaData);
    }

    /**
     * @param Request  $request
     *
     * @param int|null $pageId
     *
     * @return array
     */
    protected function validateRequest(Request $request, int $pageId = null)
    {
        $uniqSlugRule = 'unique:pages,slug';
        $uniqSlugRule = $pageId ? "$uniqSlugRule,$pageId" : $uniqSlugRule;

        return $request->validate([
            'title' => ['required','string'],
            'slug' => ['required','string', $uniqSlugRule],
            'header_link' => ['in:0,1'],
            'footer_link' => ['in:0,1'],
            'footer_column' => ['in:1,2'],
            'content' => 'string'
        ]);
    }

    /**
     * @param int       $pageId
     * @param PagesRepo $repo
     *
     * @throws Exception
     */
    public function destroy(int $pageId, PagesRepo $repo)
    {
        $page = $this->getPageByIdOrAbort($pageId);

        $page->delete();
    }

    /**
     * @param int $pageId
     *
     * @return Model|null
     * @throws Exception
     */
    protected function getPageByIdOrAbort(int $pageId)
    {
        if(!$page = (new PagesRepo())->getByID($pageId)){
            abort(404);
        }

        return $page;
    }
}