<?php

namespace App\Http\Controllers\Dashboard;

use App\Enums\PromoCodeStatusTypesEnum;
use App\Http\Controllers\Controller;
use App\PromoCodes\PromoCodeDashboardPresenter;
use App\PromoCodes\PromoCodeRepo;
use App\PromoCodes\PromoCodeService;
use Exception;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Webmagic\Core\Controllers\AjaxRedirectTrait;
use Webmagic\Core\Entity\Exceptions\EntityNotExtendsModelException;
use Webmagic\Core\Entity\Exceptions\ModelNotDefinedException;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;

class PromoCodeController extends Controller
{
    use AjaxRedirectTrait;


    /**
     * @param PromoCodeRepo $promoCodeRepo
     * @param Dashboard $dashboard
     * @param Request $request
     * @param PromoCodeDashboardPresenter $dashboardPresenter
     * @param PromoCodeService $promoCodeService
     * @return mixed|Dashboard
     * @throws NoOneFieldsWereDefined
     * @throws Exception
     */
    public function index(
        PromoCodeRepo $promoCodeRepo,
        Dashboard $dashboard,
        Request $request,
        PromoCodeDashboardPresenter $dashboardPresenter,
        PromoCodeService $promoCodeService
    )
    {
        $promoCodeService->checkAndUpdateStatuses();

        $codes = $promoCodeRepo->getByFilter(
            $request->get('items', 10),
            $request->get('page', 1),
            'created_at',
            'desc',
            true
        );


        return $dashboardPresenter->getTablePage($codes, $dashboard, $request);
    }

    /**
     * @param PromoCodeDashboardPresenter $dashboardPresenter
     * @return FormPageGenerator
     * @throws NoOneFieldsWereDefined
     * @throws FieldUnavailable
     */
    public function create(PromoCodeDashboardPresenter $dashboardPresenter)
    {

        return $dashboardPresenter->getCreateForm();
    }

    /**
     * @param Request $request
     * @param PromoCodeRepo $promoCodeRepo
     * @return ResponseFactory|JsonResponse|RedirectResponse|Response|Redirector
     * @throws Exception
     */
    public function store(Request $request, PromoCodeRepo $promoCodeRepo)
    {
        $data = $request->all();
        $data['status'] = PromoCodeStatusTypesEnum::LIVE;

        if (!$promoCodeRepo->create($data)) {
            return response('Error on creating', 500);
        }

        return $this->redirect(route('dashboard::promo-codes.index'));
    }

    /**
     * @param int $item_id
     * @param PromoCodeRepo $promoCodeRepo
     * @param PromoCodeDashboardPresenter $dashboardPresenter
     * @return void|FormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function edit(int $item_id, PromoCodeRepo $promoCodeRepo, PromoCodeDashboardPresenter $dashboardPresenter)
    {
        if (!$code = $promoCodeRepo->getByID($item_id)) {
            return abort(404);
        }

        return $dashboardPresenter->getEditForm($code);
    }

    /**
     * @param int $item_id
     * @param PromoCodeRepo $promoCodeRepo
     * @param Request $request
     * @param PromoCodeService $promoCodeService
     * @return ResponseFactory|Response|void
     * @throws Exception
     */
    public function update(int $item_id, PromoCodeRepo $promoCodeRepo, Request $request, PromoCodeService $promoCodeService)
    {
        if (!$code = $promoCodeRepo->getByID($item_id)) {
            return abort(404);
        }

        if (!$promoCodeRepo->update($code->id, $request->all())) {
            return response('Error on updating', 500);
        }

        $promoCodeService->checkAndUpdateStatuses();
    }

    /**
     * @param int $item_id
     * @param PromoCodeRepo $promoCodeRepo
     * @return JsonResponse|RedirectResponse|Redirector
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     */
    public function destroy(int $item_id, PromoCodeRepo $promoCodeRepo)
    {
        if (!$promoCodeRepo->getByID($item_id)) {
            abort(404);
        };

        if (!$promoCodeRepo->destroy($item_id)) {
            abort(500, 'Error on destroying');
        }

        return $this->redirect(route('dashboard::promo-codes.index'));
    }
}
