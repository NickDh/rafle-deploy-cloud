<?php

namespace App\Http\Controllers\Dashboard;

use App\Competitions\CompetitionRepo;
use App\Competitions\CompetitionService;
use App\Http\Controllers\Controller;
use App\Trash\TrashDashboardPresenter;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Laracasts\Presenter\Exceptions\PresenterException;
use Webmagic\Core\Controllers\AjaxRedirectTrait;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;

class TrashController extends Controller
{
    use AjaxRedirectTrait;

    /**
     * @param Dashboard $dashboard
     * @param CompetitionRepo $competitionRepo
     * @param Request $request
     * @param TrashDashboardPresenter $dashboardPresenter
     * @return string
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     * @throws Exception
     */
    public function index(
        Dashboard $dashboard,
        CompetitionRepo $competitionRepo,
        Request $request,
        TrashDashboardPresenter $dashboardPresenter
    ) {
        $competitions = $competitionRepo->getByFilter(
            $request->get('keyword', ''),
            $request->get('items', 10),
            $request->get('page', 1),
            'deleted_at',
            'desc',
            false,
            false,
            true
        );

        return $dashboardPresenter->getTablePage($competitions, $request, $dashboard);
    }


    /**
     * @param int $item_id
     * @param CompetitionRepo $competitionRepo
     * @return JsonResponse|RedirectResponse|Redirector|void
     * @throws Exception
     */
    public function restoreCompetition(int $item_id, CompetitionRepo $competitionRepo)
    {
        if (! $competition = $competitionRepo->getByID($item_id, false, true)) {
            return abort(404);
        }

        $competition->restore();

        return $this->redirect(route('dashboard::trash.index'));
    }


    /**
     * Real destroy competitions with relations and related files
     *
     * @param int $item_id
     * @param CompetitionRepo $competitionRepo
     * @param CompetitionService $competitionService
     * @return JsonResponse|RedirectResponse|Redirector|void
     * @throws PresenterException
     * @throws Exception
     */
    public function destroyCompetition(
        int $item_id,
        CompetitionRepo $competitionRepo,
        CompetitionService $competitionService
    ){
        if (! $competition = $competitionRepo->getByID($item_id, false, true)) {
            return abort(404);
        }

        $competitionService->realDestroy($competition);

        return $this->redirect(route('dashboard::trash.index'));
    }

}
