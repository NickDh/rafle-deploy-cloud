<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\PastWinners\PastWinnerDashboardPresenter;
use App\PastWinners\PastWinnerRepo;
use App\Services\ImagePathManager;
use App\Services\ImagesPrepare;
use Exception;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Webmagic\Core\Controllers\AjaxRedirectTrait;
use Webmagic\Core\Entity\Exceptions\EntityNotExtendsModelException;
use Webmagic\Core\Entity\Exceptions\ModelNotDefinedException;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;

class PastWinnerController extends Controller
{
    use AjaxRedirectTrait;


    /**
     * @param PastWinnerRepo $pastWinnerRepo
     * @param Dashboard $dashboard
     * @param Request $request
     * @param PastWinnerDashboardPresenter $dashboardPresenter
     * @return mixed|Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function index(
        PastWinnerRepo $pastWinnerRepo,
        Dashboard $dashboard,
        Request $request,
        PastWinnerDashboardPresenter $dashboardPresenter
    )
    {
        $winners = $pastWinnerRepo->getByFilter(
            $request->get('items', 10),
            $request->get('page', 1)
        );


        return $dashboardPresenter->getTablePage($winners, $dashboard, $request);
    }

    /**
     * @param PastWinnerDashboardPresenter $dashboardPresenter
     * @return FormPageGenerator
     * @throws NoOneFieldsWereDefined
     * @throws FieldUnavailable
     */
    public function create(PastWinnerDashboardPresenter $dashboardPresenter)
    {

        return $dashboardPresenter->getCreateForm();
    }

    /**
     * @param Request $request
     * @param PastWinnerRepo $pastWinnerRepo
     * @param ImagesPrepare $imagesPrepare
     * @return ResponseFactory|JsonResponse|RedirectResponse|Response|Redirector
     * @throws Exception
     */
    public function store(Request $request, PastWinnerRepo $pastWinnerRepo, ImagesPrepare $imagesPrepare)
    {
        $request->validate([
            'main_image' => 'image:jpeg,png'
        ]);

        $data = $imagesPrepare->saveFiles(
            $request->all(),
            ['main_image'],
            (new ImagePathManager())->publicPathForPastWinnersImages()
        );


        if (!$pastWinnerRepo->create($data)) {
            return response('Error on creating', 500);
        }

        return $this->redirect(route('dashboard::past-winners.index'));
    }

    /**
     * @param int $item_id
     * @param PastWinnerRepo $pastWinnerRepo
     * @param PastWinnerDashboardPresenter $dashboardPresenter
     * @return void|FormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     * @throws Exception
     */
    public function edit(int $item_id, PastWinnerRepo $pastWinnerRepo, PastWinnerDashboardPresenter $dashboardPresenter)
    {
        if (!$code = $pastWinnerRepo->getByID($item_id)) {
            return abort(404);
        }

        return $dashboardPresenter->getEditForm($code);
    }

    /**
     * @param int $item_id
     * @param PastWinnerRepo $pastWinnerRepo
     * @param Request $request
     * @param ImagesPrepare $imagesPrepare
     * @return ResponseFactory|Response|void
     * @throws Exception
     */
    public function update(int $item_id, PastWinnerRepo $pastWinnerRepo, Request $request, ImagesPrepare $imagesPrepare)
    {
        $request->validate([
            'main_image' => 'image:jpeg,png'
        ]);

        $data = $imagesPrepare->saveFiles(
            $request->all(),
            ['main_image'],
            (new ImagePathManager())->publicPathForPastWinnersImages()
        );

        if (!$winner = $pastWinnerRepo->getByID($item_id)) {
            return abort(404);
        }

        if (isset($data['main_image']) && $data['main_image'] != $winner->main_image){
            $imagesPrepare->deleteLocalFile($winner->present()->getLocalPath($winner->main_image));
        }

        if (!$pastWinnerRepo->update($winner->id, $data)) {
            return response('Error on updating', 500);
        }
    }

    /**
     * @param int $item_id
     * @param PastWinnerRepo $pastWinnerRepo
     * @param ImagesPrepare $imagesPrepare
     * @return JsonResponse|RedirectResponse|Redirector
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     */
    public function destroy(int $item_id, PastWinnerRepo $pastWinnerRepo, ImagesPrepare $imagesPrepare)
    {
        if (!$winner = $pastWinnerRepo->getByID($item_id)) {
            abort(404);
        };

        $imagesPrepare->deleteLocalFile($winner->present()->getLocalPath($winner->main_image));

        if (!$pastWinnerRepo->destroy($item_id)) {
            abort(500, 'Error on destroying');
        }

        return $this->redirect(route('dashboard::past-winners.index'));
    }
}
