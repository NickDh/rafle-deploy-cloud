<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\LiveDraws\LiveDrawDashboardPresenter;
use App\LiveDraws\LiveDrawRepo;
use Exception;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Webmagic\Core\Controllers\AjaxRedirectTrait;
use Webmagic\Core\Entity\Exceptions\EntityNotExtendsModelException;
use Webmagic\Core\Entity\Exceptions\ModelNotDefinedException;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;

class LiveDrawController extends Controller
{
    use AjaxRedirectTrait;


    /**
     * @param LiveDrawRepo $liveDrawRepo
     * @param Dashboard $dashboard
     * @param Request $request
     * @param LiveDrawDashboardPresenter $dashboardPresenter
     * @return mixed|Dashboard
     * @throws NoOneFieldsWereDefined
     * @throws Exception
     */
    public function index(
        LiveDrawRepo $liveDrawRepo,
        Dashboard $dashboard,
        Request $request,
        LiveDrawDashboardPresenter $dashboardPresenter
    )
    {
        $items = $liveDrawRepo->getByFilter(
            $request->get('items', 10),
            $request->get('page', 1)
        );


        return $dashboardPresenter->getTablePage($items, $dashboard, $request);
    }

    /**
     * @param LiveDrawDashboardPresenter $dashboardPresenter
     * @return FormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function create(LiveDrawDashboardPresenter $dashboardPresenter)
    {

        return $dashboardPresenter->getCreateForm();
    }

    /**
     * @param Request $request
     * @param LiveDrawRepo $liveDrawRepo
     * @return ResponseFactory|JsonResponse|RedirectResponse|Response|Redirector
     * @throws Exception
     */
    public function store(Request $request, LiveDrawRepo $liveDrawRepo)
    {
        if (!$liveDrawRepo->create($request->all())) {
            return response('Error on creating', 500);
        }

        return $this->redirect(route('dashboard::live-draws.index'));
    }

    /**
     * @param int $item_id
     * @param LiveDrawRepo $liveDrawRepo
     * @param LiveDrawDashboardPresenter $dashboardPresenter
     * @return void|FormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     * @throws Exception
     */
    public function edit(int $item_id, LiveDrawRepo $liveDrawRepo, LiveDrawDashboardPresenter $dashboardPresenter)
    {
        if (!$item = $liveDrawRepo->getByID($item_id)) {
            return abort(404);
        }

        return $dashboardPresenter->getEditForm($item);
    }

    /**
     * @param int $item_id
     * @param LiveDrawRepo $liveDrawRepo
     * @param Request $request
     * @return ResponseFactory|Response|void
     * @throws Exception
     */
    public function update(int $item_id, LiveDrawRepo $liveDrawRepo, Request $request)
    {
        if (!$item = $liveDrawRepo->getByID($item_id)) {
            return abort(404);
        }


        if (!$liveDrawRepo->update($item->id, $request->all())) {
            return response('Error on updating', 500);
        }
    }

    /**
     * @param int $item_id
     * @param LiveDrawRepo $liveDrawRepo
     * @return JsonResponse|RedirectResponse|Redirector
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     * @throws Exception
     */
    public function destroy(int $item_id, LiveDrawRepo $liveDrawRepo)
    {
        if (!$liveDrawRepo->getByID($item_id)) {
            abort(404);
        };

        if (!$liveDrawRepo->destroy($item_id)) {
            abort(500, 'Error on destroying');
        }

        return $this->redirect(route('dashboard::live-draws.index'));
    }
}
