<?php


namespace App\Http\ViewComposers;


use App\Analytics\AnalyticsRepo;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\View\View;

class AnalyticsViewComposer
{
    /**
     * @param View $view
     * @throws BindingResolutionException
     * @throws Exception
     */
    public function compose(View $view)
    {
        if(!app()->environment('local')){

            $analytic = app()->make(AnalyticsRepo::class)->get();
            
            $view->with(compact('analytic'));
        }
    }
}