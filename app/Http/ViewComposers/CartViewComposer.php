<?php

namespace App\Http\ViewComposers;

use App\Ecommerce\Cart\CartStorageRepo;
use App\Ecommerce\Cart\SessionCart;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class CartViewComposer
{
    /**
     * @param View $view
     * @throws BindingResolutionException
     */
    public function compose(View $view)
    {
        if(Auth::check()){
            $user = Auth::user();

            $storageManager = new CartStorageRepo();
            $sessionCart = new SessionCart($storageManager);

            $cart = $sessionCart->key($user->id);

            $cartItems = $cart->getItemsQuantity();
            $cartLifeTime = $sessionCart->getLifeTimeLeft();

            $view->with(compact('cartItems', 'cartLifeTime', 'cart'));
        }
    }
}
