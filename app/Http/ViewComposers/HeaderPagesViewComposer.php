<?php


namespace App\Http\ViewComposers;


use App\Pages\PagesRepo;
use Illuminate\View\View;

class HeaderPagesViewComposer
{
    /**
     * @param View $view
     *
     * @throws \Exception
     */
    public function compose(View $view)
    {
        $headerLinksPages = (new PagesRepo())->getPagesForHeaderLinks();

        $view->with(compact('headerLinksPages'));
    }
}