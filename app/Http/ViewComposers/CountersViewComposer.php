<?php

namespace App\Http\ViewComposers;

use App\Settings\SettingsRepo;
use Exception;
use Illuminate\View\View;

class CountersViewComposer
{
    /**
     * @param View $view
     * @throws Exception
     */
    public function compose(View $view)
    {
        if(!app()->environment('local')){
            $settingsRepo = app()->make(SettingsRepo::class);
            $google_analytics = $settingsRepo->getGoogleAnalytics();

            $view->with(compact('google_analytics'));
        }
    }
}
