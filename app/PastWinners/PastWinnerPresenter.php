<?php

namespace App\PastWinners;

use Illuminate\Config\Repository;
use Illuminate\Support\Facades\Storage;
use Webmagic\Core\Presenter\Presenter as CorePresenter;

class PastWinnerPresenter extends CorePresenter
{
    /**
     * @return Repository|mixed
     */
    public function getConfigPath()
    {
        return config('project.past_winner_img_path');
    }

    /**
     * Main image
     *
     * @return string
     */
    public function main_image()
    {
        return $this->prepareImageURL($this->entity->main_image);
    }

    /**
     * Prepare url for images
     *
     * @param $file_name
     *
     * @return string
     */
    protected function prepareImageURL($file_name)
    {
        return Storage::disk('public')->url($this->getConfigPath()."/".$file_name);
    }

    /**
     * @param $file_name
     * @return string
     */
    public function getLocalPath($file_name)
    {
        return $this->getConfigPath()."/".$file_name;
    }
}
