<?php

namespace App\PromoCodes;

use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Webmagic\Core\Entity\EntityRepo;

class PromoCodeRepo extends EntityRepo
{
	protected $entity = PromoCode::class;

    /**
     * Get all with paginate and sorting
     *
     * @param null $per_page
     * @param null $page
     * @param string $sortBy
     * @param string $sort
     * @param bool $with_orders
     * @return LengthAwarePaginator|Builder[]|Collection
     * @throws Exception
     */
    public function getByFilter($per_page = null, $page = null, $sortBy = 'created_at', $sort = 'desc', $with_orders = false)
    {
        $query = $this->query();

        $query->orderBy($sortBy, $sort);

        if($with_orders){
            $query->with('orders');
        }

        if (is_null($per_page) && is_null($page)) {
            return $query->get();
        }

        return $query->paginate($per_page, ['*'], 'page', $page);
    }

    /**
     * Get code thar has expires expires_at field
     *
     * @return LengthAwarePaginator|Collection
     * @throws Exception
     */
    public function getExpiredCodes()
    {
        $query = $this->query();
        $query->whereDate('expires_at', '<', today());
        return $this->realGetMany($query);
    }

    /**
     * Find entity by code
     *
     * @param string $code
     * @return Model|null
     * @throws Exception
     */
    public function getByCode(string $code)
    {
        $query = $this->query();
        $query->where('code', $code);

        return $this->realGetOne($query);
    }
}
