<?php

namespace App\PromoCodes;


use App\Enums\PromoCodeTypesEnum;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Pagination\AbstractPaginator;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Components\TablePageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;

class PromoCodeDashboardPresenter
{

    /**
     * @param AbstractPaginator $promo_codes
     * @param Dashboard $dashboard
     * @param Request $request
     * @return mixed|Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function getTablePage(AbstractPaginator $promo_codes, Dashboard $dashboard, Request $request)
    {
        (new TablePageGenerator($dashboard->page()))
            ->title('Promo codes')
            ->tableTitles('ID', 'Code', 'Type', 'Amount', '#Used', 'Status',  'Actions')
            ->showOnly('id', 'code', 'type', 'value', 'used_count', 'status')
            ->setConfig([
                'used_count' => function (PromoCode $promoCode) {
                    return $promoCode->orders->count();
                },
            ])
            ->items($promo_codes)
            ->withPagination($promo_codes, route('dashboard::promo-codes.index',$request->all()))
            ->createLink(route('dashboard::promo-codes.create'))
            ->setEditLinkClosure(function (PromoCode $promoCode) {
                return route('dashboard::promo-codes.edit', $promoCode);
            })
        ;

        if ($request->ajax()) {
            return $dashboard->page()->content()->toArray()['box_body'];
        }

        return $dashboard;
    }


    /**
     * @return FormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function getCreateForm()
    {
        return (new FormPageGenerator())
            ->title('Add Promo Code')
            ->action(route('dashboard::promo-codes.store'))
            ->ajax(true)
            ->method('POST')
            ->textInput('code', false, 'Code', true)
            ->select('type', array_combine(PromoCodeTypesEnum::values(), PromoCodeTypesEnum::values()),1, 'Code Type', true)
            ->numberInput('value', false, 'Value', true, 1, 1 )
            ->numberInput('max_use', false, 'Maximum Use', false, 1, 1 )
            ->numberInput('max_use_per_user', false, 'Maximum Use Per User', false, 1, 1 )
            ->dateInput('expires_at', false, 'Expiry Date', false, ['min' => Carbon::now()->toDateString()])
            ->submitButtonTitle('Create');
    }


    /**
     * @param Model $promo_code
     * @return FormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function getEditForm(Model $promo_code)
    {
        $formPageGenerator = (new FormPageGenerator())
            ->title("Promo code editing" )
            ->action(route('dashboard::promo-codes.update', $promo_code))
            ->method('PUT')
            ->textInput('code', $promo_code, 'Code', true)
            ->select('type', array_combine(PromoCodeTypesEnum::values(), PromoCodeTypesEnum::values()),$promo_code, 'Code Type', true)
            ->numberInput('value', $promo_code, 'Value', true, 1, 1 )
            ->numberInput('max_use', $promo_code, 'Maximum Use', false, 1, 1 )
            ->numberInput('max_use_per_user', $promo_code, 'Maximum Use Per User', false, 1, 1 )
            ->dateInput('expires_at', $promo_code, 'Expiry Date', false, ['min' => Carbon::now()->toDateString()])
            ->submitButtonTitle('Update');

        $formPageGenerator->getBox()
            ->addElement('box_footer')
            ->linkButton()
            ->icon('fa-trash')
            ->content('Delete')
            ->class('btn-danger js_ajax-by-click-btn pull-left')
            ->dataAttr('action', route('dashboard::promo-codes.destroy', $promo_code->id))
            ->dataAttr('method', 'DELETE')
            ->dataAttr('confirm', true);

        return $formPageGenerator;
    }
}
