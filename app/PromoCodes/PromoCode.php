<?php

namespace App\PromoCodes;

use App\Ecommerce\Orders\Order;
use App\Enums\OrderStatusEnum;
use App\Enums\PromoCodeStatusTypesEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\PromoCodes\PromoCode
 *
 * @property int $id
 * @property string $code
 * @property string $type
 * @property int $value
 * @property int|null $max_use
 * @property int|null $max_use_per_user
 * @property string $status
 * @property string|null $expires_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Order[] $orders
 * @property-read int|null $orders_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Order[] $paid_orders
 * @property-read int|null $paid_orders_count
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode query()
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereExpiresAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereMaxUse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereMaxUsePerUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereValue($value)
 * @mixin \Eloquent
 */
class PromoCode extends Model
{
    protected $fillable = [
        'code',
        'type',
        'value',
        'max_use',
        'max_use_per_user',
        'status',
        'expires_at',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->status = PromoCodeStatusTypesEnum::LIVE;
        });

        static::updating(function ($model) {
            if(checkIfDateGreaterThanToday($model->expires_at) == false ){
                $model->status = PromoCodeStatusTypesEnum::EXPIRED;
            } else {
                $model->status = PromoCodeStatusTypesEnum::LIVE;
            }
        });
    }

    /**
     * @return HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    /**
     * @return HasMany
     */
    public function paid_orders()
    {
        return $this->orders()->where('status', OrderStatusEnum::PAID);
    }
}
