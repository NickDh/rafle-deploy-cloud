<?php

namespace App\Questions;

use Illuminate\Config\Repository;
use Illuminate\Support\Facades\Storage;
use Webmagic\Core\Presenter\Presenter as CorePresenter;

class AnswerPresenter extends CorePresenter
{
    /**
     * Present answer as a string
     *
     * @return string
     */
    public function stringAnswer(): string
    {
        if($this->entity->isText()){
            return $this->entity->text;
        }

        // Use answer position in the question's answers list as text identifier
        $currentAnswerId = $this->entity->id;
        $key = $this->entity->question->answers->search(function(Answer $answer) use ($currentAnswerId) {
            return $answer->id === $currentAnswerId;
        });
        $position = $key + 1;

        return "Option $position";
    }
    
    /**
     * @return Repository|mixed
     */
    public function getConfigPath()
    {
        return config('project.question_img_path');
    }

    /**
     * Main image
     *
     * @return string
     */
    public function image()
    {
        return $this->prepareImageURL($this->entity->image);
    }

    /**
     * Prepare url for images
     *
     * @param $file_name
     *
     * @return string
     */
    protected function prepareImageURL($file_name)
    {
        return Storage::disk('public')->url($this->getConfigPath()."/".$file_name);
    }

    /**
     * @param $file_name
     * @return string
     */
    public function getLocalPath($file_name)
    {
        return $this->getConfigPath()."/".$file_name;
    }
}
