<?php

namespace App\Questions;


use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Webmagic\Core\Entity\EntityRepo;

class QuestionRepo extends EntityRepo
{
	protected $entity = Question::class;

    /**
     * Get all with paginate and sorting
     *
     * @param null $per_page
     * @param null $page
     * @param string $sortBy
     * @param string $sort
     * @return LengthAwarePaginator|Builder[]|Collection
     * @throws \Exception
     */
    public function getByFilter($per_page = null, $page = null, $sortBy = 'created_at', $sort = 'desc')
    {
        $query = $this->query();

        $query->orderBy($sortBy, $sort);

        if (is_null($per_page) && is_null($page)) {
            return $query->get();
        }


        return $query->paginate($per_page, ['*'], 'page', $page);
    }


    /**
     * @param string $value
     * @param string $key
     * @param string $sortBy
     * @param string $sort
     * @return array
     * @throws \Exception
     */
    public function getForSelect($value = 'id', $key = 'id', $sortBy = 'created_at', $sort = 'desc'): array
    {
        $query = $this->query();

        $query->orderBy($sortBy, $sort);

        if (!$entities = $query->pluck($value, $key)) {
            return $entities->toArray();
        }

        return $entities->toArray();
    }
}
