<?php


namespace App\Core;


abstract class SessionFilter
{
    /**
     * @return mixed
     */
    public function getAll(): array
    {
        return session()->get($this->getBasicKey(), []);
    }

    /**
     * @param string      $name
     * @param string|null $value
     * @param string|null $default
     */
    protected function setOrLoadFromSession(string $name, string $value = null, string $default = null)
    {
        if($value !== null){

            $this->put($name, $value);
            $this->$name = $value;

        } elseif($this->get($name) !== null) {

            $this->$name = $this->get($name);

        } elseif($default !== null) {

            $this->$name = $default;

        }
    }

    /**
     * Update session attributes based on set
     *
     * @param mixed ...$attributes
     */
    protected function updateSessionStorage(... $attributes)
    {
        foreach ($attributes as $attribute){
            $this->setAttribute($attribute, $this->$attribute);
        }
    }

    /**
     * Load attributes from session if exists
     *
     * @param mixed ...$attributes
     */
    protected function loadFromSessionStorage(... $attributes)
    {
        foreach ($attributes as $attribute){
            if($this->get($attribute) !== null){
                $this->$attribute = $this->get($attribute);
            }
        }
    }

    /**
     * @param string $name
     */
    protected function resetAttribute(string $name)
    {
        session()->forget($name);
    }

    /**
     * @param string $name
     * @param  $value
     */
    protected function setAttribute(string $name, $value)
    {
        $this->put($name, $value);
        $this->{$name} = $this->get($name);
    }

    /**
     * @param string $key
     *
     * @return array|mixed
     */
    public function get(string $key)
    {
        return data_get($this->getAll(), $key, null);
    }

    /**
     * @param string      $key
     * @param string|null $value
     */
    public function put(string $key, string $value = null)
    {
        $data = $this->getAll();
        $data[$key] = $value;

        session()->put($this->getBasicKey(), $data);
    }

    /**
     * @return string
     */
    protected function getBasicKey(): string
    {
        $userId = auth()->user()->id;
        $url = str_slug(url()->current());
        $className = get_class($this);

        return "{$userId}_{$className}_$url";
    }
}