<?php

namespace App\Results;

use Illuminate\Config\Repository;
use Illuminate\Support\Facades\Storage;
use Webmagic\Core\Presenter\Presenter as CorePresenter;

class ResultPresenter extends CorePresenter
{
    /**
     * @return Repository|mixed
     */
    public function getConfigPath()
    {
        return config('project.result_img_path');
    }

    /**
     * Main image
     *
     * @return string
     */
    public function image()
    {
        return $this->prepareImageURL($this->entity->image);
    }

    /**
     * @param $file_name
     * @return string
     */
    public function getLocalPath($file_name)
    {
        return $this->getConfigPath()."/".$file_name;
    }

    /**
     * Prepare url for images
     *
     * @param $file_name
     *
     * @return string
     */
    protected function prepareImageURL($file_name)
    {
        return Storage::disk('public')->url($this->getLocalPath($file_name));
    }
}
