<?php

namespace App\Results;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Components\TablePageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;

class ResultDashboardPresenter
{

    /**
     * @param $items
     * @param Dashboard $dashboard
     * @param Request $request
     * @return mixed|Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function getTablePage($items, Dashboard $dashboard, Request $request)
    {
        (new TablePageGenerator($dashboard->page()))
            ->title('Results')
            ->tableTitles('ID', 'Title', 'Date', 'Actions')
            ->showOnly('id', 'title', 'date')
            ->setConfig([
                'date' => function (Result $result) {
                    return $result->getFormattedData();
                },
            ])
            ->items($items)
            ->withPagination($items, route('dashboard::results.index',$request->all()))
            ->createLink(route('dashboard::results.create'))
            ->setEditLinkClosure(function (Result $item) {
                return route('dashboard::results.edit', $item);
            })
        ;

        if ($request->ajax()) {
            return $dashboard->page()->content()->toArray()['box_body'];
        }

        return $dashboard;
    }


    /**
     * @return FormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function getCreateForm()
    {
        return (new FormPageGenerator())
            ->title('Add Result')
            ->action(route('dashboard::results.store'))
            ->ajax(true)
            ->method('POST')
            ->textInput('title', false, 'Title', true)
            ->visualEditor('content', false, 'Text', true)
            ->imageInput('image', '', 'Image')
            ->textInput('link', '', 'Link')
            ->submitButtonTitle('Create');
    }


    /**
     * @param Model $item
     * @return FormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function getEditForm(Model $item)
    {
        $formPageGenerator = (new FormPageGenerator())
            ->title("Past Winner editing" )
            ->action(route('dashboard::results.update', $item))
            ->method('PUT')
            ->textInput('title', $item, 'Title', true)
            ->visualEditor('content', $item, 'Text', true)
            ->imageInput('image', ['image' => $item->image ? $item->present()->image : ''], 'Image')
            ->textInput('link', $item, 'Link')
            ->submitButtonTitle('Update');

        $formPageGenerator->getBox()
            ->addElement('box_footer')
            ->linkButton()
            ->icon('fa-trash')
            ->content('Delete')
            ->class('btn-danger js_ajax-by-click-btn pull-left')
            ->dataAttr('action', route('dashboard::results.destroy', $item->id))
            ->dataAttr('method', 'DELETE')
            ->dataAttr('confirm', true);

        return $formPageGenerator;
    }
}
