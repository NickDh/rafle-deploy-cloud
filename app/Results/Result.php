<?php

namespace App\Results;

use Illuminate\Database\Eloquent\Model;
use Webmagic\Core\Presenter\PresentableTrait;
use Webmagic\Core\Presenter\Presenter;

/**
 * App\Results\Result
 *
 * @property int $id
 * @property string $title
 * @property string $image
 * @property string $link
 * @property string $content
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Result newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Result newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Result query()
 * @method static \Illuminate\Database\Eloquent\Builder|Result whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Result whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Result whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Result whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Result whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Result whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Result whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Result extends Model
{
    use PresentableTrait;

    /** @var  Presenter class that using for present model */
    protected $presenter = ResultPresenter::class;

    protected $fillable = [
        'title',
        'content',
        'image',
        'link',
    ];

    /**
     * @return false|string
     */
    public function getFormattedData()
    {
        return date('d F Y', strtotime($this->created_at));
    }
}
