<?php

namespace App\Jobs;

use App\Ecommerce\Cart\CartStorageRepo;
use App\Tickets\TicketRepo;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Webmagic\Core\Entity\Exceptions\EntityNotExtendsModelException;
use Webmagic\Core\Entity\Exceptions\ModelNotDefinedException;

class ClearCartJob extends Job
{
    /**
     * @var string
     */
    private $cartDataId;
    /**
     * @var string
     */
    private $discountId;
    /**
     * @var int
     */
    private $cartLifetime;


    /**
     * Create a new job instance.
     *
     * @param string $cartDataId
     * @param string $discountId
     * @param int $cartLifetime
     */
    public function __construct(string $cartDataId, string $discountId, int $cartLifetime)
    {
        $this->cartDataId = $cartDataId;
        $this->discountId = $discountId;
        $this->cartLifetime = $cartLifetime;
    }

    /**
     * Execute the job.
     *
     * @param CartStorageRepo $storageRepo
     * @param TicketRepo $ticketRepo
     * @return void
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     */
    public function handle(CartStorageRepo $storageRepo, TicketRepo $ticketRepo)
    {
        if(! $storageRepo->checkIfLifetimeIsOver($this->cartDataId, $this->cartLifetime)){
            return;
        }

        $cartItems = $storageRepo->get($this->cartDataId);

        if(count($cartItems)){
            foreach ($cartItems as $item){
                $ticketsIds = explode(';',$item['ticket_id']);
                $ticketRepo->destroy($ticketsIds);
            }
        }
        $storageRepo->remove($this->cartDataId);
        $storageRepo->remove($this->discountId);
    }

    public function failed( $exception)
    {
        logger('clear cart job failed: '. $this->cartDataId);
        Log::error($exception->getMessage());
    }
}
