<?php


namespace App\Analytics;


use Exception;
use Webmagic\Core\Entity\EntityRepo;

class AnalyticsRepo extends EntityRepo
{
    /** @var string  */
    protected $entity = Analytics::class;

    /**
     * AnalyticsRepo constructor.
     *
     * @return Analytics|null
     */
    public function get()
    {
        return Analytics::first();
    }
}