<?php


namespace App\Payment\Core;


use App\Ecommerce\Cart\SessionCart;
use App\Ecommerce\Orders\Order;
use App\Users\User;

interface PaymentGatewayContract
{

    /**
     * Check if current user has saved payment method
     *
     * @return bool
     */
    public function isSavedCardExists(): bool;

    /**
     * Return last 4 digits for saved card
     *
     * @return string
     */
    public function getSavedCardLast4Digits(): string;

    /**
     * Prepare and return checkout page
     *
     * @param SessionCart $cart
     * @param User        $user
     * @param bool        $useSavedCard
     * @param string      $orderCreatingUrl
     * @param string      $orderCreatingMethod
     *
     * @return string
     */
    public function getCheckoutPage(SessionCart $cart, User $user, bool $useSavedCard, string $orderCreatingUrl, string $orderCreatingMethod = 'POST'): string;

    /**
     * This function should be used for registering needed routes
     *
     * @return mixed
     */
    public function registerRoutes();

    /**
     * Check if payment transaction exists
     *
     * @param Order $order
     *
     * @return bool
     */
    public function hasTransactions(Order $order): bool;

    /**
     * Return transaction for payment if exists
     *
     * @param Order $order
     *
     * @return TransactionContract|null
     */
    public function getTransactions(Order $order);
}