<?php


namespace App\Payment\TrustPayment;


use App\Ecommerce\Orders\Order;
use App\Ecommerce\Orders\OrderRepo;
use App\Ecommerce\Orders\OrderService;
use App\Enums\OrderStatusEnum;
use Exception;
use Firebase\JWT\JWT;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Webmagic\Core\Entity\Exceptions\EntityNotExtendsModelException;
use Webmagic\Core\Entity\Exceptions\ModelNotDefinedException;

class TrustPaymentTransactionsProcessor
{
    /** @var Request */
    protected $request;

    /** @var bool */
    protected $transactionStatus;

    /** @var TrustPaymentTransaction */
    protected $transaction;

    /**
     * TrustPaymentTransactionsProcessor constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return Application|Factory|View
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     * @throws Exception
     */
    public function finalizeOrderProcessing()
    {
        $transaction = $this->createTransaction();

        // Update order status based on transaction if possible
        if($transaction){
            $order = $this->updateOrderStatus($transaction->isSuccessful(), $transaction->order);

            return redirect(route('order.payment-result', ['order_id' => $order->id]));
        }

        // If transaction failed mark order failed if possible to recognize the order
        $order = $this->getOrderBasedOnRequest();
        if($order){
            $this->updateOrderStatus(false, $order);

            \Log::channel('payment')->info("Order #$order->id was marked as failed");

            return redirect(route('order.payment-result', ['order_id' => $order->id]));
        }

        $orderId = $this->request->get('order_id', '???');
        \Log::channel('payment')->info("Order #$orderId was not recognized");

        // Show payment error page with mocked order
        $order = new Order();
        $order['status'] = OrderStatusEnum::FAILED;
        return view('payment-result', compact('order'));
    }

    /**
     * @param bool  $status
     * @param Order $order
     *
     * @return Order| Model
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     */
    protected function updateOrderStatus(bool $status, Order $order): Order
    {
        $orderService = app(OrderService::class);

        if ($this->isJWTValid() && $status && !$order->isFailed()) {
            return $orderService->executeSuccessOrderScenario($order);
        }

        return $orderService->executeFailOrderScenario($order);
    }

    /**
     * Decode JWT token and prepare data
     */
    protected function isJWTValid(): bool
    {
        try {
            $jwt = $this->request->get('jwt');
            $jwtSecret = config('payment.gateways.trustPayment.jwt.secret');
            JWT::decode($jwt, $jwtSecret, ['HS256']);
            return true;
        } catch (Exception $exception) {
            return false;
        }
    }

    /**
     * @return TrustPaymentTransaction|Model|bool
     * @throws Exception
     */
    protected function createTransaction()
    {
        $defaultData = [
            'errorcode' => 0,
            'status' => '',
            'transactionreference' => '',
            'baseamount' => 0,
            'eci' => 0,
            'currencyiso3a' => '',
            'errormessage' => '',
            'enrolled' => '',
            'settlestatus' => '',
            'jwt' => '',
            'use_for_next_payment' => 0
        ];

        // Validate request data
        $validator = \Validator::make($this->request->all(), [
            'order_id'             => ['required', 'integer'],
            'errorcode'            => ['string'],
            'status'               => ['string'],
            'transactionreference' => ['required', 'string'],
            'baseamount'           => ['string'],
            'eci'                  => ['string'],
            'currencyiso3a'        => ['string'],
            'errormessage'         => ['string'],
            'enrolled'             => ['string'],
            'settlestatus'         => ['string'],
            'jwt'                  => ['required', 'string'],
            'save_payment_method'  => ['boolean'],
            'user_id'              => ['required', 'integer']
        ]);

        // Process
        if ($validator->fails()) {
            \Log::channel('payment')->error('Trust Payment request/transaction validation error'.PHP_EOL.json_encode($this->request->all()).PHP_EOL.json_encode($validator->errors()));

            return null;
        }

        // Create transaction with valida data
        $data = $validator->validated();
        $data = array_merge($defaultData, $data);

        $data['use_for_next_payment'] = $data['save_payment_method'];
        return (new TrustPaymentTransactionsRepo())->create($data);
    }

    /**
     * Get order based on the request data
     *
     * @return Model|null
     * @throws Exception
     */
    protected function getOrderBasedOnRequest(): ?Order
    {
        $orderId = (int)$this->request->get('order_id', null);

        if(!$orderId){
            return null;
        }

        return (new OrderRepo())->getByID($orderId);
    }
}