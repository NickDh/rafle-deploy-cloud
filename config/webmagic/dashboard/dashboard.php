<?php

return [
    /*
     |--------------------------------------------------------------------------
     | Dashboard title
     |--------------------------------------------------------------------------
     */
    'title' => 'Raffle Labs ',

    /*
     |--------------------------------------------------------------------------
     | Dashboard logo configuration
     |--------------------------------------------------------------------------
     */
    'logo' => [
        'link' => '/dashboard',
        'icon' => '/img/raffle-labs-white.svg',
        'text' => ''
    ],

    /*
     |--------------------------------------------------------------------------
     | Menu configuration
     |--------------------------------------------------------------------------
     */
    'menu'              => [
        [
            'text' => 'Competitions',
            'icon' => 'fa-stream',
            'rank' => 0,
            'subitems' => [
                [
                    'text' => 'Live Competitions',
                    'icon' => 'fa-meteor',
                    'link' => 'dashboard/competitions',
                    'active_rules' => [
                        'routes_parts' => [
                            'dashboard::competitions.',
                        ],
                    ],
                ],

                [
                    'text' => 'Finished Competitions ',
                    'icon' => 'fa-ticket-alt',
                    'link' => 'dashboard/competition-entries',
                    'active_rules' => [
                        'routes_parts' => [
                            'dashboard::competition-entries.',
                        ],
                    ],
                ],

                [
                    'text' => 'Competition Questions',
                    'icon' => 'fa-question-circle',
                    'link' => 'dashboard/questions',
                    'active_rules' => [
                        'routes_parts' => [
                            'dashboard::questions.',
                        ],
                    ],
                ],

                [
                    'text' => 'Draws',
                    'icon' => 'fa-video',
                    'rank' => 0,
                    'link' => 'dashboard/live-draws',
                    'active_rules' => [
                        'routes_parts' => [
                            'dashboard::live-draws.',
                        ],
                    ],
                ],
            ],
        ],

        [
            'text' => 'Winners',
            'icon' => 'fa-child',
            'rank' => 0,
            'link' => 'dashboard/past-winners',
            'active_rules' => [
                'routes_parts' => [
                    'dashboard::past-winners.',
                ],
            ],
        ],

        [
            'text' => 'User Management',
            'icon' => 'fa-user',
            'rank' => 0,
            'link' => 'dashboard/users',
            'active_rules' => [
                'routes_parts' => [
                    'dashboard::users.',
                ],
            ],
        ],

        [
            'text' => 'Order Management',
            'icon' => 'fa-coins',
            'rank' => 0,
            'subitems' => [
                [
                    'text' => 'Orders',
                    'icon' => 'fa-coins',
                    'link' => 'dashboard/orders',
                    'active_rules' => [
                        'routes_parts' => [
                            'dashboard::orders.',
                        ],
                    ],
                ],

                [
                    'text' => 'Promo Codes',
                    'icon' => 'fa-tags',
                    'rank' => 0,
                    'link' => 'dashboard/promo-codes',
                    'active_rules' => [
                        'routes_parts' => [
                            'dashboard::promo-codes.',
                        ],
                    ],
                ],

                [
                    'text' => 'Accounts',
                    'icon' => 'fa-circle',
                    'link' => 'dashboard/statistics',
                    'active_rules' => [
                        'routes_parts' => [
                            'dashboard::statistics.',
                        ],
                    ],
                ],

            ],
        ],

        [
            'text' => 'Pages',
            'icon' => 'fa-file',
            'rank' => 0,
            'link' => 'dashboard/pages/page',
            'active_rules' => [
                'routes_parts' => [
                    'dashboard::pages.',
                ],
            ],
        ],
        [
            'text' => 'Marketing',
            'icon' => 'fa-coins',
            'rank' => 0,
            'subitems' => [
                [
                    'text' => 'Sms',
                    'icon' => 'fa-sms',
                    'link' => 'dashboard/sms',
                    'active_rules' => [
                        'routes_parts' => [
                            'dashboard::sms.',
                        ],
                    ],
                ],
                [
                    'text' => 'Sms History',
                    'icon' => 'fa-sms',
                    'link' => 'dashboard/sms/history',
                    'active_rules' => [
                        'routes_parts' => [
                            'dashboard::sms.history.',
                        ],
                    ],
                ],
            ]

        ],
        [
            'text' => 'Settings',
            'icon' => 'fa-wrench',
            'rank' => 0,
            'subitems' => [
                [
                    'text' => 'General Settings',
                    'icon' => 'fa-wrench',
                    'link' => 'dashboard/settings',
                    'active_rules' => [
                        'routes_parts' => [
                            'dashboard::settings.',
                        ],
                    ],
                ],

                [
                    'text' => 'Tracking Codes',
                    'icon' => 'fa-circle',
                    'link' => 'dashboard/analytics',
                    'active_rules' => [
                        'routes_parts' => [
                            'dashboard::analytics.',
                        ],
                    ],
                ],
                [
                    'text' => 'Twilio Settings',
                    'icon' => 'fa-circle',
                    'link' => 'dashboard/twiliosettings',
                    'active_rules' => [
                        'routes_parts' => [
                            'dashboard::twiliosettings.',
                        ],
                    ],
                ],

                [
                    'text' => 'Recycle bin',
                    'icon' => 'fa-trash',
                    'link' => 'dashboard/trash',
                    'active_rules' => [
                        'routes_parts' => [
                            'dashboard::trash.',
                        ],
                    ],
                ]
            ],
        ]
    ],

    /*
     |--------------------------------------------------------------------------
     | NavBar menu
     |--------------------------------------------------------------------------
     */
    'header_navigation' => [
        [
            'text' => 'Logout',
            'icon' => '',
            'link' => 'logout',
        ],
        [
            'text' => 'Preview site',
            'icon' => '',
            'link' => '/',
            'target' => '_blank'
        ],
    ],

    /*
 |--------------------------------------------------------------------------
 | Default image for preview
 |--------------------------------------------------------------------------
 |
 | You can see the available dashboard components if enabled
 |
 */
    'default_image'     => '/webmagic/dashboard/img/default-image-png.png',


    /*
     |--------------------------------------------------------------------------
     | Activate dashboard presentation mode
     |--------------------------------------------------------------------------
     |
     | You can see the available dashboard components if enabled
     |
     */
    'presentation_mode' => false,
];
