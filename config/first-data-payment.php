<?php

return [
    /**
     * Sandbox mode configuration
     */
    'sandbox_mode'     => env('FIRST_DATA_SANDBOX_MODE', 1),

    /**
     * Service URL
     */
    'hostname'         => 'https://prod.api.firstdata.com',
    'hostname_sandbox' => 'https://cert.api.firstdata.com',

    /**
     * Library URL
     */
    'lib'              => 'https://docs.paymentjs.firstdata.com/lib/prod/client-2.0.0.js',
    'lib_sandbox'      => 'https://docs.paymentjs.firstdata.com/lib/uat/client-2.0.0.js',

    /**
     * Credentials for integration
     */
    'credentials'      =>
        [
            'apiKey'    => env('FIRST_DATA_API_KEY', null),
            'apiSecret' => env('FIRST_DATA_API_SECRET', null),
            'storeID'   => env('FIRST_DATA_STORE_ID', null),
        ],

    /**
     * Credentials to gateway
     */
    'gatewayConfig'    =>
        [
            "gateway"        => 'IPG',
            "apiKey"         => env('FIRST_DATA_API_KEY', null),
            "apiSecret"      => env('FIRST_DATA_API_SECRET', null),
            'zeroDollarAuth' => false

            //            "gateway"        => 'ipg_cert_Nitrous-Competitions_1120541940',
            //            "apiKey"         => env('FIRST_DATA_API_KEY', null),
            //            "apiSecret"      => env('FIRST_DATA_API_SECRET', null),
            //            'zeroDollarAuth' => false,



            //            'gateway'         => 'PAYEEZY',
            //            'apiKey'          => '{PAYEEZY_APIKEY}',
            //            'apiSecret'       => '{PAYEEZY_APISECRET}',
            //            'authToken'       => '{PAYEEZY_AUTHTOKEN}',
            //            'transarmorToken' => '{PAYEEZY_TATOKEN}',
            //            'zeroDollarAuth'  => false
            //
            //            /*"gateway" => 'BLUEPAY',
            //              "accountId" => '',
            //              "secretKey" => '',
            //              'zeroDollarAuth' => false */
            //
            //            /*"gateway" => 'CARD_CONNECT',
            //              "apiUserAndPass" => '',
            //              "merchantId" => '',
            //              'zeroDollarAuth' => false */
        ],
];