<?php

return [
    /*
     * Image path for different entities
     */
    'competition_img_path' => 'dashboard/competitions/img',
    'past_winner_img_path' => 'dashboard/past-winners/img',
    'question_img_path'    => 'dashboard/questions/img',
    'result_img_path'      => 'dashboard/results/img',

    /*
    * Available counts for items
    */
    'view_sort_by' => [12, 20, 32, 40, 52],
    'items_count_default' => 12,

    /*
     * Max count of tickets for random generation
     */
    'max_ticket_for_randomizer' => 3,

    /*
     * Max order processing time, minutes
     * After this time orders will be automatically marked as "Failed" and the tickets will be returned
     */
    'max_order_processing_time' => 32,

    /**
     * Count of competitions which will be shown on home page
     */
    'competitions_for_home_page' => 6,


    /**
     * Not tickets feature control
     */
    'without_tickets' => false,
];
