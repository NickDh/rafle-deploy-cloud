<?php

use App\Competitions\Competition;
use App\LiveDraws\LiveDraw;
use App\Ecommerce\Orders\Order;
use App\PastWinners\PastWinner;
use App\PromoCodes\PromoCode;
use App\Questions\Answer;
use App\Questions\Question;
use App\Tickets\Ticket;
use App\Tickets\TicketService;
use App\Users\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createAnswerAndQuestions();
        $this->createCompetitions();
        $this->createPromoCodes();
        $this->createPastWinners();
        $this->createLiveDraws();
        $this->createUsers();
        $this->createOrders();
        //$this->createTickets();
    }

    /**
     * Create answers and questions
     */
    protected function createAnswerAndQuestions()
    {
        factory(Question::class, 2)->create()->each(function ($question) {
            $question->answers()->saveMany(factory(Answer::class, 3)->make());
            $question->answers()->saveMany(factory(Answer::class, 1)->make(['is_correct' => true]));
        });
    }


    /**
     * Create competitions
     */
    protected function createCompetitions()
    {
        return factory(Competition::class, 50)->create();
    }

    /**
     * Create promo codes
     */
    protected function createPromoCodes()
    {
        factory(PromoCode::class, 1)->create([
            'code' => 100,
            'type' => \App\Enums\PromoCodeTypesEnum::PERCENT(),
            'value' => 100,
            'max_use' => 100,
            'max_use_per_user' => 100,
            'status' => \App\Enums\PromoCodeStatusTypesEnum::LIVE(),
            'expires_at' => now()->addYear()
        ]);

        return factory(PromoCode::class, 20)->create();
    }

    /**
     * Create past winners
     */
    protected function createPastWinners()
    {
        return factory(PastWinner::class, 20)->create();
    }

    /**
     * Create live draws
     */
    protected function createLiveDraws()
    {
        return factory(LiveDraw::class, 20)->create();
    }

    /**
     * Create live draws
     */
    protected function createUsers()
    {
        return factory(User::class, 20)->create();
    }

    /**
     * Create live draws
     */
    protected function createOrders()
    {
        return factory(Order::class, 60)->create();
    }

    /**
     * Create live draws
     */
    protected function createTickets()
    {
        $competitions = Competition::all();
        $service = new TicketService();

        foreach ($competitions as $competition){
            $tickets_picked = rand(1, $competition->tickets_count/5);
            $numbers = $service->randomPicker($competition, $tickets_picked);

            foreach ($numbers as $number){
                $competition->tickets()->saveMany(factory(Ticket::class, 1)->make(
                    ['number' => $number]
                ));
            }

        }
    }

}
