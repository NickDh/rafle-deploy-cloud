<?php

use App\Users\Roles\Role;
use App\Users\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createRoles();
        $this->createAdmin();

    }

    public function createRoles()
    {
        $role_employee = new Role();
        $role_employee->name = 'Admin';
        $role_employee->description = 'Can use dashboard';
        $role_employee->save();

        $role_manager = new Role();
        $role_manager->name = 'User';
        $role_manager->description = 'Simple app user';
        $role_manager->save();
    }

    public function createAdmin()
    {
        $role_admin = Role::where('name', 'admin')->first();

        User::create([
            'username' => 'rafflelabs',
            'first_name' => 'Administrator',
            'last_name' => 'Labs',
            'email' => 'info@rafflelabs.co.uk',
            'password' => 'DNsoq4LW6Nx3',
            'role_id' => $role_admin->id
        ]);
    }

}
