<?php $user_ids = App\Users\User::where('role_id', 2)->get('id');

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\LiveDraws\LiveDraw::class, function (Faker $faker) {
    return [
        'draw' => $faker->words(4, true),
        'date' => $faker->dateTimeBetween(now(),now()->addMonth()),
    ];
});
