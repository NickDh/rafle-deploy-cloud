<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Competitions\Competition::class, function (Faker $faker) {

    $ids = \App\Questions\Question::all()->pluck('id');

    return [
        'title' => $faker->words(3, true),
        'slug' => $faker->unique()->slug,
        'short_description' => $faker->text,
        'full_description' => $faker->text,
        'price' => $price = $faker->numberBetween(0, 2000),
        'sale_price' => $faker->randomElement([0, $price/2]),
        'competition_type' => $faker->randomElement(\App\Enums\CompetitionTypeEnum::values()),
        'tickets_count' => $faker->numberBetween(1000, 2000),
        'tickets_sort_by' => $faker->numberBetween(10, 100),
        'deadline' => now()->addDays($faker->numberBetween(1, 300)),
        'auto_extend' => $faker->boolean,
        'hours_extend' => $faker->numberBetween(1, 10),
        'status' => $faker->randomElement(\App\Enums\CompetitionStatusEnum::values()),
        'max_tickets_per_user' => $faker->numberBetween(10, 20),
        'main_image'=> $faker->randomElement(['dyson.jpg','raptor.jpg','rolex.jpg']),
        'question_id'=> $faker->randomElement($ids),
        'letters_to' => $faker->randomElement(range('A', 'Z'))
    ];
});
