<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Ecommerce\Orders\Order::class, function (Faker $faker) {

    $user_ids = App\Users\User::where('role_id', 2)->get('id');
    $user = App\Users\User::find($faker->randomElement($user_ids)->id);

    return [
        'total' => $total = $faker->numberBetween(100, 1000),
        'subtotal' => $total,

        'status' => $faker->randomElement(\App\Enums\OrderStatusEnum::values()),
        'user_id' => $user->id,
//        'promo_code_id',
        'customer_name' => $user->present()->prepareFullName(),
        'customer_email' => $user->email,
        'customer_phone' => $user->phone,
        'customer_address' => $user->address,
        'customer_county' => $user->town,
        'customer_city' => $user->town,
        'customer_post_code' => $user->post_code,
        'cart_data' => '{}',
        'created_at' => now()->subMonths($faker->numberBetween(0,15))
    ];
});
