<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Questions\Answer::class, function (Faker $faker) {

    return [
//        'text' => $faker->word(),
        'image' => $faker->randomElement(['option-1.jpg', 'option-2.jpg', 'option-3.jpg', 'option-4.jpg']),
        'is_correct' => false
    ];
});

