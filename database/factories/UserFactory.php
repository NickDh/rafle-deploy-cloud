<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Users\User::class, function (Faker $faker) {
    static $password;
    $role = App\Users\Roles\Role::where('name', 'user')->first();

    return [
        'username' => $faker->userName,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'phone' => $faker->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'address' => $faker->address,
        'town' => $faker->city,
        'post_code' => $faker->postcode,
        'password' => $password ?: $password = 'secret',
        'role_id' => $role->id
    ];
});
