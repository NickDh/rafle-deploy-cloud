<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTwilioSendedSms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('twilio_sended_sms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('message_id');

            $table->dateTimeTz('date_created');
            $table->dateTimeTz('date_sent');
            $table->dateTimeTz('date_updated');

            $table->string('messaging_service_sid');
            $table->string('error_code')->nullable();
            $table->string('error_message')->nullable();

            $table->float('price')->nullable();
            $table->string('sid');
            $table->string('uri');
            $table->string('status',20);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('twilio_sended_sms');
    }
}
