<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PrepareTablesForCompetitionEntriesFlow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competition_entries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('competition_title');
            $table->integer('competition_id');
            $table->unsignedBigInteger('user_id');
            $table->string('question');
            $table->bigInteger('question_id');
            $table->string('answer');
            $table->bigInteger('answer_id');
            $table->boolean('is_answer_right');
            $table->unsignedBigInteger('order_id');
            $table->float('entry_price');

            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('selected_tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('competition_entry_id');
            $table->string('ticket');

            $table->foreign('competition_entry_id')->references('id')->on('competition_entries')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competition_entries');
        Schema::dropIfExists('selected_tickets');
    }
}
