<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePastWinnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('past_winners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('competition')->nullable();
            $table->string('username')->nullable();
            $table->integer('ticket_number')->nullable();
            $table->string('main_image')->nullable();
            $table->string('draw_video')->nullable();
            $table->string('delivery_video')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('past_winners');
    }
}
