<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFirstDataTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('first_data_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('status');

            $table->float('total');
            $table->string('currency');

            $table->string('client_request_id')->nullable();
            $table->string('api_trace_id')->nullable();
            $table->string('ipg_transaction_id')->nullable();
            $table->timestamp('transaction_time')->nullable();
            $table->string('secure_3D_version')->nullable();
            $table->text('method_form')->nullable();
            $table->string('acs_url')->nullable();
            $table->text('creq')->nullable();
            $table->text('session_data')->nullable();
            $table->text('payer_authentication_request')->nullable();
            $table->text('merchant_data')->nullable();
            $table->text('pa_res')->nullable();
            $table->text('md')->nullable();

            $table->text('cres')->nullable();

            $table->text('secure_3D_authentication_request')->nullable();
            $table->text('secure_3D_authentication_response')->nullable();
            $table->text('secure_3D_api_initiation_request')->nullable();
            $table->text('secure_3D_initiation_request')->nullable();
            $table->text('secure_3D_initiation_response')->nullable();
            $table->text('secure_3D_api_initiation2_request')->nullable();
            $table->text('secure_3D_final_request')->nullable();
            $table->text('secure_3D_final_response')->nullable();

            $table->bigInteger('order_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('first_data_transactions');
    }
}
