<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_methods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('client_token')->nullable();
            $table->string('public_key_base64')->nullable();
            $table->string('payment_js_nonce')->nullable();
            $table->dateTime('payment_js_authenticated')->nullable();

            $table->string('token')->nullable();
            $table->string('gateway_ref_id')->nullable();
            $table->dateTime('first_data_authenticated')->nullable();

            $table->string('card_masked')->nullable();
            $table->integer('card_last4')->nullable();
            $table->string('card_brand')->nullable();

            $table->bigInteger('client_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_methods');
    }
}
