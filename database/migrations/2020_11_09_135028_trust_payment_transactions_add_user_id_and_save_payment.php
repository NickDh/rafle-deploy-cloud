<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TrustPaymentTransactionsAddUserIdAndSavePayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trust_payment_transactions', function (Blueprint $table) {
            $table->bigInteger('user_id');
            $table->boolean('use_for_next_payment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trust_payment_transactions', function (Blueprint $table) {
            $table->dropColumn(['user_id', 'use_for_next_payment']);
        });
    }
}
